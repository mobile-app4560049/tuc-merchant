import { DatePipe } from '@angular/common';
import { HttpClient, HttpErrorResponse, HttpHeaders } from "@angular/common/http";
import { Injectable } from '@angular/core';
import { AlertController, LoadingController, MenuController, ModalController, NavController, Platform, ToastController } from '@ionic/angular';
import { Observable, throwError } from "rxjs";
import { catchError, map } from "rxjs/operators";
import "../../assets/js/systemhelper.js";
import { OResponse, OAccountInfo, OAccountOwner, ORewardTypeOverview, OOverview, OUserCounts, OCardTypeSalesSummary, OSalesSummary, ORewardsSummary, OSalesOverview, OAccountOverview, LastTransactionDetails, ORewardOverview, OAppUsersOverview, ORedeemOverview, OTerminalStatusCount, OCountry } from './object.service';
import { Geolocation } from "@ionic-native/geolocation/ngx";
declare var $: any;
declare var moment: any;
declare var SystemHelper: any;

@Injectable()
export class HelperService {
    AnaSlideOptions = {
        initialSlide: 1,
        speed: 400,
        slidesPerView: 2.5,
        spaceBetween: 0
    }
    public AccountInfo: OAccountInfo;
    public AccountCountry: OCountry;
    public AccountOwner: OAccountOwner;
    constructor(
        public _ToastController: ToastController,
        public _ModalController: ModalController,
        public _Platform: Platform,
        private _Http: HttpClient,
        public _NavController: NavController,
        public _Geolocation: Geolocation,
        public _DatePipe: DatePipe,
        public _LoadingController: LoadingController,
        public _AlertController: AlertController,
        public _MenuController: MenuController
    ) {
        this.RefreshProfile();
    }
    RefreshProfile() {
        var Account = this.GetStorage(this.AppConfig.StorageHelper.Account);
        if (Account != null) {
            this.AccountInfo = Account as OAccountInfo;
            this.AccountCountry = this.AccountInfo.UserCountry;
            this.AppConfig.CurrencySymbol = this.AccountCountry.CurrencyCode;
            if (this.AccountInfo.UserOwner != undefined && this.AccountInfo.UserOwner != null) {
                if (this.AccountInfo.UserAccount.AccountTypeCode == 'thankumerchant') {
                    this.AccountOwner =
                    {
                        AccountCode: this.AccountInfo.UserAccount.AccountCode,
                        AccountKey: this.AccountInfo.UserAccount.AccountKey,
                        Name: this.AccountInfo.User.Name,
                        IsTucPlusEnabled: this.AccountInfo.UserAccount.IsTucPlusEnabled,
                        IconUrl: this.AccountInfo.UserAccount.IconUrl,
                        DisplayName: this.AccountInfo.UserAccount.DisplayName,
                        AccountTypeCode: this.AccountInfo.UserAccount.AccountTypeCode,
                        AccountId: this.AccountInfo.UserAccount.AccountId,
                    }
                }
                else {
                    this.AccountOwner = this.AccountInfo.UserOwner;
                }
            }
            else {
                this.AccountOwner =
                {
                    AccountCode: this.AccountInfo.UserAccount.AccountCode,
                    AccountKey: this.AccountInfo.UserAccount.AccountKey,
                    Name: this.AccountInfo.User.Name,
                    IsTucPlusEnabled: this.AccountInfo.UserAccount.IsTucPlusEnabled,
                    IconUrl: this.AccountInfo.UserAccount.IconUrl,
                    DisplayName: this.AccountInfo.UserAccount.DisplayName,
                    AccountTypeCode: this.AccountInfo.UserAccount.AccountTypeCode,
                    AccountId: this.AccountInfo.UserAccount.AccountId,
                }
            }
        }
        else {
            this.AccountOwner =
            {
                AccountCode: null,
                AccountId: 0,
                AccountKey: null,
                AccountTypeCode: null,
                DisplayName: null,
                IconUrl: null,
                IsTucPlusEnabled: false,
                Name: null,
            }
            this.AccountInfo =
            {
                AccessKey: null,
                LoginTime: null,
                PublicKey: null,
                User:
                {
                    Address: "na",
                    AddressLatitude: 0,
                    AddressLongitude: 0,
                    ContactNumber: "na",
                    ContactNumberVerificationStatus: 0,
                    EmailAddress: "na",
                    EmailVerificationStatus: 0,
                    FirstName: "na",
                    LastName: "na",
                    MobileNumber: "na",
                    Name: "na",
                    UserName: "na"
                },
                UserAccount:
                {
                    AccountCode: "na",
                    AccountId: 0,
                    AccountKey: "na",
                    AccountType: "na",
                    AccountTypeCode: "na",
                    CreateDate: null,
                    DisplayName: "na",
                    IconUrl: "na",
                    IsAccessPinSet: "na",
                    IsTucPlusEnabled: false,
                    PosterUrl: "na",
                    ReferralCode: "na",
                    UserKey: "na",
                },
                UserCountry:
                {
                    CountryIsd: "na",
                    CountryIso: "na",
                    CountryKey: "na",
                    CountryName: "na",
                    CurrencyName: "na",
                    CurrencyNotation: "na",
                    CurrencyCode: 'na'
                },
                UserOwner:
                {
                    AccountCode: null,
                    AccountId: null,
                    AccountKey: null,
                    AccountTypeCode: null,
                    DisplayName: null,
                    IconUrl: null,
                    IsTucPlusEnabled: null,
                    Name: null,
                }
            }
        }
    }
    public AppConfig = {
        TucDateModal: false,
        TucDateModalBackdrop: false,
        IsProcessing: false,
        ShowProgressBar: false,
        AccountOwnerId: 0,
        AccountOwnerKey: null,
        AccountOwnerDisplayName: null,
        AccountOwnerIconUrl: null,
        CurrencySymbol: "",
        StatusSuccess: "Success",
        StatusError: "Error",
        ActiveLocation:
        {
            Lat: 0,
            Lon: 0,
        },
        PushTopics:
        {
            tucmerchant: 'tucmerchant',
            // tucmerchant: 'tucmerchant_test',
        },
        ServerUrl: "https://webconnect.thankucash.com/",
        // ServerUrl: "https://appconnect.thankucash.com/",
        // ServerUrl: "https://testwebconnect.thankucash.com/",
        // ServerUrl: "https://localhost:5001/",
        ActiveReferenceId: null,
        ActiveReferenceKey: null,
        TimeZone: "Africa/Lagos",
        TimeFormat: "h:mm a",
        DateFormat: "Do MMM YYYY",
        DateMonthFormat: "DD-MMM",
        DateTimeFormat: "DD-MM-YYYY h:mm a",
        NetworkKeys: {
            Android: {
                AppVersion: "0.0.4",
                AppKey: "3646a0ba8bc74d47996dbd116e227c8d",
                AppVersionKey: "324ef9c718c843bdbb19c98a7b86f0c1"
            },
            Ios: {
                AppVersion: "0.0.4",
                AppKey: "3646a0ba8bc74d47996dbd116e227c8d",
                AppVersionKey: "1f77e1ea19aa1dd8603f27318cc155ae4f4fbfbc"
            }
        },
        // NetworkKeys: {
        //     Android: {
        //         AppVersion: "0.0.4",
        //         AppKey: "9a369214b7a84d73b954bfb79c5809e6",
        //         AppVersionKey: "5f320c496782415c91a74891668d5126"
        //     },
        //     Ios: {
        //         AppVersion: "0.0.4",
        //         AppKey: "9a369214b7a84d73b954bfb79c5809e6",
        //         AppVersionKey: "5f320c496782415c91a74891668d5126"
        //     }
        // },
        Network: {
            V1: {
                System: "api/v1/system/",
                ThankU: "api/v1/thanku/",
                Account: "api/v1/account/",
                EnyoAppConnect: "api/v1/enyoappconnect/",
                EnyoOrderConnect: "api/v1/enyoorderconnect/",
                Core: "api/v1/core/",
                EnyoSysConnect: "api/v1/enyosysconnect/",
            },
            V2: {
                System: "api/v2/system/",
                ThankU: "api/v2/thankucash/",
                TUCTransCore: "api/v2/tuctranscore/",
                HCProduct: "api/v2/hcproduct/",
                TUCAccCore: "api/v2/tucacccore/",
                TUCRmManager: "api/v2/rmmanager/",
                ThankUCashLead: "api/v2/tucashlead/",
                TUCCampaign: "api/v2/tuccampaign/",
                TUCAnalytics: "api/v2/tucanalytics/",
                TUCGiftPoints: "api/v2/tucgiftpoints/",
                CAProduct: "api/v2/caproduct/",
                HCWorkHorse: "api/v2/workhorse/"
            },
            V3:
            {
                Transactions: "api/v3/merchant/trans/",
                DealManager: 'api/v3/plugins/deals/',
                Deals: 'api/v3/plugins/dealsop/',
                Tran: 'api/v3/merchant/trans/',
                Rewards: 'api/v3/merchant/app/',
                Payments: "api/v3/merchant/payments/",
                BankManager: "api/v3/tuc/bankop/",
                OuterOperations: "api/v3/tuc/op/",
                Accounts: "api/v3/merchant/accounts/",

                System2: "api/v3/plugins/dealscat/",
                ConAccount: "api/v3/con/accounts/",
                Bank: "api/v3/tuc/bankop/",
                OnBoardM: "api/v3/merchant/onboarding/",
                SystemFaq: "api/v3/sys/faq/",
                Maddeals: "api/v3/plugins/maddeals/",
                PlOps: "api/v3/plugins/operations/",
                Account: "api/v3/merchant/accounts/",
                Cop: "api/v3/tuc/cop/",
                Branch: "api/v3/acquirer/branch/",
                SubAccounts: "api/v3/acquirer/subaccounts/",
                Analytics: "api/v3/merchant/analytics/",
                OnBoard: "api/v3/acquirer/onboarding/",
                MOnBoard: "api/v3/merchant/onboarding/",
                GC: "api/v3/plugins/gc/",
                SystemMange: "api/v3/system/manage/",
                // Operations: "api/v3/merchant/operations/",
                Op: "api/v3/tuc/op/",
                Subscription: "api/v3/merchant/subscription/",
                AccountSubscription: "api/v3/tuc/subscriptions/",
                MTransaction: "api/v3/merchant/trans/", System: "api/v3/con/sys/",
                Requestotp: "api/v3/w/acc/connect/requestotp",
                AccountManager: {
                    Manage: "api/v3/acc/manage/"
                }
            }

        },
        NetworkApi:
        {
            Login: "login",
            Logout: "logout",
            Feature:
            {
                SaveUserAccount: 'saveuseraccount'
            },
            getaccountparameters: 'getaccountparameters',
            saveaccountparameter: 'saveaccountparameter'
        },
        StorageHelper: {
            OReqH: "hcscoreqh",
            Device: "hcscd",
            Location: "hcsl",
            Account: "hca",
            ActiveReference: "hcark",
            NavPreviousPageReference: "hcppr",
            Verification: "hcv",
            Stations: "enstations",
            Products: "enproducts",
            PaymentsHistory: "paymentshistory",
            OrdersHistory: "ordershistory",
            ActiveOrder: "activeorder",
            SelectedProduct: "activeproduct",
            CustomLocation: 'custloc',
            Balance: 'hcbalance'
        },
        Pages:
        {
            Reg:
            {
                Registration: 'reginfo',
                MobileVerifyPage: 'mobileverifypage',
                addresslocator: "addresslocator"
            },
            Auth:
            {
                Login: 'login',
                VerifyPin: 'verifypin',
                AuthProfile: 'authprofile',
            },
            Access:
            {

                CashierAccess:
                {
                    cashierdashboard: 'cashierdashboard',
                    cashiertucpaytransactions: 'cashiertucpaytransactions',
                    Reward:
                    {
                        ScanCode: 'cashierrewardscancode',
                        Initialize: 'cashierrewardinitialize',
                        Confirm: 'cashierrewardconfirm',
                        RewardHistory: 'cashierrewardhistory',
                    },
                    Deals:
                    {
                        scandealcode: 'cashierscandealcode',
                        redeemdealconfirm: 'cashierredeemdealconfirm',
                        redeemhistory: 'cashierredeemhistory',
                        dealdetails: 'cashierdealdetails',
                    },
                },

                Dashboard: 'dashboard',
                Profile: 'profile',
                Stores: 'stores',
                Terminals: 'terminals',
                Cashiers: 'cashiers',
                SalesHistory: 'saleshistory',
                OrdersHistory: 'ordershistory',
                Wallet: 'wallet',
                TransactionsHistory: 'transactionshistory',
                About: 'about',
                Store: 'store',
                St1OrderDetails: 'st1orderdetails',
                St2OrderPayment: 'st2orderpayment',
                St3OrderConformation: 'st3orderconfirmation',

                slgasselectproduct: 'slgasselectproduct',
                slgasselectproductvarient: 'slgasselectproductvarient',

                d2dcreateorder: 'd2dcreateorder',
                orderaddresssetup: 'orderaddresssetup',
                orderpaymentprocess: 'orderpaymentprocess',
                orderdetails: 'orderdetails',

                servicehistory: 'servicehistory',

                // vehiconcreateorder: 'vehiconcreateorder',
                // vehicon_selectmodel: 'vehicon_selectmodel',
                // vehicon_selectservice: 'vehicon_selectservice',
                // vehicon_selectvarient: 'vehicon_selectvarient',
                // vehicon_selectstation: 'vehicon_selectstation',
                OrdeManager:
                {
                    order: 'order',
                    orders: 'orders'
                },
                Deals:
                {
                    Dashboard: 'dealsdashboard',
                    scandealcode: 'scandealcode',
                    redeemdealconfirm: 'redeemdealconfirm',
                    redeemhistory: 'redeemhistory',
                    dealdetails: 'dealdetails',
                    dealslist: 'deals'
                },
                Redeem:
                {
                    St1: 'redeemst1',
                },
                Reward:
                {
                    ScanCode: 'rewardscancode',
                    Initialize: 'rewardinitialize',
                    Confirm: 'rewardconfirm',
                    RewardHistory: 'rewardhistory',
                },
                TucPay:
                {
                    TucPay: 'tucpaytransactions'
                },
                BankManager:
                {
                    list: "bankmanager",
                },
                Cashout:
                {
                    list: 'cashout'
                },
                Loyalty:
                {
                    Dashboard: 'loyaltydashboard',
                    list: 'rewards'
                },
                pendingrewardhistory: 'pendingrewardhistory',
            }
        },
        LoaderItem: undefined,
        Organization:
        {
            enr:
            {
                OrgId: 2,
                OrgCode: "enr",
                OrgName: "Enyo Retail",
                OrgDomain: "enyoretail.com",
                OrgLogo: 'logo_er.png'
            },
            enrslgas:
            {
                OrgId: 45,
                OrgCode: "enrslgas",
                OrgName: "Enyo Retail - SL-Gas",
                OrgDomain: "slgas.ng",
                OrgLogo: 'logo_slgas.png'
            },
            enrd2d:
            {
                OrgId: 46,
                OrgCode: "enrd2d",
                OrgName: "Enyo Retail - Diesel2Door",
                OrgDomain: "diesel2door.com",
                OrgLogo: 'logo_d2d.png'
            },
            enrvehicon:
            {
                OrgId: 47,
                OrgCode: "enrvehicon",
                OrgName: "Enyo Retail - Vehicon",
                OrgDomain: "vehicon.ng",
                OrgLogo: 'logo_vehicon.png'
            }
        },
        OrganizationList: [
            {
                OrgCode: "enr",
                OrgName: "Enyo Retail",
                OrgDomain: "enyoretail.com",
            },
            {
                OrgCode: "enrslgas",
                OrgName: "Enyo Retail - SL-Gas",
                OrgDomain: "slgas.ng",
            },
            {
                OrgCode: "enrd2d",
                OrgName: "Enyo Retail - Diesel2Door",
                OrgDomain: "diesel2door.com",
            },
            {
                OrgCode: "enrvehicon",
                OrgName: "Enyo Retail - Vehicon",
                OrgDomain: "vehicon.ng",
            }
        ],
        DataType: {
            Text: "text",
            Number: "number",
            Date: "date",
            Decimal: "decimal"
        },
    }
    public exc = 0;
    HandleException(Exception: HttpErrorResponse) {
        this.HideSpinner();
        if (Exception.status != undefined && Exception.status == 401) {
            var Ex = Exception as any;
            var ResponseData = JSON.parse(atob(Ex.error.zx)) as OResponse;
            this.exc = 0;
            if (ResponseData.ResponseCode == "HCA121") {
                if (this.exc == 0) {
                    this.exc = 1;
                    this.Notify("Session Expired", ResponseData.Message);

                }
                this.DeleteStorage(this.AppConfig.StorageHelper.Account);
                this.DeleteStorage(this.AppConfig.StorageHelper.ActiveReference);
                this.DeleteStorage(this.AppConfig.StorageHelper.Verification);
                this.DeleteStorage(this.AppConfig.StorageHelper.Stations);
                this.DeleteStorage(this.AppConfig.StorageHelper.Products);
                this.DeleteStorage(this.AppConfig.StorageHelper.PaymentsHistory);
                this.DeleteStorage(this.AppConfig.StorageHelper.OrdersHistory);
                this.DeleteStorage(this.AppConfig.StorageHelper.ActiveOrder);
                this.DeleteStorage(this.AppConfig.StorageHelper.SelectedProduct);
                this.NavigateRoot(this.AppConfig.Pages.Auth.Login);
            } else {
                this.Notify("Operation failed", ResponseData.Message);
            }
        }
    }

    ShowTucDateModal() {
        this.AppConfig.TucDateModal = true;
        this.AppConfig.TucDateModalBackdrop = true;
    }

    HideTucDateModal() {
        this.AppConfig.TucDateModal = false;
        this.AppConfig.TucDateModalBackdrop = false;
    }

    async ModalDismiss(Data) {
        await this._ModalController.dismiss(Data);
    }
    private ProgressItem = 0;
    ShowProgress() {
        this.ProgressItem += 1;
        this.AppConfig.ShowProgressBar = true;
    }
    HideProgress() {
        this.ProgressItem = this.ProgressItem - 1;
        if (this.ProgressItem == 0) {
            this.AppConfig.ShowProgressBar = false;
        }
    }
    GeneratePassoword() {
        var plength = 14;
        var keylistalpha = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        var keylistint = "123456789";
        var keylistspec = "!@#_";
        var temp = "";
        var len = plength / 2;
        var len = len - 1;
        var lenspec = plength - len - len;

        for (let index = 0; index < len; index++)
            temp += keylistalpha.charAt(
                Math.floor(Math.random() * keylistalpha.length)
            );

        for (let index = 0; index < lenspec; index++)
            temp += keylistspec.charAt(
                Math.floor(Math.random() * keylistspec.length)
            );

        for (let index = 0; index < len; index++)
            temp += keylistint.charAt(Math.floor(Math.random() * keylistint.length));

        temp = temp
            .split("")
            .sort(function () {
                return 0.5 - Math.random();
            })
            .join("");

        return temp;
    }
    GetRandomNumber() {
        return Math.floor(1000 + Math.random() * 9000);
    }
    GenerateGuid() {
        var arr = new Uint8Array(40 / 2);
        window.crypto.getRandomValues(arr);
        return Array.from(arr, this.dec2hex).join("");
    }
    dec2hex(dec) {
        return ("0" + dec.toString(16)).substr(-2);
    }
    async Notify(Title, Message) {
        const alert = await this._AlertController.create({
            header: Title,
            message: Message,
            buttons: ["OK"]
        });
        await alert.present();
    }
    async NotifySimple(Message) {
        const alert = await this._AlertController.create({
            message: Message,
            buttons: ["OK"]
        });
        await alert.present();
    }
    async ShowSpinner(message = "Please wait...") {
        if (this.AppConfig.LoaderItem == undefined) {
            this.AppConfig.LoaderItem = await this._LoadingController.create({
                message: message,
                spinner: "dots"
            });
            setTimeout(() => {
                this.HideSpinner();
            }, 5000);
            return await this.AppConfig.LoaderItem.present();
        }
    }
    async NotifyToast(Message) {
        const toast = await this._ToastController.create({
            position: 'top',
            message: Message,
            duration: 2000,
            // color: "#a42e80"
        });
        toast.present();
    }
    HideSpinner() {
        if (this.AppConfig.LoaderItem != undefined) {
            this.AppConfig.LoaderItem.dismiss();
            this.AppConfig.LoaderItem = undefined;
        }

    }
    NavigateRoot(PageName) {
        this._NavController.navigateRoot(PageName);
    }
    NavigateRootNoAnimate(PageName) {
        this._NavController.navigateRoot(PageName, { animated: false });
    }
    NavigatePush(PageName) {
        this.SaveStorageValue(this.AppConfig.StorageHelper.NavPreviousPageReference, window.location.pathname.replace("/", ""));
        this._NavController.navigateForward(PageName);
    }
    NavigatePushItem(PageName, Data) {
        this._NavController.navigateForward([PageName, Data]);
    }
    NavMore() {
        this._MenuController.open('end');
    }
    NavOrders() {
        this.NavigateRoot(this.AppConfig.Pages.Access.OrdeManager.orders);
    }
    NavDeals() {
        this.NavigateRoot(this.AppConfig.Pages.Access.Deals.Dashboard);
    }
    NavDashboard() {
        this.NavigateRoot(this.AppConfig.Pages.Access.Dashboard);
    }
    NavLoyalty() {
        this.NavigateRoot(this.AppConfig.Pages.Access.Loyalty.Dashboard);
    }

    RefreshLocation() {
        var GeoLocationOptions = {
            maximumAge: 600000,
            timeout: 20000
        };
        var DeviceStorage = this.GetStorage(this.AppConfig.StorageHelper.Device);
        this.AppConfig.ActiveLocation.Lat = DeviceStorage.Latitude;
        this.AppConfig.ActiveLocation.Lon = DeviceStorage.Longitude;
        if (DeviceStorage != null) {
            this._Geolocation
                .getCurrentPosition()
                .then(resp => {
                    DeviceStorage.Latitude = resp.coords.latitude;
                    DeviceStorage.Longitude = resp.coords.longitude;
                    this.AppConfig.ActiveLocation.Lat = resp.coords.latitude;
                    this.AppConfig.ActiveLocation.Lon = resp.coords.longitude;
                    this.SaveStorage(this.AppConfig.StorageHelper.Device, DeviceStorage);
                })
                .catch(error => { });
        }
    }
    SaveStorage(StorageName, StorageValue) {
        try {
            var StringV = JSON.stringify(StorageValue);
            localStorage.setItem(StorageName, StringV);
            return true;
        } catch (e) {
            alert(e);
            return false;
        }
    }
    SaveStorageValue(StorageName, StorageValue) {
        try {
            localStorage.setItem(StorageName, StorageValue);
            return true;
        } catch (e) {
            alert(e);
            return false;
        }
    }
    GetStorage(StorageName) {
        var StorageValue = localStorage.getItem(StorageName);
        if (StorageValue != undefined) {
            if (StorageValue != null) {
                return JSON.parse(StorageValue);
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
    GetStorageValue(StorageName) {
        var StorageValue = localStorage.getItem(StorageName);
        if (StorageValue != undefined) {
            if (StorageValue != null) {
                return StorageValue;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
    DeleteStorage(StorageName) {
        localStorage.removeItem(StorageName);
        return true;
    }
    payWithPaystack(Amount) {
        return SystemHelper.payWithPaystack(this.AccountInfo, Amount);
    }
    GetDateTime(Date) {
        return SystemHelper.GetDateTime(Date, this.AppConfig.TimeZone);
    }
    GetTimeS(Date) {
        return this._DatePipe.transform(Date, "h:mm a", this.AppConfig.TimeZone);
    }
    GetDateS(Date) {
        return this._DatePipe.transform(Date, "dd-MM-yyyy", this.AppConfig.TimeZone);
    }
    GetTodaysDate(Date) {
        return this._DatePipe.transform(Date, "dd-MM-yyyy", this.AppConfig.TimeZone);
    }
    GetCurrentDateTimeS() {
        return this._DatePipe.transform(new Date(), "dd-MM-yyyy h:mm a", this.AppConfig.TimeZone);
    }
    CheckDateIsAfter(Date, CompareTo) {
        return SystemHelper.CheckDateIsAfter(Date, CompareTo);
    }
    CheckDateIsBefore(Date, CompareTo) {
        return SystemHelper.CheckDateIsBefore(Date, CompareTo);
    }
    GetTimeDifference(Date, CompareTo) {
        return SystemHelper.GetTimeDifference(Date, CompareTo);
    }
    GetTimeDifferenceS(Date, CompareTo) {
        return SystemHelper.GetTimeDifferenceS(Date, CompareTo);
    }
    GetDateTimeS(Date) {
        return SystemHelper.GetDateTimeS(Date, this.AppConfig.TimeZone, this.AppConfig.DateTimeFormat);
    }
    GetTimeInterval(Date, CompareTo) {
        return SystemHelper.GetTimeInterval(Date, CompareTo, this.AppConfig.TimeZone);
    }
    GetDateMonthS(Date) {
        return SystemHelper.GetDateS(
            Date,
            this.AppConfig.TimeZone,
            this.AppConfig.DateMonthFormat
        );
    }
    FormatDate(Date, Format) {
        return SystemHelper.GetDateS(Date, this.AppConfig.TimeZone, Format);
    }
    GetSearchConditionStrict(
        BaseString,
        ColumnName,
        ColumnType,
        ColumnValue,
        Condition
    ) {
        if (BaseString == undefined || BaseString == null) {
            BaseString = "";
        }

        var SearchExpression = BaseString;
        if (
            ColumnName != undefined &&
            ColumnType != undefined &&
            // ColumnValue != undefined &&
            ColumnName != null &&
            ColumnType != null &&
            //ColumnValue != null &&
            ColumnName != "" &&
            ColumnType != "" //&&
            // ColumnValue != ''
        ) {
            if (ColumnType == "text") {
                if (
                    SearchExpression != undefined &&
                    SearchExpression != null &&
                    SearchExpression != ""
                ) {
                    SearchExpression = "( " + SearchExpression + ") AND ";
                }
                // SearchExpression += ColumnName + '.Contains("' + ColumnValue + '")';
                if (ColumnValue == "") {
                    SearchExpression += ColumnName + " " + Condition + ' "" ';
                } else if (ColumnValue != null && ColumnValue != "") {
                    SearchExpression +=
                        ColumnName + " " + Condition + ' "' + ColumnValue + '" ';
                } else {
                    SearchExpression +=
                        "( " + ColumnName + " " + Condition + " null " + ")";
                }
            } else if (ColumnType == "number") {
                if (
                    SearchExpression != undefined &&
                    SearchExpression != null &&
                    SearchExpression != ""
                ) {
                    SearchExpression = "( " + SearchExpression + ") AND ";
                }
                if (ColumnValue != null && ColumnValue != "") {
                    SearchExpression +=
                        ColumnName + " " + Condition + ' "' + ColumnValue + '" ';
                } else {
                    SearchExpression += "( " + ColumnName + " " + Condition + " " + ")";
                }
            }

            if (BaseString != undefined && BaseString != null && BaseString != "") {
                BaseString += " OR ";
            }

            //  BaseString += SearchExpression;
            return SearchExpression;
        } else {
            return BaseString;
        }
    }
    GetSearchCondition(BaseString, ColumnName, ColumnType, ColumnValue) {
        if (BaseString == undefined || BaseString == null) {
            BaseString = "";
        }

        var SearchExpression = BaseString;
        if (
            ColumnName != undefined &&
            ColumnType != undefined &&
            ColumnValue != undefined &&
            ColumnName != null &&
            ColumnType != null &&
            ColumnValue != null &&
            ColumnName != "" &&
            ColumnType != "" &&
            ColumnValue != ""
        ) {
            if (ColumnType == "text") {
                if (
                    SearchExpression != undefined &&
                    SearchExpression != null &&
                    SearchExpression != ""
                ) {
                    SearchExpression += " OR ";
                }
                SearchExpression += ColumnName + '.Contains("' + ColumnValue + '")';
            } else if (ColumnType == "number") {
                if (isNaN(ColumnValue) == false) {
                    if (
                        SearchExpression != undefined &&
                        SearchExpression != null &&
                        SearchExpression != ""
                    ) {
                        SearchExpression = "( " + SearchExpression + ") AND ";
                    }
                    SearchExpression += ColumnName + " = " + ColumnValue + " ";
                }
            }

            if (BaseString != undefined && BaseString != null && BaseString != "") {
                BaseString += " OR ";
            }

            //  BaseString += SearchExpression;
            return SearchExpression;
        } else {
            return BaseString;
        }
    }
    GetSearchConditionStrictOr(
        BaseString,
        ColumnName,
        ColumnType,
        ColumnValue,
        Condition
    ) {
        if (BaseString == undefined || BaseString == null) {
            BaseString = "";
        }

        var SearchExpression = BaseString;
        if (
            ColumnName != undefined &&
            ColumnType != undefined &&
            // ColumnValue != undefined &&
            ColumnName != null &&
            ColumnType != null &&
            //ColumnValue != null &&
            ColumnName != "" &&
            ColumnType != "" //&&
            // ColumnValue != ''
        ) {
            if (ColumnType == this.AppConfig.DataType.Text) {
                if (
                    SearchExpression != undefined &&
                    SearchExpression != null &&
                    SearchExpression != ""
                ) {
                    SearchExpression = "( " + SearchExpression + ") OR ";
                }
                // SearchExpression += ColumnName + '.Contains("' + ColumnValue + '")';
                if (ColumnValue == "") {
                    SearchExpression += ColumnName + " " + Condition + ' "" ';
                } else if (ColumnValue != null && ColumnValue != "") {
                    SearchExpression +=
                        ColumnName + " " + Condition + ' "' + ColumnValue + '" ';
                } else {
                    SearchExpression +=
                        "( " + ColumnName + " " + Condition + " null " + ")";
                }
            } else if (ColumnType == this.AppConfig.DataType.Number) {
                if (
                    isNaN(ColumnValue) == false &&
                    ColumnValue.toString().indexOf(".") == -1
                ) {
                    if (
                        SearchExpression != undefined &&
                        SearchExpression != null &&
                        SearchExpression != ""
                    ) {
                        SearchExpression = "( " + SearchExpression + ") OR ";
                    }
                    if (ColumnValue != null && ColumnValue != "") {
                        SearchExpression +=
                            ColumnName + " " + Condition + ' "' + ColumnValue + '" ';
                    } else {
                        SearchExpression += "( " + ColumnName + " " + Condition + " " + ")";
                    }
                }
            } else if (ColumnType == this.AppConfig.DataType.Decimal) {
                if (isNaN(ColumnValue) == false) {
                    if (
                        SearchExpression != undefined &&
                        SearchExpression != null &&
                        SearchExpression != ""
                    ) {
                        SearchExpression = "( " + SearchExpression + ") OR ";
                    }
                    if (ColumnValue != null && ColumnValue != "") {
                        SearchExpression +=
                            ColumnName + " " + Condition + ' "' + ColumnValue + '" ';
                    } else {
                        SearchExpression += "( " + ColumnName + " " + Condition + " " + ")";
                    }
                }
            }
            if (BaseString != undefined && BaseString != null && BaseString != "") {
                BaseString += " AND ";
            }

            //  BaseString += SearchExpression;
            return SearchExpression;
        } else {
            return BaseString;
        }
    }
    GetDateCondition(BaseString, ColumnName, StartTime, EndTime) {
        if (BaseString == undefined || BaseString == null) {
            BaseString = "";
        }

        var SearchExpression = BaseString;
        if (
            ColumnName != undefined &&
            StartTime != undefined &&
            EndTime != undefined &&
            ColumnName != null &&
            StartTime != null &&
            EndTime != null &&
            ColumnName != "" &&
            StartTime != "" &&
            EndTime != ""
        ) {

            StartTime = moment(StartTime).subtract(1, 'seconds');
            EndTime = moment(EndTime).add(1, 'seconds');
            var FSd = new Date(StartTime);
            var TStartDateM = moment(FSd)
                .utc()
                .format("YYYY-MM-DD HH:mm:ss");
            var ESd = new Date(EndTime);
            var TEndTimeM = moment(ESd)
                .utc()
                .format("YYYY-MM-DD HH:mm:ss");
            if (
                SearchExpression != undefined &&
                SearchExpression != null &&
                SearchExpression != ""
            ) {
                SearchExpression = "( " + SearchExpression + ") AND ";
            }
            SearchExpression +=
                "( " +
                ColumnName +
                ' > "' +
                TStartDateM +
                '" AND ' +
                ColumnName +
                ' < "' +
                TEndTimeM +
                '")';
            if (BaseString != undefined && BaseString != null && BaseString != "") {
                BaseString += " AND ";
            }
            //  BaseString += SearchExpression;
            return SearchExpression;
        } else {
            return BaseString;
        }
    }
    GetFixedDecimalNumber(Value) {
        if (Value != undefined && Value != null) {
            return Math.round((Value + Number.EPSILON) * 100) / 100;
        }
        else {
            return Value;
        }
    }
    GetPercentageFromNumber(SmallNumber, BigNumber) {
        return (SmallNumber / BigNumber) * 100;
    }
    GetAmountFromPercentage(Amount, Percentage) {
        return this.GetFixedDecimalNumber((Amount * Percentage) / 100);
    }
    PostData(NetworkLocation: string, Data: any): Observable<OResponse> {
        try {
            var DeviceInformation = this.GetStorage(this.AppConfig.StorageHelper.Device);
            var UserAccount = this.GetStorage(this.AppConfig.StorageHelper.Account);
            var PostUrl = this.AppConfig.ServerUrl + NetworkLocation + Data.Task;
            // let _Headers = new HttpHeaders();
            // var ehcak = null;
            // var ehcavk = null;
            // if (DeviceInformation != undefined) {
            //     if (DeviceInformation.OsName == "android") {
            //         ehcak = btoa(this.AppConfig.NetworkKeys.Android.AppKey);
            //         ehcavk = btoa(this.AppConfig.NetworkKeys.Android.AppVersionKey);
            //         _Headers.append("hcak", ehcak);
            //         _Headers.append("hcavk", ehcavk);
            //         _Headers.append("hcudlt", btoa(DeviceInformation.Latitude));
            //         _Headers.append("hcudln", btoa(DeviceInformation.Longitude));
            //     } else if (DeviceInformation.OsName == "ios") {
            //         ehcak = btoa(this.AppConfig.NetworkKeys.Ios.AppKey);
            //         ehcavk = btoa(this.AppConfig.NetworkKeys.Ios.AppVersionKey);
            //         _Headers.append("hcak", ehcak);
            //         _Headers.append("hcavk", ehcavk);
            //         _Headers.append("hcudlt", btoa(DeviceInformation.Latitude));
            //         _Headers.append("hcudln", btoa(DeviceInformation.Longitude));
            //     } else {
            //         ehcak = btoa(this.AppConfig.NetworkKeys.Android.AppKey);
            //         ehcavk = btoa(this.AppConfig.NetworkKeys.Android.AppVersionKey);
            //         _Headers.append("hcak", ehcak);
            //         _Headers.append("hcavk", ehcavk);
            //         _Headers.append("hcudlt", btoa(DeviceInformation.Latitude));
            //         _Headers.append("hcudln", btoa(DeviceInformation.Longitude));
            //     }
            // } else {
            //     ehcak = btoa(this.AppConfig.NetworkKeys.Android.AppKey);
            //     ehcavk = btoa(this.AppConfig.NetworkKeys.Android.AppVersionKey);
            //     _Headers.append("hcak", ehcak);
            //     _Headers.append("hcavk", ehcavk);
            //     _Headers.append("hcudlt", btoa(DeviceInformation.Latitude));
            //     _Headers.append("hcudln", btoa(DeviceInformation.Longitude));
            // }
            var ehcak = null;
            var ehcavk = null;
            ehcak = btoa(this.AppConfig.NetworkKeys.Android.AppKey);
            ehcavk = btoa(this.AppConfig.NetworkKeys.Android.AppVersionKey);
            var _Headers;
            if (UserAccount != null && UserAccount.AccessKey != undefined) {
                _Headers = new HttpHeaders()
                    .set("Content-Type", 'application/json')
                    .set("hcak", ehcak)
                    .set("hcavk", ehcavk)
                    .set("hcudlt", btoa(DeviceInformation.Latitude))
                    .set("hcudln", btoa(DeviceInformation.Longitude))
                    .set("hctk", btoa(Data.Task))
                    .set("hcudk", btoa(DeviceInformation.SerialNumber))
                    .set("hcuak", UserAccount.AccessKey)
                    .set("hcupk", btoa(UserAccount.PublicKey))
                    ;
            }
            else {
                _Headers = new HttpHeaders()
                    .set("Content-Type", 'application/json')
                    .set("hcak", ehcak)
                    .set("hcavk", ehcavk)
                    .set("hcudlt", btoa(DeviceInformation.Latitude))
                    .set("hcudln", btoa(DeviceInformation.Longitude))
                    .set("hctk", btoa(Data.Task))
                    .set("hcudk", btoa(DeviceInformation.SerialNumber));
            }
            var ORequest = JSON.stringify({ zx: btoa(JSON.stringify(Data)) });
            return this._Http
                .post<ONetworkResponse>(PostUrl, ORequest, { headers: _Headers })
                .pipe(map(_Response => JSON.parse(atob(_Response.zx)) as OResponse))
                .pipe(catchError((error: HttpErrorResponse) => throwError(error)));

        } catch (error) {
            this.HideSpinner();
            console.log(error);
        }
    }
    GetStatusClass(StatusCode) {
        var ColorGrey = "grey";
        var ColorGreen = "green";
        var ColorOrange = "orange";
        var ColorRed = "red";
        var StatusIcon = ColorGrey;
        if (StatusCode == "transaction.initialized") {
            StatusIcon = ColorGrey;
        }
        if (StatusCode == "transaction.processing") {
            StatusIcon = ColorOrange;
        }
        if (StatusCode == "transaction.pending") {
            StatusIcon = ColorOrange;
        }
        if (StatusCode == "transaction.success") {
            StatusIcon = ColorGreen;
        }
        if (StatusCode == "transaction.cancelled") {
            StatusIcon = ColorRed;
        }
        if (StatusCode == "transaction.refundinitialized") {
            StatusIcon = ColorGrey;
        }
        if (StatusCode == "transaction.refundprocessing") {
            StatusIcon = ColorGrey;
        }
        if (StatusCode == "transaction.refunded") {
            StatusIcon = ColorGrey;
        }
        if (StatusCode == "transaction.failed") {
            StatusIcon = ColorRed;
        }
        if (StatusCode == "transaction.error") {
            StatusIcon = ColorRed;
        }
        return StatusIcon;
    }
    public _RewardOverview: ORewardOverview = {
        RewardAmount: 0,
        RewardChargeAmount: 0,
        RewardPurchaseAmount: 0,
        RewardTransactions: 0,
        RewardUserAmount: 0,
        TUCPlusRewardAmount: 0,
        TUCPlusRewardChargeAmount: 0,
        TUCPlusRewardClaimedAmount: 0,
        TUCPlusRewardClaimedTransactions: 0,
        TUCPlusRewardPurchaseAmount: 0,
        TUCPlusRewardTransactions: 0,
        TUCPlusUserRewardAmount: 0
    };
    public _RewardOverviewNext: ORewardOverview = {
        RewardAmount: 0,
        RewardChargeAmount: 0,
        RewardPurchaseAmount: 0,
        RewardTransactions: 0,
        RewardUserAmount: 0,
        TUCPlusRewardAmount: 0,
        TUCPlusRewardChargeAmount: 0,
        TUCPlusRewardClaimedAmount: 0,
        TUCPlusRewardClaimedTransactions: 0,
        TUCPlusRewardPurchaseAmount: 0,
        TUCPlusRewardTransactions: 0,
        TUCPlusUserRewardAmount: 0
    };
    public _RewardTypeOverview: ORewardTypeOverview = {
        CardRewardAmount: 0,
        CardRewardPurchaseAmount: 0,
        CardRewardTransactions: 0,
        CashRewardAmount: 0,
        CashRewardPurchaseAmount: 0,
        CashRewardTransactions: 0,
        OtherRewardAmount: 0,
        OtherRewardPurchaseAmount: 0,
        OtherRewardTransactions: 0,
        RewardTypeData: [],
        RewardTypeLabels: [],

        AppUsersBank: [],
        AppUsersBankLabel: [],
        AppUsersBankDataPurchase: [],
        AppUsersBankDataTransactions: [],
        AppUsersBankDataUsers: [],

        AppUsersCardType: [],
        AppUsersCardTypeLabel: [],
        AppUsersCardTypeDataPurchase: [],
        AppUsersCardTypeDataTransactions: [],
        AppUsersCardTypeDataUsers: []
    };
    public _RewardTypeOverviewNext: ORewardTypeOverview = {
        CardRewardAmount: 0,
        CardRewardPurchaseAmount: 0,
        CardRewardTransactions: 0,
        CashRewardAmount: 0,
        CashRewardPurchaseAmount: 0,
        CashRewardTransactions: 0,
        OtherRewardAmount: 0,
        OtherRewardPurchaseAmount: 0,
        OtherRewardTransactions: 0,
        RewardTypeData: [],
        RewardTypeLabels: [],

        AppUsersBank: [],
        AppUsersBankLabel: [],
        AppUsersBankDataPurchase: [],
        AppUsersBankDataTransactions: [],
        AppUsersBankDataUsers: [],

        AppUsersCardType: [],
        AppUsersCardTypeLabel: [],
        AppUsersCardTypeDataPurchase: [],
        AppUsersCardTypeDataTransactions: [],
        AppUsersCardTypeDataUsers: []
    };
    public _RedeemOverview: ORedeemOverview = {
        RedeemAmount: 0,
        RedeemPurchaseAmount: 0,
        RedeemTransactions: 0
    };
    public _RedeemOverviewNext: ORedeemOverview = {
        RedeemAmount: 0,
        RedeemPurchaseAmount: 0,
        RedeemTransactions: 0
    };
    public _AppUsersOverview: OAppUsersOverview = {
        AppUsers: 0,
        AppUsersByAgeGroup: [],
        AppUsersCountByAgeGroup: [],
        AppUsersLabelsByAgeGroup: [],
        AppUsersPurchaseByAgeGroup: [],
        AppUsersVisitByAgeGroup: [],
        AppUsersDataByGender: [],
        AppUsersLabelsByGender: [],
        AppUsersFemale: 0,
        AppUsersMale: 0,
        AppUsersOther: 0,
        OwnAppUsers: 0,
        ReferralAppUsers: 0,
        RepeatingAppUsers: 0,
        UniqueAppUsers: 0
    };
    public _AppUsersOverviewNext: OAppUsersOverview = {
        AppUsers: 0,
        AppUsersByAgeGroup: [],
        AppUsersCountByAgeGroup: [],
        AppUsersLabelsByAgeGroup: [],
        AppUsersPurchaseByAgeGroup: [],
        AppUsersVisitByAgeGroup: [],
        AppUsersDataByGender: [],
        AppUsersLabelsByGender: [],
        AppUsersFemale: 0,
        AppUsersMale: 0,
        AppUsersOther: 0,
        OwnAppUsers: 0,
        ReferralAppUsers: 0,
        RepeatingAppUsers: 0,
        UniqueAppUsers: 0
    };
    public _UserCounts: OUserCounts =
        {
            RewardPercentage: 0,
            ThankUCashPlus: 0,
            ThankUCashPlusBalanceValidity: 0,
            ThankUCashPlusMinRedeemAmount: 0,
            ThankUCashPlusMinTransferAmount: 0,
            TotalCustomerNew: 0,
            TotalCustomerRepeating: 0,
            TotalCustomerUnique: 0,
            TotalAcquirer: 0,
            TotalCashier: 0,
            TotalCustomer: 0,
            TotalManager: 0,
            TotalMerchant: 0,
            TotalPssp: 0,
            TotalPtsp: 0,
            TotalRm: 0,
            TotalStore: 0,
            TotalTerminal: 0
        }
    public _CardTypeSalesSummary: OCardTypeSalesSummary =
        {
            CardTypeLabels: [],
            CardTypeData: [],
            CardTypes: [],
        }
    _TerminalStatusCount: OTerminalStatusCount =
        {
            Total: 0,
            Unused: 0,
            Active: 0,
            Idle: 0,
            Dead: 0,
        };

    public _SalesOverview: OSalesOverview =
        {
            TotalTransaction: 0,
            TotalTransactionCustomer: 0,
            TotalTransactionInvoiceAmount: 0,
            TotalSuccessfulTransaction: 0,
            TotalSuccessfulTransactionCustomer: 0,
            TotalSuccessfulTransactionInvoiceAmount: 0,
            TotalFailedTransaction: 0,
            TotalFailedTransactionCustomer: 0,
            TotalFailedTransactionInvoiceAmount: 0,
            TotalCardTransaction: 0,
            TotalCardTransactionInvoiceAmount: 0,
            TotalCashTransaction: 0,
            TotalCashTransactionCustomer: 0,
            TotalCashTransactionInvoiceAmount: 0,

            FirstTransactionDate: null,
            LastTransactionDate: null,
            LastTransactionDateD: '',
            TotalCardTransactionCustomer: 0,
            TransactionTypeData: [],
            TransactionTypeLabels: [],
        }
    public _SalesSummary: OSalesSummary =
        {
            TotalCustomer: 0,
            TotalTransaction: 0,
            TotalTransactionCustomer: 0,
            TotalTransactionInvoiceAmount: 0,
            TotalSuccessfulTransaction: 0,
            TotalSuccessfulTransactionCustomer: 0,
            TotalSuccessfulTransactionInvoiceAmount: 0,
            TotalFailedTransaction: 0,
            TotalFailedTransactionCustomer: 0,
            TotalFailedTransactionInvoiceAmount: 0,
            TotalCardTransaction: 0,
            TotalCardTransactionInvoiceAmount: 0,
            TotalCashTransaction: 0,
            TotalCashTransactionCustomer: 0,
            TotalCashTransactionInvoiceAmount: 0,

            FirstTransactionDate: null,
            LastTransactionDate: null,
            LastTransactionDateD: '',
            TotalCardTransactionCustomer: 0,
            TransactionTypeData: [],
            TransactionTypeLabels: [],
        }
    public _RewardsSummary: ORewardsSummary =
        {
            TotalTransaction: 0,
            TotalTransactionCustomer: 0,
            TotalTransactionInvoiceAmount: 0,

            TotalRewardTransaction: 0,
            TotalRewardTransactionCustomer: 0,
            TotalRewardTransactionAmount: 0,
            TotalRewardTransactionInvoiceAmount: 0,


            TotalTucRewardTransaction: 0,
            TotalTucRewardTransactionCustomer: 0,
            TotalTucRewardTransactionAmount: 0,
            TotalTucRewardTransactionInvoiceAmount: 0,


            TotalTucPlusRewardTransaction: 0,
            TotalTucPlusRewardTransactionCustomer: 0,
            TotalTucPlusRewardTransactionAmount: 0,
            TotalTucPlusRewardTransactionInvoiceAmount: 0,

            TotalTucPlusRewardClaimTransaction: 0,
            TotalTucPlusRewardClaimTransactionCustomer: 0,
            TotalTucPlusRewardClaimTransactionAmount: 0,
            TotalTucPlusRewardClaimTransactionInvoiceAmount: 0,

            TotalRedeemTransaction: 0,
            TotalRedeemTransactionCustomer: 0,
            TotalRedeemTransactionAmount: 0,
            TotalRedeemTransactionInvoiceAmount: 0,

            FirstTransactionDate: null,
            FirstTransactionDateD: '',
            LastTransactionDate: null,
            LastTransactionDateD: '',

            TucBalance: 0,
            TucPlusBalance: 0,
            TucBalanceCredit: 0,
            TucBalanceDebit: 0,
            TucBalanceTransaction: 0,
            TucPlusBalanceCredit: 0,
            TucPlusBalanceDebit: 0,
            TucPlusBalanceTransaction: 0,
            UserBalance: [],
        }
    public _AccountOverview: OAccountOverview = {
        TransactionFailed: 0,
        TransactionSuccess: 0,
        UnusedTerminals: 0,
        ReferredAppUsers: 0,
        ReferredAppUsersPurchase: 0,
        ReferredAppUsersVisit: 0,
        ReferredMerchantSale: 0,
        ReferredMerchantVisits: 0,
        ReferredMerchants: 0,
        ReferredReferredStores: 0,
        TerminalsOverview: [],
        StoresOverview: [],
        Merchants: 0,
        Stores: 0,
        Acquirers: 0,
        Terminals: 0,
        Ptsp: 0,
        Pssp: 0,
        Cashiers: 0,
        RewardCards: 0,
        RewardCardsUsed: 0,
        ThankUCashPlus: 0,
        ThankUCashPlusForMerchant: 0,
        ThankUCashPlusBalanceValidity: 0,
        ThankUCashPlusMinRedeemAmount: 0,
        ThankUCashPlusMinTransferAmount: 0,
        RewardPercentage: 0,
        CommissionPercentage: 0,
        Balance: 0,
        CashRewardAmount: 0,
        CashRewardPurchaseAmount: 0,
        CashRewardTransactions: 0,
        CardRewardAmount: 0,
        CardRewardPurchaseAmount: 0,
        CardRewardTransactions: 0,
        RewardTransactions: 0,
        RewardAmount: 0,
        RewardPurchaseAmount: 0,
        RewardLastTransaction: null,
        RedeemTransactions: 0,
        RedeemAmount: 0,
        RedeemPurchaseAmount: 0,
        RedeemLastTransaction: null,
        Transactions: 0,
        TransactionsPercentage: 0,
        NewTransactions: 0,
        NewTransactionsPercentage: 0,
        RepeatingTransactions: 0,
        RepeatingTransactionsPercentage: 0,
        ReferralTransactions: 0,
        ReferralTransactionsPercentage: 0,
        PurchaseAmount: 0,
        PurchaseAmountPercentage: 0,
        NewPurchaseAmount: 0,
        NewPurchaseAmountPercentage: 0,
        RepeatingPurchaseAmount: 0,
        RepeatingPurchaseAmountPercentage: 0,
        ReferralPurchaseAmount: 0,
        ReferralPurchaseAmountPercentage: 0,
        Commission: 0,
        LastCommissionDate: null,
        IssuerCommissionAmount: 0,
        LastIssuerCommissionDate: null,
        CommissionAmount: 0,
        SettlementCredit: 0,
        SettlementDebit: 0,
        AppUsers: 0,
        AppUsersPercentage: 0,
        OwnAppUsers: 0,
        OwnAppUsersPercentage: 0,
        RepeatingAppUsers: 0,
        RepeatingAppUsersPercentage: 0,
        ReferralAppUsers: 0,
        ReferralAppUsersPercentage: 0,
        AppUsersMale: 0,
        AppUsersFemale: 0,
        LastAppUserDate: null,
        LastTransaction: {
            Amount: 0,
            InvoiceAmount: 0,
            MerchantName: "",
            ReferenceId: 0,
            RewardAmount: 0,
            TransactionDate: null,
            TypeName: null
        },
        GenderLabel: [],
        GenderData: [],
        RewardTypeLabel: [],
        RewardTypeData: [],
        VisitTrendLabel: [],
        VisitTrendData: [],
        AcquirerAmountDistribution: [],
        Charge: 0,
        ClaimedReward: 0,
        ActiveTerminals: 0,
        AppUsersByAge: [],
        AppUsersOther: 0,
        AppUsersPurchaseByAge: [],
        CardRewardPurchaseAmountOther: 0,
        CardRewardTransactionsOther: 0,
        ClaimedRewardTransations: 0,
        Credit: 0,
        DeadTerminals: 0,
        Debit: 0,
        FrequentBuyers: [],
        IdleTerminals: 0,
        LastTransactionDate: null,
        MerchantOverview: [],
        OtherRewardAmount: 0,
        OtherRewardPurchaseAmount: 0,
        OtherRewardTransactions: 0,
        PosOverview: [],
        RewardChargeAmount: 0,
        RewardUserAmount: 0,
        SettlementPending: 0,
        TUCPlusBalance: 0,
        TUCPlusPurchaseAmount: 0,
        TUCPlusReward: 0,
        TUCPlusRewardAmount: 0,
        TUCPlusRewardChargeAmount: 0,
        TUCPlusRewardClaimedAmount: 0,
        TUCPlusRewardClaimedTransactions: 0,
        TUCPlusRewardPurchaseAmount: 0,
        TUCPlusRewardTransactions: 0,
        TUCPlusUserRewardAmount: 0,
        ThankUAmount: 0,
        TransactionIssuerAmountCredit: 0,
        TransactionIssuerAmountDebit: 0,
        TransactionIssuerChargeCredit: 0,
        TransactionIssuerChargeDebit: 0,
        TransactionIssuerTotalCreditAmount: 0,
        TransactionIssuerTotalDebitAmount: 0,
        UniqueAppUsers: 0,
        UserAmount: 0
    };
    public _AccountOverviewNext: OAccountOverview = {
        ReferredAppUsers: 0,
        ReferredAppUsersPurchase: 0,
        ReferredAppUsersVisit: 0,
        ReferredMerchantSale: 0,
        ReferredMerchantVisits: 0,
        ReferredMerchants: 0,
        ReferredReferredStores: 0,
        TerminalsOverview: [],
        StoresOverview: [],
        Merchants: 0,
        Stores: 0,
        Acquirers: 0,
        Terminals: 0,
        Ptsp: 0,
        Pssp: 0,
        Cashiers: 0,
        RewardCards: 0,
        RewardCardsUsed: 0,
        ThankUCashPlus: 0,
        ThankUCashPlusForMerchant: 0,
        ThankUCashPlusBalanceValidity: 0,
        ThankUCashPlusMinRedeemAmount: 0,
        ThankUCashPlusMinTransferAmount: 0,
        RewardPercentage: 0,
        CommissionPercentage: 0,
        Balance: 0,
        CashRewardAmount: 0,
        CashRewardPurchaseAmount: 0,
        CashRewardTransactions: 0,
        CardRewardAmount: 0,
        CardRewardPurchaseAmount: 0,
        CardRewardTransactions: 0,
        RewardTransactions: 0,
        RewardAmount: 0,
        RewardPurchaseAmount: 0,
        RewardLastTransaction: null,
        RedeemTransactions: 0,
        RedeemAmount: 0,
        RedeemPurchaseAmount: 0,
        RedeemLastTransaction: null,
        Transactions: 0,
        TransactionsPercentage: 0,
        NewTransactions: 0,
        NewTransactionsPercentage: 0,
        RepeatingTransactions: 0,
        RepeatingTransactionsPercentage: 0,
        ReferralTransactions: 0,
        ReferralTransactionsPercentage: 0,
        PurchaseAmount: 0,
        PurchaseAmountPercentage: 0,
        NewPurchaseAmount: 0,
        NewPurchaseAmountPercentage: 0,
        RepeatingPurchaseAmount: 0,
        RepeatingPurchaseAmountPercentage: 0,
        ReferralPurchaseAmount: 0,
        ReferralPurchaseAmountPercentage: 0,
        Commission: 0,
        LastCommissionDate: null,
        IssuerCommissionAmount: 0,
        LastIssuerCommissionDate: null,
        CommissionAmount: 0,
        SettlementCredit: 0,
        SettlementDebit: 0,
        AppUsers: 0,
        AppUsersPercentage: 0,
        OwnAppUsers: 0,
        OwnAppUsersPercentage: 0,
        RepeatingAppUsers: 0,
        RepeatingAppUsersPercentage: 0,
        ReferralAppUsers: 0,
        ReferralAppUsersPercentage: 0,
        AppUsersMale: 0,
        AppUsersFemale: 0,
        LastAppUserDate: null,
        LastTransaction: {
            Amount: 0,
            InvoiceAmount: 0,
            MerchantName: "",
            ReferenceId: 0,
            RewardAmount: 0,
            TransactionDate: null,
            TypeName: null
        },
        GenderLabel: [],
        GenderData: [],
        RewardTypeLabel: [],
        RewardTypeData: [],
        VisitTrendLabel: [],
        VisitTrendData: [],
        AcquirerAmountDistribution: [],
        Charge: 0,
        ClaimedReward: 0,
        ActiveTerminals: 0,
        AppUsersByAge: [],
        AppUsersOther: 0,
        AppUsersPurchaseByAge: [],
        CardRewardPurchaseAmountOther: 0,
        CardRewardTransactionsOther: 0,
        ClaimedRewardTransations: 0,
        Credit: 0,
        DeadTerminals: 0,
        Debit: 0,
        FrequentBuyers: [],
        IdleTerminals: 0,
        LastTransactionDate: null,
        MerchantOverview: [],
        OtherRewardAmount: 0,
        OtherRewardPurchaseAmount: 0,
        OtherRewardTransactions: 0,
        PosOverview: [],
        RewardChargeAmount: 0,
        RewardUserAmount: 0,
        SettlementPending: 0,
        TUCPlusBalance: 0,
        TUCPlusPurchaseAmount: 0,
        TUCPlusReward: 0,
        TUCPlusRewardAmount: 0,
        TUCPlusRewardChargeAmount: 0,
        TUCPlusRewardClaimedAmount: 0,
        TUCPlusRewardClaimedTransactions: 0,
        TUCPlusRewardPurchaseAmount: 0,
        TUCPlusRewardTransactions: 0,
        TUCPlusUserRewardAmount: 0,
        ThankUAmount: 0,
        TransactionIssuerAmountCredit: 0,
        TransactionIssuerAmountDebit: 0,
        TransactionIssuerChargeCredit: 0,
        TransactionIssuerChargeDebit: 0,
        TransactionIssuerTotalCreditAmount: 0,
        TransactionIssuerTotalDebitAmount: 0,
        UniqueAppUsers: 0,
        UserAmount: 0
    };
    _UserOverviewPlot = {
        // PieChartPlugins: [pluginDataLabels],
        StartTime: null,
        EndTime: null,
        GuageLight: {
            Type: "arch",
            Append: "%",
            Cap: "round",
            Thick: 6,
            Size: 55,
            Color: "#ffffff"
        },
        PlotChartHeight: 95,
        ChartTypeDoughnet: "doughnut",
        ChartTypeBar: "bar",
        ChartTypeBarHorizontal: "horizontalBar",
        ChartTypeBarHOptions: {
            layout: {
                padding: {
                    right: 16
                }
            },
            responsive: true,
            legend: {
                display: false
            },
            ticks: {
                autoSkip: false
            },
            scales: {
                xAxes: [
                    {
                        categoryPercentage: 0.1,
                        barPercentage: 0.1,
                        gridLines: {
                            stacked: true,
                            display: false
                        },
                        ticks: {
                            autoSkip: false,
                            fontSize: 11
                        }
                    }
                ],
                yAxes: [
                    {
                        gridLines: {
                            stacked: true,
                            display: false
                        }
                    }
                ]
            },
            plugins: {
                datalabels: {
                    backgroundColor: "transparent",
                    color: "#000000bf",
                    borderRadius: "2",
                    borderWidth: "1",
                    borderColor: "transparent",
                    anchor: "end",
                    align: "end",
                    padding: 2,
                    font: {
                        size: 10,
                        weight: 500
                    },
                    formatter: (value, ctx) => {
                        const label = ctx.chart.data.labels[ctx.dataIndex];
                        if (label != undefined) {
                            return value;
                        } else {
                            return value;
                        }
                    }
                }
            }
        },
        ChartTypeBarOptions: {
            layout: {
                padding: {
                    top: 12
                }
            },
            responsive: true,
            legend: {
                display: false
            },
            ticks: {
                autoSkip: false
            },
            scales: {
                xAxes: [
                    {
                        categoryPercentage: 0.98,
                        barPercentage: 0.98,
                        gridLines: {
                            stacked: true,
                            display: false
                        },
                        ticks: {
                            autoSkip: false,
                            fontSize: 11
                        }
                    }
                ],
                yAxes: [
                    {
                        gridLines: {
                            stacked: true,
                            display: false
                        }
                    }
                ]
            },
            plugins: {
                datalabels: {
                    backgroundColor: "transparent",
                    color: "#000000bf",
                    borderRadius: "2",
                    borderWidth: "1",
                    borderColor: "transparent",
                    anchor: "end",
                    align: "end",
                    padding: 2,
                    font: {
                        size: 10,
                        weight: 500
                    },
                    formatter: (value, ctx) => {
                        const label = ctx.chart.data.labels[ctx.dataIndex];
                        if (label != undefined) {
                            return value;
                        } else {
                            return value;
                        }
                    }
                }
            }
        },
        ChartTypeBarOptionsStr: {
            // layout: {
            //     padding: {
            //         top: 12
            //     }
            // },
            responsive: true,
            maintainAspectRatio: false,
            legend: {
                display: false
            },
            ticks: {
                autoSkip: false
            },
            scales: {
                xAxes: [
                    {
                        categoryPercentage: 0.1,
                        barPercentage: 0.85,
                        barThickness: 16,  // number (pixels) or 'flex'
                        maxBarThickness: 16, // number (pixels),
                        gridLines: {
                            stacked: true,
                            display: false
                        },
                        ticks: {
                            fontColor: "#303F9F",
                            autoSkip: false,
                            fontSize: 9
                        }
                    }
                ],
                yAxes: [
                    {
                        display: false,
                        ticks: {
                            display: false
                        },
                        gridLines: {
                            drawBorder: false,
                            stacked: true,
                            display: false
                        }
                    }
                ]
            },
            plugins: {
                datalabels: {
                    backgroundColor: "transparent",
                    color: "#000000bf",
                    borderRadius: "2",
                    borderWidth: "1",
                    borderColor: "transparent",
                    anchor: "end",
                    align: "end",
                    padding: 2,
                    font: {
                        size: 10,
                        weight: 500
                    },
                    formatter: (value, ctx) => {
                        const label = ctx.chart.data.labels[ctx.dataIndex];
                        if (label != undefined) {
                            return value;
                        } else {
                            return value;
                        }
                    }
                }
            }
        },
        ChartTypeBarOptionsInverse: {
            // layout: {
            //     padding: {
            //         top: 12
            //     }
            // },
            responsive: true,
            legend: {
                display: false
            },
            ticks: {
                autoSkip: false
            },
            scales: {
                xAxes: [
                    {
                        categoryPercentage: 0.85,
                        barPercentage: 0.85,
                        gridLines: {
                            stacked: true,
                            display: false
                        },
                        ticks: {
                            fontColor: "white",
                            autoSkip: false,
                            fontSize: 9
                        }
                    }
                ],
                yAxes: [
                    {
                        display: false,
                        ticks: {
                            display: false
                        },
                        gridLines: {
                            drawBorder: false,
                            stacked: true,
                            display: false
                        }
                    }
                ]
            },
            plugins: {
                datalabels: {
                    backgroundColor: "transparent",
                    color: "#000000bf",
                    borderRadius: "2",
                    borderWidth: "1",
                    borderColor: "transparent",
                    anchor: "end",
                    align: "end",
                    padding: 2,
                    font: {
                        size: 10,
                        weight: 500
                    },
                    formatter: (value, ctx) => {
                        const label = ctx.chart.data.labels[ctx.dataIndex];
                        if (label != undefined) {
                            return value;
                        } else {
                            return value;
                        }
                    }
                }
            }
        },
        ChartTypePieOptions: {
            responsive: true,
            plugins: {
                datalabels: {
                    backgroundColor: "transparent",
                    color: "#000000bf",
                    borderRadius: "2",
                    borderWidth: "1",
                    borderColor: "transparent",
                    anchor: "end",
                    align: "end",
                    padding: 2,
                    display: true,
                    font: {
                        size: 10,
                        weight: 500
                    },
                    formatter: (value, ctx) => {
                        let sum = 0;
                        let dataArr = ctx.chart.data.datasets[0].data;
                        dataArr.map(data => {
                            sum += data;
                        });
                        let percentage = ((value * 100) / sum).toFixed(2) + "%";
                        // return percentage;
                        const label = ctx.chart.data.labels[ctx.dataIndex];
                        if (label != undefined) {
                            return label + "\n" + percentage;
                            // return label;
                        } else {
                            return value;
                        }
                    }
                }
            },
            legend: {
                display: true,
                position: "bottom"
            },
            legendCallback: function (chart) {
                // Return the HTML string here.
            }
        },
        Colors: [
            {
                backgroundColor: "#303F9F"
            },
            {
                backgroundColor: "rgb(0, 194, 146)"
            },
            {
                backgroundColor: "rgb(171, 140, 228)"
            },
            {
                backgroundColor: "rgb(255, 180, 99)"
            },
            {
                backgroundColor: "rgb(3, 169, 243)"
            },
            {
                backgroundColor: "rgb(251, 150, 120)"
            }
        ],
        ColorsInverse: [
            {
                backgroundColor: "rgba(255, 255, 255, 0.95)"
            },
            {
                backgroundColor: "rgb(0, 194, 146)"
            },
            {
                backgroundColor: "rgb(171, 140, 228)"
            },
            {
                backgroundColor: "rgb(255, 180, 99)"
            },
            {
                backgroundColor: "rgb(3, 169, 243)"
            },
            {
                backgroundColor: "rgb(251, 150, 120)"
            }
        ],
        PieColors: [
            {
                backgroundColor: [
                    "rgb(0, 194, 146)",
                    "rgb(171, 140, 228)",
                    "rgb(255, 180, 99)",
                    "rgb(3, 169, 243)",
                    "rgb(251, 150, 120)"
                ]
            }
        ],
        ChartTypePlotOptions: {
            fill: false,
            scales: {
                xAxes: [
                    {
                        gridLines: {
                            stacked: true,
                            display: false
                        }
                    }
                ],
                yAxes: [
                    {
                        gridLines: {
                            stacked: true,
                            display: false
                        }
                    }
                ]
            },
            legend: {
                display: true,
                position: "bottom"
            },
            plugins: {
                datalabels: {
                    backgroundColor: "transparent",
                    color: "#000000bf",
                    borderRadius: "2",
                    borderWidth: "1",
                    borderColor: "transparent",
                    anchor: "end",
                    align: "end",
                    padding: 2,
                    font: {
                        size: 11,
                        weight: 500
                    },
                    formatter: (value, ctx) => {
                        const label = ctx.chart.data.labels[ctx.dataIndex];
                        if (label != undefined) {
                            return value;
                        } else {
                            return value;
                        }
                    }
                }
            }
        },
        RewardTypeLabel: [],
        RewardTypeData: [],
        GenderLabel: [],
        GenderData: [],
        RegSourceLabel: [],
        RegSourceData: [],
        VisitorLabel: [],
        VisitorData: [],
        RewardLabel: [],
        RewardData: [],
        RedeemData: [],
        RedeemLabel: [],
        PaymentData: [],
        PaymentLabel: [],
        RewardPlotLabel: [],
        RewardPlotDataSet: [],
        RedeemPlotLabel: [],
        RedeemPlotDataSet: [],
        VisitorsPlotLabel: [],
        VisitorsPlotDataSet: [],
        SalePlotLabel: [],
        SalePlotDataSet: [],
        CardUserPlotLabel: [],
        CardUserPlotDataSet: [],
        PaymentsPlotLabel: [],
        PaymentsPlotDataSet: [],
        HourlyVisitLabel: [],
        HourlyVisitData: [],
        HourlySalesData: [],
        HourlyVisitNextLabel: [],
        HourlyVisitNextData: []
    };
    public ChartConfig =
        {
            SalesTypeColors: [
                {
                    backgroundColor: [
                        "#61c03c",
                        "#ff9900",
                    ]
                }
            ],
            SalesTypeCustomersColors: [
                {
                    backgroundColor: [
                        "#20ab68",
                        "#303F9F",
                    ]
                }
            ],
            TransactionStatusDataSetColors: [
                {
                    backgroundColor: [
                        "#61c03c"
                    ],
                },
                {
                    backgroundColor: [
                        "#dc3912"
                    ],
                }
            ],
            TransactionStatusColors: [
                {
                    backgroundColor: [
                        "#61c03c",
                        "#dc3912",
                    ]
                }
            ],
            CardTypeColors: [
                {
                    backgroundColor: [
                        "#ff9900",
                        "#00aff0",
                        "#dc3912",
                    ]
                }
            ],
            // Plugins: [pluginDataLabels],
            DoughnetChart:
            {
                Type: 'doughnut',
                Options:
                {
                    responsive: true,
                    plugins: {
                        datalabels: {
                            backgroundColor: "#fff",
                            color: "#000000bf",
                            borderRadius: "2",
                            borderWidth: "1",
                            borderColor: "transparent",
                            anchor: "center",
                            align: "center",
                            padding: 2,
                            opacity: 0.9,
                            display: true,
                            font: {
                                size: 10,
                                weight: 500
                            },
                            formatter: (value, ctx) => {
                                let sum = 0;
                                let dataArr = ctx.chart.data.datasets[0].data;
                                dataArr.map(data => {
                                    sum += data;
                                });
                                let percentage = ((value * 100) / sum).toFixed(2) + "%";
                                // return percentage;
                                const label = ctx.chart.data.labels[ctx.dataIndex];
                                if (label != undefined) {
                                    return label + "\n" + percentage;
                                    // return label;
                                } else {
                                    return value;
                                }
                            }
                        }
                    },
                    legend: {
                        display: true,
                        position: "bottom"
                    },
                    legendCallback: function (chart) {
                        // Return the HTML string here.
                    }
                }

            },
            LineChart:
            {
                layout: {
                    padding: {
                        top: 16
                    }
                },
                Type: "line",
                Options: {

                    responsive: true,
                    legend: {
                        display: true,
                        position: 'bottom',
                    },
                    ticks: {
                        autoSkip: false
                    },
                    scales: {
                        xAxes: [
                            {
                                // categoryPercentage: 0.98,
                                // barPercentage: 0.98,
                                maxBarThickness: 50,
                                categoryPercentage: 0.4,
                                barPercentage: 0.4,
                                gridLines: {
                                    stacked: true,
                                    display: false
                                },
                                ticks: {
                                    autoSkip: false,
                                    fontSize: 11
                                }
                            }
                        ],
                        yAxes: [
                            {
                                maxBarThickness: 50,
                                categoryPercentage: 0.4,
                                barPercentage: 0.4,
                                gridLines: {
                                    stacked: true,
                                    display: false
                                },
                                ticks: {
                                    beginAtZero: true,
                                    fontSize: 11
                                }
                            }
                        ]
                    },
                    plugins: {
                        datalabels: {
                            backgroundColor: "#ffffff47",
                            color: "#798086",
                            borderRadius: "2",
                            borderWidth: "1",
                            borderColor: "transparent",
                            anchor: "end",
                            align: "end",
                            padding: 2,
                            font: {
                                size: 9,
                                weight: 400
                            },
                            formatter: (value, ctx) => {
                                const label = ctx.chart.data.labels[ctx.dataIndex];
                                if (label != undefined) {
                                    return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                } else {
                                    return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                }
                            }
                        }
                    }
                },
            },
            BarChart:
            {
                layout: {
                    padding: {
                        top: 16
                    }
                },
                Type: "bar",
                Options: {
                    cornerRadius: 20,
                    responsive: true,
                    legend: {
                        display: true,
                        position: 'right',
                    },
                    ticks: {
                        autoSkip: false
                    },
                    scales: {
                        xAxes: [
                            {
                                // stacked: true,
                                // categoryPercentage: 0.98,
                                // barPercentage: 0.98,
                                // maxBarThickness: 30,
                                // categoryPercentage: 0.4,
                                // barPercentage: 0.4,
                                gridLines: {
                                    stacked: true,
                                    display: false
                                },
                                ticks: {
                                    autoSkip: false,
                                    fontSize: 11
                                }
                            }
                        ],
                        yAxes: [
                            {
                                // maxBarThickness: 30,
                                // categoryPercentage: 0.4,
                                // barPercentage: 0.4,
                                // stacked: true,
                                gridLines: {
                                    stacked: true,
                                    display: false
                                },
                                ticks: {
                                    beginAtZero: true,
                                    fontSize: 11
                                }
                            }
                        ]
                    },
                    plugins: {
                        datalabels: {
                            backgroundColor: "#ffffff47",
                            color: "#798086",
                            borderRadius: "2",
                            borderWidth: "1",
                            borderColor: "transparent",
                            anchor: "end",
                            align: "end",
                            padding: 2,
                            font: {
                                size: 10,
                                weight: 500
                            },
                            formatter: (value, ctx) => {
                                const label = ctx.chart.data.labels[ctx.dataIndex];
                                if (label != undefined) {
                                    return value;
                                } else {
                                    return value;
                                }
                            }
                        }
                    }
                },
            }
        }
    GetTerminalStatusCount(UserAccountId, UserAccountTypeId, SubUserAccountId, SubUserAccountTypeId) {
        this.IsFormProcessing = true;
        var Data = {
            Task: "getterminalstatuscount",
            UserAccountId: UserAccountId,
            UserAccountTypeId: UserAccountTypeId,
            SubUserAccountId: SubUserAccountId,
            SubUserAccountTypeId: SubUserAccountTypeId,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this.PostData(this.AppConfig.Network.V2.TUCAnalytics, Data);
        _OResponse.subscribe(
            _Response => {
                this.IsFormProcessing = false;
                if (_Response.Status == this.AppConfig.StatusSuccess) {
                    this._TerminalStatusCount = _Response.Result as OTerminalStatusCount;

                } else {
                    this.NotifySimple(_Response.Message);
                }
            },
            _Error => {
                this.IsFormProcessing = false;
                this.HandleException(_Error);
            }
        );
    }
    GetUserCounts(UserAccountId, UserAccountTypeId, SubUserAccountId, SubUserAccountTypeId, StartTime, EndTime) {
        this.IsFormProcessing = true;
        var Data = {
            Task: "getusercounts",
            StartTime: StartTime, // new Date(2017, 0, 1, 0, 0, 0, 0),
            EndTime: EndTime, // moment().add(2, 'days'),
            UserAccountId: UserAccountId,
            UserAccountTypeId: UserAccountTypeId,
            SubUserAccountId: SubUserAccountId,
            SubUserAccountTypeId: SubUserAccountTypeId,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this.PostData(this.AppConfig.Network.V2.TUCAnalytics, Data);
        _OResponse.subscribe(
            _Response => {
                this.IsFormProcessing = false;
                if (_Response.Status == this.AppConfig.StatusSuccess) {
                    this._UserCounts = _Response.Result as OUserCounts;
                    if (
                        this._AccountOverview.SettlementCredit != undefined &&
                        this._AccountOverview.SettlementDebit != undefined
                    ) {
                        this._AccountOverview.SettlementPending =
                            this._AccountOverview.SettlementCredit -
                            this._AccountOverview.SettlementDebit;
                    }
                } else {
                    this.NotifySimple(_Response.Message);
                }
            },
            _Error => {
                this.IsFormProcessing = false;
                this.HandleException(_Error);
            }
        );
    }
    GetSalesOverview(UserAccountId, UserAccountTypeId, SubUserAccountId, SubUserAccountTypeId) {
        this.IsFormProcessing = true;
        this._SalesOverview.TransactionTypeLabels = [];
        this._SalesOverview.TransactionTypeData = [];
        this._SalesOverview.TransactionTypeUsersLabels = [];
        this._SalesOverview.TransactionTypeUsersData = [];
        this._SalesOverview.TransactionStatusLabels = [];
        this._SalesOverview.TransactionStatusData = [];

        this._SalesOverview.TransactionTypeSalesLabels = [];
        this._SalesOverview.TransactionTypeSalesData = [];

        var Data = {
            Task: "getsalesoverview",
            UserAccountId: UserAccountId,
            UserAccountTypeId: UserAccountTypeId,
            SubUserAccountId: SubUserAccountId,
            SubUserAccountTypeId: SubUserAccountTypeId,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this.PostData(this.AppConfig.Network.V2.TUCAnalytics, Data);
        _OResponse.subscribe(
            _Response => {
                this.IsFormProcessing = false;
                if (_Response.Status == this.AppConfig.StatusSuccess) {
                    this._SalesOverview = _Response.Result as OSalesOverview;
                    this._SalesOverview.FirstTransactionDate = this.GetDateTimeS(this._SalesOverview.FirstTransactionDate);
                    this._SalesOverview.LastTransactionDateD = this.GetTimeDifferenceS(this._SalesOverview.LastTransactionDate, moment());
                    this._SalesOverview.LastTransactionDate = this.GetDateTimeS(this._SalesOverview.LastTransactionDate);

                    this._SalesOverview.TransactionTypeUsersLabels = [];
                    this._SalesOverview.TransactionTypeUsersData = [];
                    this._SalesOverview.TransactionTypeUsersLabels.push("Cash Tr Users");
                    this._SalesOverview.TransactionTypeUsersLabels.push("Card Tr. Users");
                    this._SalesOverview.TransactionTypeUsersData.push(
                        this._SalesOverview.TotalCashTransactionCustomer
                    );
                    this._SalesOverview.TransactionTypeUsersData.push(
                        this._SalesOverview.TotalCardTransactionCustomer
                    );

                    this._SalesOverview.TransactionTypeSalesLabels = [];
                    this._SalesOverview.TransactionTypeSalesData = [];
                    this._SalesOverview.TransactionTypeSalesLabels.push("Cash Payments Customers");
                    this._SalesOverview.TransactionTypeSalesLabels.push("Card Payments Customers");
                    this._SalesOverview.TransactionTypeSalesData.push(
                        this._SalesOverview.TotalCashTransactionInvoiceAmount
                    );
                    this._SalesOverview.TransactionTypeSalesData.push(
                        this._SalesOverview.TotalCardTransactionInvoiceAmount
                    );


                    this._SalesOverview.TransactionTypeLabels = [];
                    this._SalesOverview.TransactionTypeData = [];
                    this._SalesOverview.TransactionTypeLabels.push("Cash Transactions");
                    this._SalesOverview.TransactionTypeLabels.push("Card Transactions");
                    this._SalesOverview.TransactionTypeData.push(
                        this._SalesOverview.TotalCashTransaction
                    );
                    this._SalesOverview.TransactionTypeData.push(
                        this._SalesOverview.TotalCardTransaction
                    );


                    this._SalesOverview.TransactionStatusLabels = [];
                    this._SalesOverview.TransactionStatusData = [];
                    this._SalesOverview.TransactionStatusLabels.push("Success");
                    this._SalesOverview.TransactionStatusLabels.push("Failed");
                    this._SalesOverview.TransactionStatusData.push(
                        this._SalesOverview.TotalSuccessfulTransaction
                    );
                    this._SalesOverview.TransactionStatusData.push(
                        this._SalesOverview.TotalFailedTransaction
                    );
                } else {
                    this.NotifySimple(_Response.Message);
                }
            },
            _Error => {
                this.IsFormProcessing = false;
                this.HandleException(_Error);
            }
        );
    }
    GetSalesSummary(UserAccountId, UserAccountTypeId, SubUserAccountId, SubUserAccountTypeId, StartTime, EndTime) {
        this.IsFormProcessing = true;
        this._SalesSummary.TransactionTypeLabels = [];
        this._SalesSummary.TransactionTypeData = [];
        this._SalesSummary.TransactionTypeUsersLabels = [];
        this._SalesSummary.TransactionTypeUsersData = [];
        this._SalesSummary.TransactionStatusLabels = [];
        this._SalesSummary.TransactionStatusData = [];
        this._SalesSummary.TransactionTypeSalesLabels = [];
        this._SalesSummary.TransactionTypeSalesData = [];
        var Data = {
            Task: "getsalessummary",
            StartTime: StartTime,
            EndTime: EndTime,
            UserAccountId: UserAccountId,
            UserAccountTypeId: UserAccountTypeId,
            SubUserAccountId: SubUserAccountId,
            SubUserAccountTypeId: SubUserAccountTypeId,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this.PostData(this.AppConfig.Network.V2.TUCAnalytics, Data);
        _OResponse.subscribe(
            _Response => {
                this.IsFormProcessing = false;
                if (_Response.Status == this.AppConfig.StatusSuccess) {
                    this._SalesSummary = _Response.Result as OSalesSummary;
                    this._SalesSummary.FirstTransactionDate = this.GetDateTimeS(this._SalesSummary.FirstTransactionDate);
                    this._SalesSummary.LastTransactionDateD = this.GetTimeDifferenceS(this._SalesSummary.LastTransactionDate, moment());
                    this._SalesSummary.LastTransactionDate = this.GetDateTimeS(this._SalesSummary.LastTransactionDate);

                    this._SalesSummary.TransactionTypeUsersLabels = [];
                    this._SalesSummary.TransactionTypeUsersData = [];
                    this._SalesSummary.TransactionTypeUsersLabels.push("Cash");
                    this._SalesSummary.TransactionTypeUsersLabels.push("Card");
                    this._SalesSummary.TransactionTypeUsersData.push(
                        this._SalesSummary.TotalCashTransactionCustomer
                    );
                    this._SalesSummary.TransactionTypeUsersData.push(
                        this._SalesSummary.TotalCardTransactionCustomer
                    );

                    this._SalesSummary.TransactionTypeSalesLabels = [];
                    this._SalesSummary.TransactionTypeSalesData = [];
                    this._SalesSummary.TransactionTypeSalesLabels.push("Cash Payments");
                    this._SalesSummary.TransactionTypeSalesLabels.push("Card Payments");
                    this._SalesSummary.TransactionTypeSalesData.push(
                        this._SalesSummary.TotalCashTransactionInvoiceAmount
                    );
                    this._SalesSummary.TransactionTypeSalesData.push(
                        this._SalesSummary.TotalCardTransactionInvoiceAmount
                    );

                    this._SalesSummary.TransactionTypeLabels = [];
                    this._SalesSummary.TransactionTypeData = [];
                    this._SalesSummary.TransactionTypeLabels.push("Cash Transactions");
                    this._SalesSummary.TransactionTypeLabels.push("Card Transactions");
                    this._SalesSummary.TransactionTypeData.push(
                        this._SalesSummary.TotalCashTransaction
                    );
                    this._SalesSummary.TransactionTypeData.push(
                        this._SalesSummary.TotalCardTransaction
                    );


                    this._SalesSummary.TransactionStatusLabels = [];
                    this._SalesSummary.TransactionStatusData = [];
                    this._SalesSummary.TransactionStatusLabels.push("Success");
                    this._SalesSummary.TransactionStatusLabels.push("Failed");
                    this._SalesSummary.TransactionStatusData.push(
                        this._SalesSummary.TotalSuccessfulTransaction
                    );
                    this._SalesSummary.TransactionStatusData.push(
                        this._SalesSummary.TotalFailedTransaction
                    );

                    this.gettransactiondetails();

                } else {
                    this.NotifySimple(_Response.Message);
                }
            },
            _Error => {
                this.IsFormProcessing = false;
                this.HandleException(_Error);
            }
        );


    }
    public gettransactiondetails() {
        let chartData = [];
        let chartDataobj = {};
        let failuredataobj = {};
        let successdataobj = {};
        let failuretransaction = [];
        let succeedtransaction = [];
        failuretransaction.push(this._SalesSummary.TotalFailedTransaction);
        failuretransaction.push(this._SalesSummary.TotalFailedTransactionInvoiceAmount);
        succeedtransaction.push(this._SalesSummary.TotalSuccessfulTransaction);
        succeedtransaction.push(this._SalesSummary.TotalSuccessfulTransactionInvoiceAmount);
        chartDataobj["data"] = [20, 40];
        failuredataobj["failure"] = chartDataobj["data"]
        chartData.push(chartDataobj);
        return chartData;
    }
    public IsFormProcessing = false;
    GetRewardsSummary(UserAccountId, UserAccountTypeId, SubUserAccountId, SubUserAccountTypeId, StartTime, EndTime) {
        this.IsFormProcessing = true;
        var Data = {
            Task: "getrewardssummary",
            StartTime: StartTime,
            EndTime: EndTime,
            UserAccountId: UserAccountId,
            UserAccountTypeId: UserAccountTypeId,
            SubUserAccountId: SubUserAccountId,
            SubUserAccountTypeId: SubUserAccountTypeId,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this.PostData(this.AppConfig.Network.V2.TUCAnalytics, Data);
        _OResponse.subscribe(
            _Response => {
                this.IsFormProcessing = false;
                if (_Response.Status == this.AppConfig.StatusSuccess) {
                    this._RewardsSummary = _Response.Result as ORewardsSummary;
                    this._RewardsSummary.FirstTransactionDateD = this.GetDateTimeS(this._RewardsSummary.FirstTransactionDate);
                    this._RewardsSummary.FirstTransactionDate = this.GetDateTimeS(this._RewardsSummary.FirstTransactionDate);
                    this._RewardsSummary.LastTransactionDateD = this.GetTimeDifferenceS(this._RewardsSummary.LastTransactionDate, moment());
                    this._RewardsSummary.LastTransactionDate = this.GetDateTimeS(this._RewardsSummary.LastTransactionDate);
                } else {
                    this.NotifySimple(_Response.Message);
                }
            },
            _Error => {
                this.IsFormProcessing = false;
                this.HandleException(_Error);
            }
        );
    }
    public _OSalesHistory =
        {
            Labels: [],
            SaleColors: [],
            SaleDataSet: [],

            SaleCustomersColors: [],
            SaleCustomersDataSet: [],

            TransactionStatusDataSetColors: [],
            TransactionStatusDataSet: [],

            TransactionTypeDataSetColors: [],
            TransactionTypeDataSet: [],

            TransactionTypeCustomersDataSetColors: [],
            TransactionTypeCustomersDataSet: [],
        }
    GetSalesHistory(UserAccountId, UserAccountTypeId, SubUserAccountId, SubUserAccountTypeId, StartTime, EndTime) {
        this.IsFormProcessing = true;
        var Data = {
            Task: "getsaleshistory",
            StartTime: StartTime,
            EndTime: EndTime,
            UserAccountId: UserAccountId,
            UserAccountTypeId: UserAccountTypeId,
            SubUserAccountId: SubUserAccountId,
            SubUserAccountTypeId: SubUserAccountTypeId,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this.PostData(this.AppConfig.Network.V2.TUCAnalytics, Data);
        _OResponse.subscribe(
            _Response => {
                this.IsFormProcessing = false;
                if (_Response.Status == this.AppConfig.StatusSuccess) {
                    var TStatusDataSet = [];
                    var TStatusDataSetColor = [];
                    this._OSalesHistory.Labels = [];
                    var ResponseData = _Response.Result;
                    var TSaleColor = [{
                        backgroundColor: [],
                    }];
                    var TSaleDataSet = [{
                        label: 'Sale',
                        fill: false,
                        borderColor: "#907eec",
                        borderDash: [5, 5],
                        backgroundColor: "#907eec",
                        pointBackgroundColor: "#fdab29",
                        pointBorderColor: "#fdab29",
                        pointHoverBackgroundColor: "#fdab29",
                        pointHoverBorderColor: "#fdab29",
                        data: []
                    }];
                    var TSaleCustomersColor = [{
                        backgroundColor: [],
                    }];
                    var TSaleCustomersDataSet = [{
                        label: 'Customer Visits',
                        fill: false,
                        borderColor: "#907eec",
                        borderDash: [5, 5],
                        backgroundColor: "#907eec",
                        pointBackgroundColor: "#fdab29",
                        pointBorderColor: "#fdab29",
                        pointHoverBackgroundColor: "#fdab29",
                        pointHoverBorderColor: "#fdab29",
                        data: [],
                        dataDate: [],
                    }];
                    var TTypeDataSetColor = [];
                    var TTypeDataSet = [];
                    var TTypeCustomersDataSetColor = [];
                    var TTypeCustomersDataSet = [];
                    var TTypeDataSetCardItemColor = {
                        backgroundColor: [],
                    };
                    var TTypeDataSetCashItemColor = {
                        backgroundColor: [],
                    };
                    var TTypeCustomersDataSetCardItemColor = {
                        backgroundColor: [],
                    };
                    var TTypeCustomersDataSetCashItemColor = {
                        backgroundColor: [],
                    };
                    var TTypeDataSetCardItem = {
                        label: 'Card',
                        backgroundColor: [],
                        data: []
                    };
                    var TTypeDataSetCashItem = {
                        label: 'Cash',
                        backgroundColor: [],
                        data: []
                    };
                    var TTypeCustomersDataSetCardItem = {
                        label: 'Card Customers',
                        backgroundColor: [],
                        data: []
                    };
                    var TTypeCustomersDataSetCashItem = {
                        label: 'Cash Customers',
                        backgroundColor: [],
                        data: []
                    };
                    var TStatusDataSetSuccessItemColor = {
                        backgroundColor: [],
                    };
                    var TStatusDataSetErrorItemError = {
                        backgroundColor: [],
                    };
                    var TStatusDataSetSuccessItem = {
                        label: 'Success',
                        backgroundColor: [],
                        data: []
                    };
                    var TStatusDataSetErrorItem = {
                        label: 'Error',
                        backgroundColor: [],
                        data: []
                    };
                    ResponseData.forEach(element => {
                        var Data = element.Data;
                        if (ResponseData.length < 8) {
                            this._OSalesHistory.Labels.push(this.FormatDate(element.Date, "ddd"));
                        }
                        else {
                            this._OSalesHistory.Labels.push(this.GetDateMonthS(element.Date));
                        }
                        TSaleDataSet[0].data.push(Math.round(Data.TotalSuccessfulTransactionInvoiceAmount));
                        TSaleColor[0].backgroundColor.push("rgba(97, 192, 60, 0.7)");
                        TSaleCustomersDataSet[0].dataDate.push(this.GetDateS(element.Date));
                        TSaleCustomersDataSet[0].data.push(Data.TotalSuccessfulTransactionCustomer);
                        TSaleCustomersColor[0].backgroundColor.push("rgba(153, 0, 153, 0.7)");
                        TTypeDataSetCardItem.data.push(Math.round(Data.TotalCardTransactionInvoiceAmount));
                        TTypeDataSetCardItemColor.backgroundColor.push("#61c03c");
                        TTypeDataSetCashItem.data.push(Math.round(Data.TotalCashTransactionInvoiceAmount));
                        TTypeDataSetCashItemColor.backgroundColor.push("#ff9900");
                        TTypeCustomersDataSetCardItem.data.push(Data.TotalCardTransactionCustomer);
                        TTypeCustomersDataSetCardItemColor.backgroundColor.push("#990099");
                        TTypeCustomersDataSetCashItem.data.push(Data.TotalCashTransactionCustomer);
                        TTypeCustomersDataSetCashItemColor.backgroundColor.push("#00aff0");
                        TStatusDataSetSuccessItem.data.push(Data.TotalSuccessfulTransaction);
                        TStatusDataSetSuccessItemColor.backgroundColor.push("#61c03c");
                        TStatusDataSetErrorItem.data.push(Data.TotalFailedTransaction);
                        TStatusDataSetErrorItemError.backgroundColor.push("#dc3912");
                    });
                    TStatusDataSetColor.push(TStatusDataSetSuccessItemColor);
                    TStatusDataSetColor.push(TStatusDataSetErrorItemError);
                    TStatusDataSet.push(TStatusDataSetSuccessItem);
                    TStatusDataSet.push(TStatusDataSetErrorItem);
                    TTypeDataSetColor.push(TTypeDataSetCardItemColor);
                    TTypeDataSetColor.push(TTypeDataSetCashItemColor);
                    TTypeDataSet.push(TTypeDataSetCardItem);
                    TTypeDataSet.push(TTypeDataSetCashItem);
                    TTypeCustomersDataSetColor.push(TTypeCustomersDataSetCardItemColor);
                    TTypeCustomersDataSetColor.push(TTypeCustomersDataSetCashItemColor);
                    TTypeCustomersDataSet.push(TTypeCustomersDataSetCardItem);
                    TTypeCustomersDataSet.push(TTypeCustomersDataSetCashItem);
                    this._OSalesHistory.TransactionStatusDataSet = TStatusDataSet;
                    this._OSalesHistory.TransactionStatusDataSetColors = TStatusDataSetColor;
                    this._OSalesHistory.TransactionTypeDataSet = TTypeDataSet;
                    this._OSalesHistory.TransactionTypeDataSetColors = TTypeDataSetColor;
                    this._OSalesHistory.TransactionTypeCustomersDataSet = TTypeCustomersDataSet;
                    this._OSalesHistory.TransactionTypeCustomersDataSetColors = TTypeCustomersDataSetColor;
                    this._OSalesHistory.SaleDataSet = TSaleDataSet;
                    this._OSalesHistory.SaleColors = TSaleColor;
                    this._OSalesHistory.SaleCustomersDataSet = TSaleCustomersDataSet;
                    this._OSalesHistory.SaleCustomersColors = TSaleCustomersColor;
                } else {
                    this.NotifySimple(_Response.Message);
                }
            },
            _Error => {
                this.IsFormProcessing = false;
                this.HandleException(_Error);
            }
        );
    }


    GetCardTypeSalesSummary(UserAccountId, UserAccountTypeId, SubUserAccountId, SubUserAccountTypeId, StartTime, EndTime) {
        this.IsFormProcessing = true;
        this._CardTypeSalesSummary.CardTypeLabels = [];
        this._CardTypeSalesSummary.CardTypeData = [];
        var Data = {
            Task: "getcardtypesalessummary",
            StartTime: StartTime,
            EndTime: EndTime,
            UserAccountId: UserAccountId,
            UserAccountTypeId: UserAccountTypeId,
            SubUserAccountId: SubUserAccountId,
            SubUserAccountTypeId: SubUserAccountTypeId,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this.PostData(this.AppConfig.Network.V2.TUCAnalytics, Data);
        _OResponse.subscribe(
            _Response => {
                this.IsFormProcessing = false;
                if (_Response.Status == this.AppConfig.StatusSuccess) {
                    this._CardTypeSalesSummary = _Response.Result as OCardTypeSalesSummary;
                    this._CardTypeSalesSummary.CardTypeLabels = [];
                    this._CardTypeSalesSummary.CardTypeData = [];
                    if (this._CardTypeSalesSummary != undefined && this._CardTypeSalesSummary.CardTypes != undefined) {
                        this._CardTypeSalesSummary.CardTypes.forEach(element => {
                            this._CardTypeSalesSummary.CardTypeLabels.push(element.CardTypeName);
                            this._CardTypeSalesSummary.CardTypeData.push(element.TotalTransaction);
                            element.FirstTransactionDate = this.GetDateTimeS(element.FirstTransactionDate);
                            element.LastTransactionDateD = this.GetTimeDifferenceS(element.LastTransactionDate, moment());
                            element.LastTransactionDate = this.GetDateTimeS(element.LastTransactionDate);
                        });
                    }

                } else {
                    this.NotifySimple(_Response.Message);
                }
            },
            _Error => {
                this.IsFormProcessing = false;
                this.HandleException(_Error);
            }
        );
    }
    GetAccountOverview(UserAccountKey, SubUserAccountKey, StartTime, EndTime) {
        this.IsFormProcessing = true;
        var Data = {
            Task: "getaccountoverview",
            StartTime: StartTime, // new Date(2017, 0, 1, 0, 0, 0, 0),
            EndTime: EndTime, // moment().add(2, 'days'),
            UserAccountKey: UserAccountKey,
            SubUserAccountKey: SubUserAccountKey
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this.PostData(this.AppConfig.Network.V2.ThankU, Data);
        _OResponse.subscribe(
            _Response => {
                this.IsFormProcessing = false;
                if (_Response.Status == this.AppConfig.StatusSuccess) {
                    this._AccountOverview = _Response.Result as OAccountOverview;
                    if (
                        this._AccountOverview.SettlementCredit != undefined &&
                        this._AccountOverview.SettlementDebit != undefined
                    ) {
                        this._AccountOverview.SettlementPending =
                            this._AccountOverview.SettlementCredit -
                            this._AccountOverview.SettlementDebit;
                    }
                } else {
                    this.NotifySimple(_Response.Message);
                }
            },
            _Error => {
                this.IsFormProcessing = false;
                this.HandleException(_Error);
            }
        );
    }
    GetAccountOverviewNext(
        UserAccountKey,
        SubUserAccountKey,
        StartTime,
        EndTime
    ) {
        this.IsFormProcessing = true;
        var Data = {
            Task: "getaccountoverview",
            StartTime: StartTime, // new Date(2017, 0, 1, 0, 0, 0, 0),
            EndTime: EndTime, // moment().add(2, 'days'),
            UserAccountKey: UserAccountKey,
            SubUserAccountKey: SubUserAccountKey
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this.PostData(this.AppConfig.Network.V2.ThankU, Data);
        _OResponse.subscribe(
            _Response => {
                this.IsFormProcessing = false;
                if (_Response.Status == this.AppConfig.StatusSuccess) {
                    this._AccountOverviewNext = _Response.Result as OAccountOverview;
                } else {
                    this.NotifySimple(_Response.Message);
                }
            },
            _Error => {
                this.IsFormProcessing = false;
                this.HandleException(_Error);
            }
        );
    }
    GetRewardOverview(UserAccountKey, SubUserAccountKey, StartTime, EndTime) {
        this.IsFormProcessing = true;
        var Data = {
            Task: "getrewardoverview",
            StartTime: StartTime, // new Date(2017, 0, 1, 0, 0, 0, 0),
            EndTime: EndTime, // moment().add(2, 'days'),
            UserAccountKey: UserAccountKey,
            SubUserAccountKey: SubUserAccountKey
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this.PostData(this.AppConfig.Network.V2.ThankU, Data);
        _OResponse.subscribe(
            _Response => {
                this.IsFormProcessing = false;
                if (_Response.Status == this.AppConfig.StatusSuccess) {
                    this._RewardOverview = _Response.Result as ORewardOverview;
                } else {
                    this.NotifySimple(_Response.Message);
                }
            },
            _Error => {
                this.IsFormProcessing = false;
                this.HandleException(_Error);
            }
        );
    }
    GetRewardOverviewNext(UserAccountKey, SubUserAccountKey, StartTime, EndTime) {
        this.IsFormProcessing = true;
        var Data = {
            Task: "getrewardoverview",
            StartTime: StartTime, // new Date(2017, 0, 1, 0, 0, 0, 0),
            EndTime: EndTime, // moment().add(2, 'days'),
            UserAccountKey: UserAccountKey,
            SubUserAccountKey: SubUserAccountKey
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this.PostData(this.AppConfig.Network.V2.ThankU, Data);
        _OResponse.subscribe(
            _Response => {
                this.IsFormProcessing = false;
                if (_Response.Status == this.AppConfig.StatusSuccess) {
                    this._RewardOverviewNext = _Response.Result as ORewardOverview;
                } else {
                    this.NotifySimple(_Response.Message);
                }
            },
            _Error => {
                this.IsFormProcessing = false;
                this.HandleException(_Error);
            }
        );
    }
    GetRewardTypeOverview(UserAccountKey, SubUserAccountKey, StartTime, EndTime) {
        this.IsFormProcessing = true;
        var Data = {
            Task: "getrewardtypeoverview",
            StartTime: StartTime, // new Date(2017, 0, 1, 0, 0, 0, 0),
            EndTime: EndTime, // moment().add(2, 'days'),
            UserAccountKey: UserAccountKey,
            SubUserAccountKey: SubUserAccountKey
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this.PostData(this.AppConfig.Network.V2.ThankU, Data);
        _OResponse.subscribe(
            _Response => {
                this.IsFormProcessing = false;
                if (_Response.Status == this.AppConfig.StatusSuccess) {
                    this._RewardTypeOverview = _Response.Result as ORewardTypeOverview;
                    this._RewardTypeOverview.RewardTypeLabels = [];
                    this._RewardTypeOverview.RewardTypeData = [];
                    this._RewardTypeOverview.RewardTypeLabels.push("Cash");
                    this._RewardTypeOverview.RewardTypeLabels.push("Card");
                    this._RewardTypeOverview.RewardTypeData.push(
                        this._RewardTypeOverview.CashRewardTransactions
                    );
                    this._RewardTypeOverview.RewardTypeData.push(
                        this._RewardTypeOverview.CardRewardTransactions
                    );

                    this._RewardTypeOverview.AppUsersCardTypeLabel = [];
                    this._RewardTypeOverview.AppUsersCardTypeDataUsers = [];
                    this._RewardTypeOverview.AppUsersCardTypeDataTransactions = [];
                    this._RewardTypeOverview.AppUsersCardTypeDataPurchase = [];
                    this._RewardTypeOverview.AppUsersCardType.forEach(element => {
                        this._RewardTypeOverview.AppUsersCardTypeLabel.push(element.Name);
                        this._RewardTypeOverview.AppUsersCardTypeDataUsers.push(
                            element.Users
                        );
                        this._RewardTypeOverview.AppUsersCardTypeDataTransactions.push(
                            element.Transactions
                        );
                        this._RewardTypeOverview.AppUsersCardTypeDataPurchase.push(
                            Math.round(element.Purchase)
                        );
                        element.Purchase = Math.round(element.Purchase);
                    });

                    this._RewardTypeOverview.AppUsersBankLabel = [];
                    this._RewardTypeOverview.AppUsersBankDataUsers = [];
                    this._RewardTypeOverview.AppUsersBankDataTransactions = [];
                    this._RewardTypeOverview.AppUsersBankDataPurchase = [];
                    if (this._RewardTypeOverview.AppUsersBank != undefined) {
                        this._RewardTypeOverview.AppUsersBank.forEach(element => {
                            element.Purchase = Math.round(element.Purchase);
                            if (element.Users > 0) {
                                this._RewardTypeOverview.AppUsersBankLabel.push(element.Name);
                                this._RewardTypeOverview.AppUsersBankDataUsers.push(
                                    element.Users
                                );
                                this._RewardTypeOverview.AppUsersBankDataTransactions.push(
                                    element.Transactions
                                );
                                this._RewardTypeOverview.AppUsersBankDataPurchase.push(
                                    Math.round(element.Purchase)
                                );
                            }
                        });
                    }

                } else {
                    this.NotifySimple(_Response.Message);
                }
            },
            _Error => {
                this.IsFormProcessing = false;
                this.HandleException(_Error);
            }
        );
    }
    GetRewardTypeOverviewNext(
        UserAccountKey,
        SubUserAccountKey,
        StartTime,
        EndTime
    ) {
        this.IsFormProcessing = true;
        var Data = {
            Task: "getrewardtypeoverview",
            StartTime: StartTime, // new Date(2017, 0, 1, 0, 0, 0, 0),
            EndTime: EndTime, // moment().add(2, 'days'),
            UserAccountKey: UserAccountKey,
            SubUserAccountKey: SubUserAccountKey
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this.PostData(this.AppConfig.Network.V2.ThankU, Data);
        _OResponse.subscribe(
            _Response => {
                this.IsFormProcessing = false;
                if (_Response.Status == this.AppConfig.StatusSuccess) {
                    this._RewardTypeOverviewNext = _Response.Result as ORewardTypeOverview;
                    this._RewardTypeOverviewNext.RewardTypeLabels = [];
                    this._RewardTypeOverviewNext.RewardTypeData = [];
                    this._RewardTypeOverviewNext.RewardTypeLabels.push("Cash");
                    this._RewardTypeOverviewNext.RewardTypeLabels.push("Card");
                    this._RewardTypeOverviewNext.RewardTypeData.push(
                        this._RewardTypeOverviewNext.CashRewardTransactions
                    );
                    this._RewardTypeOverviewNext.RewardTypeData.push(
                        this._RewardTypeOverviewNext.CardRewardTransactions
                    );

                    this._RewardTypeOverviewNext.AppUsersCardTypeLabel = [];
                    this._RewardTypeOverviewNext.AppUsersCardTypeDataUsers = [];
                    this._RewardTypeOverviewNext.AppUsersCardTypeDataTransactions = [];
                    this._RewardTypeOverviewNext.AppUsersCardTypeDataPurchase = [];
                    this._RewardTypeOverviewNext.AppUsersCardType.forEach(element => {
                        this._RewardTypeOverviewNext.AppUsersCardTypeLabel.push(
                            element.Name
                        );
                        this._RewardTypeOverviewNext.AppUsersCardTypeDataUsers.push(
                            element.Users
                        );
                        this._RewardTypeOverviewNext.AppUsersCardTypeDataTransactions.push(
                            element.Transactions
                        );
                        this._RewardTypeOverviewNext.AppUsersCardTypeDataPurchase.push(
                            Math.round(element.Purchase)
                        );
                        element.Purchase = Math.round(element.Purchase);
                    });

                    this._RewardTypeOverviewNext.AppUsersBankLabel = [];
                    this._RewardTypeOverviewNext.AppUsersBankDataUsers = [];
                    this._RewardTypeOverviewNext.AppUsersBankDataTransactions = [];
                    this._RewardTypeOverviewNext.AppUsersBankDataPurchase = [];
                    this._RewardTypeOverviewNext.AppUsersBank.forEach(element => {
                        element.Purchase = Math.round(element.Purchase);
                        if (element.Users > 0) {
                            this._RewardTypeOverviewNext.AppUsersBankLabel.push(element.Name);
                            this._RewardTypeOverviewNext.AppUsersBankDataUsers.push(
                                element.Users
                            );
                            this._RewardTypeOverviewNext.AppUsersBankDataTransactions.push(
                                element.Transactions
                            );
                            this._RewardTypeOverviewNext.AppUsersBankDataPurchase.push(
                                Math.round(element.Purchase)
                            );
                        }
                    });
                } else {
                    this.NotifySimple(_Response.Message);
                }
            },
            _Error => {
                this.IsFormProcessing = false;
                this.HandleException(_Error);
            }
        );
    }
    GetRedeemOverview(UserAccountKey, SubUserAccountKey, StartTime, EndTime) {
        this.IsFormProcessing = true;
        var Data = {
            Task: "getredeemoverview",
            StartTime: StartTime, // new Date(2017, 0, 1, 0, 0, 0, 0),
            EndTime: EndTime, // moment().add(2, 'days'),
            UserAccountKey: UserAccountKey,
            SubUserAccountKey: SubUserAccountKey
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this.PostData(this.AppConfig.Network.V2.ThankU, Data);
        _OResponse.subscribe(
            _Response => {
                this.IsFormProcessing = false;
                if (_Response.Status == this.AppConfig.StatusSuccess) {
                    this._RedeemOverview = _Response.Result as ORedeemOverview;
                } else {
                    this.NotifySimple(_Response.Message);
                }
            },
            _Error => {
                this.IsFormProcessing = false;
                this.HandleException(_Error);
            }
        );
    }
    GetRedeemOverviewNext(UserAccountKey, SubUserAccountKey, StartTime, EndTime) {
        this.IsFormProcessing = true;
        var Data = {
            Task: "getredeemoverview",
            StartTime: StartTime, // new Date(2017, 0, 1, 0, 0, 0, 0),
            EndTime: EndTime, // moment().add(2, 'days'),
            UserAccountKey: UserAccountKey,
            SubUserAccountKey: SubUserAccountKey
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this.PostData(this.AppConfig.Network.V2.ThankU, Data);
        _OResponse.subscribe(
            _Response => {
                this.IsFormProcessing = false;
                if (_Response.Status == this.AppConfig.StatusSuccess) {
                    this._RedeemOverviewNext = _Response.Result as ORedeemOverview;
                } else {
                    this.NotifySimple(_Response.Message);
                }
            },
            _Error => {
                this.IsFormProcessing = false;
                this.HandleException(_Error);
            }
        );
    }
    GetAppUsersOverview(UserAccountKey, SubUserAccountKey, StartTime, EndTime) {
        this.IsFormProcessing = true;
        var Data = {
            Task: "getappusersoverview",
            StartTime: StartTime, // new Date(2017, 0, 1, 0, 0, 0, 0),
            EndTime: EndTime, // moment().add(2, 'days'),
            UserAccountKey: UserAccountKey,
            ParentKey: SubUserAccountKey
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this.PostData(this.AppConfig.Network.V2.ThankU, Data);
        _OResponse.subscribe(
            _Response => {
                this.IsFormProcessing = false;
                if (_Response.Status == this.AppConfig.StatusSuccess) {
                    this._AppUsersOverview = _Response.Result as OAppUsersOverview;
                    this._AppUsersOverview.AppUsersLabelsByGender = [];
                    this._AppUsersOverview.AppUsersDataByGender = [];
                    this._AppUsersOverview.AppUsersLabelsByAgeGroup = [];
                    this._AppUsersOverview.AppUsersVisitByAgeGroup = [];
                    this._AppUsersOverview.AppUsersCountByAgeGroup = [];
                    this._AppUsersOverview.AppUsersPurchaseByAgeGroup = [];

                    this._AppUsersOverview.AppUsersLabelsByGender.push("Male");
                    this._AppUsersOverview.AppUsersLabelsByGender.push("Female");

                    this._AppUsersOverview.AppUsersDataByGender.push(
                        this._AppUsersOverview.AppUsersMale
                    );
                    this._AppUsersOverview.AppUsersDataByGender.push(
                        this._AppUsersOverview.AppUsersFemale
                    );
                    this._AppUsersOverview.AppUsersByAgeGroup.forEach(element => {
                        if (element.Name != "Unknown") {
                            this._AppUsersOverview.AppUsersLabelsByAgeGroup.push(
                                element.Name
                            );
                            this._AppUsersOverview.AppUsersVisitByAgeGroup.push(
                                element.Visits
                            );
                            this._AppUsersOverview.AppUsersCountByAgeGroup.push(
                                element.Users
                            );
                            this._AppUsersOverview.AppUsersPurchaseByAgeGroup.push(
                                Math.round(element.Purchase)
                            );
                        }
                    });
                } else {
                    this.NotifySimple(_Response.Message);
                }
            },
            _Error => {
                this.IsFormProcessing = false;
                this.HandleException(_Error);
            }
        );
    }
    GetAppUsersOverviewNext(
        UserAccountKey,
        SubUserAccountKey,
        StartTime,
        EndTime
    ) {
        this.IsFormProcessing = true;
        var Data = {
            Task: "getappusersoverview",
            StartTime: StartTime, // new Date(2017, 0, 1, 0, 0, 0, 0),
            EndTime: EndTime, // moment().add(2, 'days'),
            UserAccountKey: UserAccountKey,
            ParentKey: SubUserAccountKey
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this.PostData(this.AppConfig.Network.V2.ThankU, Data);
        _OResponse.subscribe(
            _Response => {
                this.IsFormProcessing = false;
                if (_Response.Status == this.AppConfig.StatusSuccess) {
                    this._AppUsersOverviewNext = _Response.Result as OAppUsersOverview;
                    this._AppUsersOverviewNext.AppUsersLabelsByGender = [];
                    this._AppUsersOverviewNext.AppUsersDataByGender = [];
                    this._AppUsersOverviewNext.AppUsersLabelsByAgeGroup = [];
                    this._AppUsersOverviewNext.AppUsersVisitByAgeGroup = [];
                    this._AppUsersOverviewNext.AppUsersCountByAgeGroup = [];
                    this._AppUsersOverviewNext.AppUsersPurchaseByAgeGroup = [];

                    this._AppUsersOverviewNext.AppUsersLabelsByGender.push("Male");
                    this._AppUsersOverviewNext.AppUsersLabelsByGender.push("Female");

                    this._AppUsersOverviewNext.AppUsersDataByGender.push(
                        this._AppUsersOverviewNext.AppUsersMale
                    );
                    this._AppUsersOverviewNext.AppUsersDataByGender.push(
                        this._AppUsersOverviewNext.AppUsersFemale
                    );
                    this._AppUsersOverviewNext.AppUsersByAgeGroup.forEach(element => {
                        if (element.Name != "Unknown") {
                            this._AppUsersOverviewNext.AppUsersLabelsByAgeGroup.push(
                                element.Name
                            );
                            this._AppUsersOverviewNext.AppUsersVisitByAgeGroup.push(
                                element.Visits
                            );
                            this._AppUsersOverviewNext.AppUsersCountByAgeGroup.push(
                                element.Users
                            );
                            this._AppUsersOverviewNext.AppUsersPurchaseByAgeGroup.push(
                                Math.round(element.Purchase)
                            );
                        }
                    });
                } else {
                    this.NotifySimple(_Response.Message);
                }
            },
            _Error => {
                this.IsFormProcessing = false;
                this.HandleException(_Error);
            }
        );
    }

    GetStoreVisits(StartTime, EndTime) {
        var Data = {
            Task: "getaccountsaleshistoryhourly",
            ReferenceId: this.AccountOwner.AccountId,
            SearchCondition: this.GetDateCondition('', 'Date', StartTime, EndTime)
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this.PostData(this.AppConfig.Network.V2.TUCAnalytics, Data);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this.AppConfig.StatusSuccess) {
                    var ChartResponse = _Response.Result.Data;
                    this._UserOverviewPlot.HourlyVisitLabel = [];
                    this._UserOverviewPlot.HourlyVisitData = [];
                    this._UserOverviewPlot.HourlySalesData = [];
                    var TPlotDataSet = [];
                    var TSalesPlotDataSet = [];
                    var DataSetItemVisit = {
                        label: 'Visits',
                        data: [],
                        lineTension: 0.0,
                        fill: false,
                    };
                    var DataSetItemSale = {
                        label: 'Sale',
                        data: []
                    };
                    for (let index = 0; index < 24; index++) {
                        var dd = " am";
                        var h = index;
                        if (h >= 12) {
                            h = index - 12;
                            dd = " pm";
                        }
                        if (h == 0) {
                            h = 12;
                        }
                        var Hour = h + dd;
                        this._UserOverviewPlot.HourlyVisitLabel.push(Hour);
                        var RData = ChartResponse.find(x => x.Hour == index);
                        if (RData != undefined && RData != null) {
                            DataSetItemVisit.data.push(RData.SuccessfulTransaction);
                            DataSetItemSale.data.push(RData.SuccessfulTransactionInvoiceAmount);
                        }
                        else {
                            DataSetItemVisit.data.push(0);
                            DataSetItemSale.data.push(0);
                        }
                    }
                    TPlotDataSet.push(DataSetItemVisit);
                    TSalesPlotDataSet.push(DataSetItemSale);
                    this._UserOverviewPlot.HourlySalesData = TSalesPlotDataSet;
                    this._UserOverviewPlot.HourlyVisitData = TPlotDataSet;
                } else {
                    this.NotifySimple(_Response.Message);
                }
            },
            _Error => {
                this.HandleException(_Error);
            }
        );
    }
    GetVisitHistory(UserAccountKey, SubUserAccountKey, StartTime, EndTime) {
        this.IsFormProcessing = true;
        var Data = {
            Task: "getvisithistory",
            StartTime: StartTime, // new Date(2017, 0, 1, 0, 0, 0, 0),
            EndTime: EndTime, // moment().add(2, 'days'),
            UserAccountKey: UserAccountKey,
            SubUserAccountKey: SubUserAccountKey
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this.PostData(this.AppConfig.Network.V2.ThankU, Data);
        _OResponse.subscribe(
            _Response => {
                this.IsFormProcessing = false;
                if (_Response.Status == this.AppConfig.StatusSuccess) {
                    var ChartResponse = _Response.Result;
                    this._UserOverviewPlot.HourlyVisitLabel = [];
                    this._UserOverviewPlot.HourlyVisitData = [];
                    ChartResponse.DateRange.forEach(RangeItem => {
                        this._UserOverviewPlot.HourlyVisitLabel.push(
                            this.GetTimeS(RangeItem.StartTime)
                        );
                    });
                    var TPlotDataSet = [];
                    if (ChartResponse.DateRange.length > 0) {
                        var RangeItem = ChartResponse.DateRange[0].Data;
                        RangeItem.forEach(element => {
                            var DataSetItem = {
                                label: element.Name,
                                data: []
                            };
                            TPlotDataSet.push(DataSetItem);
                        });
                        ChartResponse.DateRange.forEach(RangeItem => {
                            var Data = RangeItem.Data;
                            Data.forEach(element => {
                                TPlotDataSet[0].data.push(parseInt(element.Value));
                                var DataItem = TPlotDataSet.find(x => x.label == element.Name);
                                if (DataItem != undefined) {
                                    DataItem.data.push(parseInt(element.Value));
                                }
                            });
                        });
                    }
                    this._UserOverviewPlot.HourlyVisitData = TPlotDataSet;
                } else {
                    this.NotifySimple(_Response.Message);
                }
            },
            _Error => {
                this.IsFormProcessing = false;
                this.HandleException(_Error);
            }
        );
    }
    GetVisitHistoryNext(UserAccountKey, SubUserAccountKey, StartTime, EndTime) {
        this.IsFormProcessing = true;
        var Data = {
            Task: "getvisithistory",
            StartTime: StartTime, // new Date(2017, 0, 1, 0, 0, 0, 0),
            EndTime: EndTime, // moment().add(2, 'days'),
            UserAccountKey: UserAccountKey,
            SubUserAccountKey: SubUserAccountKey
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this.PostData(this.AppConfig.Network.V2.ThankU, Data);
        _OResponse.subscribe(
            _Response => {
                this.IsFormProcessing = false;
                if (_Response.Status == this.AppConfig.StatusSuccess) {
                    var ChartResponse = _Response.Result;
                    this._UserOverviewPlot.HourlyVisitNextLabel = [];
                    this._UserOverviewPlot.HourlyVisitNextData = [];
                    ChartResponse.DateRange.forEach(RangeItem => {
                        this._UserOverviewPlot.HourlyVisitNextLabel.push(
                            this.GetTimeS(RangeItem.StartTime)
                        );
                    });
                    var TPlotDataSet = [];
                    if (ChartResponse.DateRange.length > 0) {
                        var RangeItem = ChartResponse.DateRange[0].Data;
                        RangeItem.forEach(element => {
                            var DataSetItem = {
                                label: element.Name,
                                data: []
                            };
                            TPlotDataSet.push(DataSetItem);
                        });
                        ChartResponse.DateRange.forEach(RangeItem => {
                            var Data = RangeItem.Data;
                            Data.forEach(element => {
                                TPlotDataSet[0].data.push(parseInt(element.Value));
                                var DataItem = TPlotDataSet.find(x => x.label == element.Name);
                                if (DataItem != undefined) {
                                    DataItem.data.push(parseInt(element.Value));
                                }
                            });
                        });
                    }
                    this._UserOverviewPlot.HourlyVisitNextData = TPlotDataSet;
                } else {
                    this.NotifySimple(_Response.Message);
                }
            },
            _Error => {
                this.IsFormProcessing = false;
                this.HandleException(_Error);
            }
        );
    }

}

interface ONetworkResponse {
    fx: string;
    vx: string;
    zx: string;
}


[
    {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#e9e9e9"
            },
            {
                "lightness": 17
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#f5f5f5"
            },
            {
                "lightness": 20
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "lightness": 17
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "lightness": 29
            },
            {
                "weight": 0.2
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "lightness": 18
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "lightness": 16
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#f5f5f5"
            },
            {
                "lightness": 21
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#dedede"
            },
            {
                "lightness": 21
            }
        ]
    },
    {
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#ffffff"
            },
            {
                "lightness": 16
            }
        ]
    },
    {
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "saturation": 36
            },
            {
                "color": "#333333"
            },
            {
                "lightness": 40
            }
        ]
    },
    {
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#f2f2f2"
            },
            {
                "lightness": 19
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#fefefe"
            },
            {
                "lightness": 20
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#fefefe"
            },
            {
                "lightness": 17
            },
            {
                "weight": 1.2
            }
        ]
    }
]



