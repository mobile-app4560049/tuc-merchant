import { DatePipe } from '@angular/common';
import { HttpClient, HttpErrorResponse, HttpHeaders } from "@angular/common/http";
import { Injectable } from '@angular/core';
import { AlertController, LoadingController, ModalController, NavController, Platform } from '@ionic/angular';
import { Observable, throwError } from "rxjs";
import { catchError, map } from "rxjs/operators";
import { OResponse, OAccount, OAccountInfo } from './object.service';
import { Geolocation } from "@ionic-native/geolocation/ngx";
import { HelperService } from '../service/helper.service';
@Injectable()
export class DataService {
    public AppData = {
        Stations: [],
        Products: [],
    }
    constructor(
        private _ModalController: ModalController,
        private _Platform: Platform,
        private _Http: HttpClient,
        private _NavController: NavController,
        private _Geolocation: Geolocation,
        private _DatePipe: DatePipe,
        private _LoadingController: LoadingController,
        private _AlertController: AlertController,
        private _HelperService: HelperService,
    ) {
    }

    private HandelError(_Error) {
        this._HelperService.HideSpinner();
        if (_Error.status == 0) {
            this._HelperService.Notify('Operation failed', "Please check your internet connection");
        }
        else if (_Error.status == 401) {
            var EMessage = JSON.parse(_Error._body).error;
            this._HelperService.Notify('Operation failed', EMessage + ' Unable to start verification. Please contact support')

        }
        else {
            this._HelperService.Notify('Operation failed', ' Unable to start verification. Please contact support')
        }
    }
    GetProducts() {
        var _RequestData = {
            Task: "getproducts",
            Offset: 0,
            Limit: 100,
        };
        try {
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V1.EnyoAppConnect, _RequestData);
            _OResponse.subscribe(
                (_Response) => {
                    this._HelperService.HideSpinner();
                    if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.Products, _Response.Result.Data);
                        this.AppData.Products = _Response.Result.Data;
                    } else {
                        this._HelperService.Notify('Operation failed', "Unable to connect server " + _Response.ResponseCode);
                    }
                },
                (_Error) => {
                    this.HandelError(_Error);
                }
            );
        } catch (_Error) {
            this.HandelError(_Error);
        }
    }
    GetStations() {
        var _RequestData = {
            Task: "getstations",
            Offset: 0,
            Limit: 100,
        };
        try {
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V1.EnyoAppConnect, _RequestData);
            _OResponse.subscribe(
                (_Response) => {
                    this._HelperService.HideSpinner();
                    if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.Stations, _Response.Result.Data);
                        this.AppData.Stations = _Response.Result.Data;
                    } else {
                        this._HelperService.Notify('Operation failed', "Unable to connect server " + _Response.ResponseCode);
                    }
                },
                (_Error) => {
                    this.HandelError(_Error);
                }
            );
        } catch (_Error) {
            this.HandelError(_Error);
        }
    }
}