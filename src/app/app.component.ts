import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Platform, MenuController, AlertController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { HelperService } from './service/helper.service';
import { Sim } from '@ionic-native/sim/ngx';
import { Device } from "@ionic-native/device/ngx";
import { FCM } from '@ionic-native/fcm/ngx';
import { ODeviceInformation, OResponse } from './service/object.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  private _DeviceInformation: ODeviceInformation = {
    MobileNumber: null,
    SerialNumber: null,
    OsName: "website",
    OsVersion: "1",
    Brand: null,
    Model: null,
    Width: null,
    Height: null,
    CarrierName: null,
    CountryCode: null,
    Mcc: null,
    Mnc: null,
    Latitude: 0,
    Longitude: 0,
    NotificationUrl: null
  };
  constructor(
    public _Firebase: FCM,
    public _AlertController: AlertController,
    public _HelperService: HelperService,
    private _Platform: Platform,
    private splashScreen: SplashScreen,
    private _StatusBar: StatusBar,
    private _Device: Device,
    private _Sim: Sim,
    private _MenuController: MenuController,
  ) {

    this.initializeApp();

  }
  UpdateToken(Token) {
    this._HelperService.SaveStorageValue('dnot', Token);
    if (this._HelperService.AccountInfo.UserAccount.AccountId != undefined && this._HelperService.AccountInfo.UserAccount.AccountId != null && this._HelperService.AccountInfo.UserAccount.AccountId != 0) {
      var DeviceInfo =
      {
        Task: "updatedevicenotificationurl",
        UserAccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
        UserAccountId: this._HelperService.AccountInfo.UserAccount.AccountId,
        NotificationUrl: Token,
      };
      let _OResponse: Observable<OResponse>;
      _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.System, DeviceInfo);
      _OResponse.subscribe(
        _Response => {
          this._HelperService.HideSpinner();
          if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
            this._HelperService.HideSpinner();
          }
          else {
            this._HelperService.Notify(_Response.Status, _Response.Message);
          }
        },
        _Error => {
          this._HelperService.HandleException(_Error);
        });
    }
  }

  NavigatePush(data) {
    if (data != undefined && data != null) {
      if (data.Task != undefined && data.Task != null) {
        var Task = data.Task;
        if (Task == "orders") {
          this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.OrdersHistory);
        }
        if (Task == "order") {
          if (data.ReferenceId != undefined && data.ReferenceKey != null) {
            var _OrderDetails =
            {
              OrderId: data.ReferenceId,
              OrderKey: data.ReferenceKey
            };
            this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveOrder, _OrderDetails);
            this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.orderdetails);
          }
        }



      }
    }
  }
  initializeApp() {



    this._Platform.ready().then(() => {
      var DeviceI = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
      if (DeviceI != null) {
        this._DeviceInformation = DeviceI;
      }

      this._Firebase.hasPermission().then(hasPermission => {
        if (hasPermission) {
        }
        else {
          this._Firebase.requestPushPermissionIOS()
            .then((success) => {
              if (success) {
                this._Firebase.subscribeToTopic(this._HelperService.AppConfig.PushTopics.tucmerchant);
                this._Firebase.getToken().then(token => {
                  this._DeviceInformation.NotificationUrl = token;
                  this.UpdateToken(token);
                });
              }
            });
        }
      });
      this._Firebase.subscribeToTopic(this._HelperService.AppConfig.PushTopics.tucmerchant);
      this._Firebase.getToken().then(token => {
        this._DeviceInformation.NotificationUrl = token;
        this.UpdateToken(token);
      });
      this._Firebase.onTokenRefresh().subscribe(token => {
        this._DeviceInformation.NotificationUrl = token;
        this.UpdateToken(token);
      });

      this._Firebase.onNotification().subscribe(data => {
        if (data.wasTapped) {
          // background
          this.NavigatePush(data);
        } else {
          // foreground
          if (data != undefined && data != null) {
            if (data.ForceNavigation != undefined && data.ForceNavigation != null) {
              if (data.ForceNavigation) {
                this._HelperService.NotifyToast(data.Message);
                this.NavigatePush(data);
              }
              else {
                this._HelperService.NotifyToast(data.Message);
              }
            }
          }
        };
      });


      this.splashScreen.hide();
      this._StatusBar.styleBlackTranslucent();
      this._StatusBar.backgroundColorByHexString('#303F9F');
      if (this._Device.uuid != undefined && this._Device.uuid != null) {
        this._DeviceInformation.SerialNumber = this._Device.uuid;
      }
      if (this._DeviceInformation.SerialNumber == undefined || this._DeviceInformation.SerialNumber == null || this._DeviceInformation.SerialNumber == '') {
        this._DeviceInformation.SerialNumber = 'cu_' + this._HelperService.GenerateGuid();
      }
      this._DeviceInformation.OsName = this._Device.platform;
      this._DeviceInformation.OsVersion = this._Device.version;
      this._DeviceInformation.Brand = this._Device.manufacturer;
      this._DeviceInformation.Model = this._Device.model;
      this._DeviceInformation.Width = window.screen.width;
      this._DeviceInformation.Height = window.screen.height;
      if (this._Platform.is("android")) {
        this._DeviceInformation.OsName = "android";
      } else if (this._Platform.is("ios")) {
        this._DeviceInformation.OsName = "ios";
      } else if (this._Platform.is("ipad")) {
        this._DeviceInformation.OsName = "ios";
      } else if (this._Platform.is("iphone")) {
        this._DeviceInformation.OsName = "ios";
      } else {
        this._DeviceInformation.OsName = "android";
      }
      this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.Device, this._DeviceInformation);
      this.LoadSimCardInformation();
      try {
        var UserAccount = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Account);
        if (UserAccount != null) {
          var TokenItem = this._HelperService.GetStorageValue('dnot');
          if (TokenItem != undefined && TokenItem != null) {
            this.UpdateToken(TokenItem);
          }
          this._Firebase.subscribeToTopic(this._HelperService.AppConfig.PushTopics.tucmerchant + this._HelperService.AccountOwner.AccountId).then(x => {
          }).catch(x => {
          });

          this._HelperService.RefreshLocation();
          var AccType = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Account);
          if (AccType.UserAccount.AccountTypeCode == 'merchantcashier') {
            this._MenuController.enable(false);
            this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.CashierAccess.cashierdashboard);
          }
          else {
            this._MenuController.enable(true);
            this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Dashboard);
          }
        } else {
          this._HelperService.SaveStorage(
            this._HelperService.AppConfig.StorageHelper.Device, this._DeviceInformation);
          this._HelperService.RefreshLocation();
          this._MenuController.enable(false);
          var VerificationCheck = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Verification);
          if (VerificationCheck != null) {
            if (VerificationCheck.Stage == 2) {
              this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Auth.VerifyPin);
            }
            else if (VerificationCheck.Stage == 3) {
              this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Auth.AuthProfile);
            }
            else if (VerificationCheck.Stage == 4) {
              var AccType = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Account);
              if (AccType.UserAccount.AccountTypeCode == 'merchantcashier') {
                this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.CashierAccess.cashierdashboard);
              }
              else {
                this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Dashboard);
              }
            }
            else {
              this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Auth.Login);
            }
          }
          else {
            this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Auth.Login);
          }
        }
      } catch (error) {
        this._HelperService.Notify("Error occured", "Please close app and open again. " + JSON.stringify(error));
      }
    });
  }

  LoadSimCardInformation() {
    var DeviceStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
    if (DeviceStorage != null) {
      if (DeviceStorage.OsName == "android") {
        this._Sim.hasReadPermission().then(
          info => {
            this._Sim.getSimInfo().then(
              SimData => {
                this.SetSimCardData(SimData);
              },
              err => {
              }
            );
          },
          err => {
            this._Sim.requestReadPermission().then(
              () => {
                this._Sim.getSimInfo().then(
                  SimData => {
                    this.SetSimCardData(SimData);
                  },
                  err => {
                  }
                );
              },
              () => {
              }
            );
          }
        );
      } else {
        this._Sim.getSimInfo().then(
          SimData => {
            this.SetSimCardData(SimData);
          },
          err => {
          }
        );
      }
    }
  }
  SetSimCardData(SimData) {
    var DeviceStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
    if (DeviceStorage != null) {
      DeviceStorage.CarrierName = SimData.carrierName;
      DeviceStorage.CountryCode = SimData.countryCode;
      DeviceStorage.Mcc = SimData.mcc;
      DeviceStorage.Mnc = SimData.mnc;
      this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.Device, DeviceStorage);
      this._HelperService.RefreshLocation();
    }
  }

  NavDashboard() {
    this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Dashboard);
  }
  NavOrders() {
    this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.OrdeManager.orders);
  }
  NavStores() {
    this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Stores);
  }
  NavTerminals() {
    this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Terminals);
  }
  NavCashiers() {
    this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Cashiers);
  }
  NavSaleHistory() {
    this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.SalesHistory);
  }
  NavWallet() {
    this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Wallet);
  }
  NavBankAccount() {
    this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.BankManager.list);
  }
  NavCashouts() {
    this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Cashout.list);
  }
  // NavOrders() {
  //   this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.OrdersHistory);
  // }
  // NavTransactions() {
  //   this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.TransactionsHistory);
  // }
  NavProfile() {
    this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Profile);
  }
  NavAbout() {
    this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.About);
  }
  NavRewards() {
    this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Reward.RewardHistory);
  }
  NavPendingRewards() {
    this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.pendingrewardhistory);
  }
  NavDealRedeem() {
    this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Deals.redeemhistory);
  }
  NacTucPay() {
    this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.TucPay.TucPay);
  }
  // NavServiceOrders() {
  //   this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.servicehistory);
  // }

  async Logout() {
    const alert = await this._AlertController.create({
      header: 'Do you want to logout ?',
      message: 'Click on confirm to logout.',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
          }
        },
        {
          text: 'Confirm',
          cssClass: 'en-text-primary',
          handler: () => {
            this._HelperService.DeleteStorage(this._HelperService.AppConfig.StorageHelper.Account);
            this._HelperService.DeleteStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference);
            this._HelperService.DeleteStorage(this._HelperService.AppConfig.StorageHelper.Verification);
            this._HelperService.DeleteStorage(this._HelperService.AppConfig.StorageHelper.Stations);
            this._HelperService.DeleteStorage(this._HelperService.AppConfig.StorageHelper.Products);
            this._HelperService.DeleteStorage(this._HelperService.AppConfig.StorageHelper.PaymentsHistory);
            this._HelperService.DeleteStorage(this._HelperService.AppConfig.StorageHelper.OrdersHistory);
            this._HelperService.DeleteStorage(this._HelperService.AppConfig.StorageHelper.ActiveOrder);
            this._HelperService.DeleteStorage(this._HelperService.AppConfig.StorageHelper.SelectedProduct);
            this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Auth.Login);
          }
        }
      ]
    });
    return await alert.present();
  }
}
