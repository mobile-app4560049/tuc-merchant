import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { HttpClientModule } from '@angular/common/http';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { DatePipe } from '@angular/common';
import { Sim } from '@ionic-native/sim/ngx';
import { Device } from "@ionic-native/device/ngx";

import { PlaceOrderModal } from './access/modals/placeorder/placeorder.modal.component';
import { DataService } from './service/data.service';

import { HelperService } from './service/helper.service';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator/ngx';
import { ChartsModule } from 'ng2-charts';

import { NgCalendarModule } from 'ionic2-calendar';
import { AddCashierModal } from './access/modals/addcashier/addcashier.modal.component';
import { FormsModule } from '@angular/forms';
import { TransactionDetailsModal } from './access/modals/transactiondetails/transactiondetails.modal.component';
import { FCM } from '@ionic-native/fcm/ngx';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { QRCodeModule } from 'angularx-qrcode';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner/ngx';
import { ScanMyCodeModal } from './access/modals/scanmycode/scanmycode.modal.component';
import { CashOutDetailsModal } from './access/v3/cashout/modalcashoutdetails/modalcashoutdetails.component';
import { AddCashoutModal } from './access/v3/cashout/modaladdcashout/modaladdcashout.component';
import { AddBankModal } from './access/v3/bankmanager/modaladdbank/modaladdbank.component';
import { BankDetailsModal } from './access/v3/bankmanager/modalbankdetails/modalbankdetails.component';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { AgmCoreModule } from '@agm/core';
import { AddCustomerModal } from './access/modals/addcustomer/addcustomer.modal.component';
import { ModalRewardReceipt } from './access/modals/rewardreceipt/rewardreceipt.modal.component';
import { DateSelectorModal } from './access/modals/dateselector/dateselector.modal.component';
import { Daterangepicker } from 'ng2-daterangepicker';

@NgModule({
  declarations: [AppComponent,
    PlaceOrderModal, AddCashierModal, TransactionDetailsModal, ScanMyCodeModal, AddCustomerModal, DateSelectorModal,
    CashOutDetailsModal, ModalRewardReceipt,
    AddBankModal,
    BankDetailsModal,
    AddCashierModal, AddCashoutModal
  ],
  entryComponents: [PlaceOrderModal, AddCashierModal, ModalRewardReceipt, TransactionDetailsModal, ScanMyCodeModal, AddCustomerModal, DateSelectorModal,
    BankDetailsModal,
    AddBankModal,
    CashOutDetailsModal,
    AddCashoutModal],
  imports: [HttpClientModule, BrowserModule, FormsModule, NgCalendarModule, ChartsModule, IonicModule.forRoot(), AppRoutingModule, QRCodeModule,
    GooglePlaceModule,
    Daterangepicker,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyARhkleqWEzCqbtlwaTwvcRdYe5X5rwQbY'
    })
  ],
  providers: [
    FormsModule,
    Device,
    Sim,
    StatusBar,
    SplashScreen,
    DataService,
    HelperService,
    Geolocation,
    DatePipe,
    LaunchNavigator,
    ChartsModule,
    FCM,
    CallNumber,
    QRScanner,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
