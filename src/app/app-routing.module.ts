import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  { path: 'login', loadChildren: () => import('./auth/login/login.module').then(m => m.LoginPageModule) },
  { path: 'dashboard', loadChildren: () => import('./access/dashboard/dashboard.module').then(m => m.DashboardPageModule) },
  { path: 'about', loadChildren: () => import('./access/about/about.module').then(m => m.AboutPageModule) },
  { path: 'profile', loadChildren: () => import('./access/profile/profile.module').then(m => m.AccessProfilePageModule) },
  { path: 'stores', loadChildren: () => import('./access/stores/stores.module').then(m => m.StoresPageModule) },
  { path: 'store', loadChildren: () => import('./access/stores/store/store.module').then(m => m.StorePageModule) },
  { path: 'terminals', loadChildren: () => import('./access/terminals/terminals.module').then(m => m.TerminalsPageModule) },
  { path: 'cashiers', loadChildren: () => import('./access/cashiers/cashiers.module').then(m => m.CashiersPageModule) },
  { path: 'wallet', loadChildren: () => import('./access/wallet/wallet.module').then(m => m.WalletPageModule) },
  { path: 'saleshistory', loadChildren: () => import('./access/transactions/saleshistory/saleshistory.module').then(m => m.SalesHistoryPageModule) },
  { path: 'orders', loadChildren: () => import('./access/ordermanager/orders/orders.module').then(m => m.OrdersPageModule) },
  { path: 'order', loadChildren: () => import('./access/ordermanager/order/order.module').then(m => m.OrderPageModule) },
  { path: 'rewardscancode', loadChildren: () => import('./access/reward/rewardscancode/rewardscancode.module').then(m => m.RewardScanCodePageModule) },
  { path: 'rewardinitialize', loadChildren: () => import('./access/reward/rewardinitialize/rewardinitialize.module').then(m => m.RewardInitializePageModule) },
  { path: 'rewardconfirm', loadChildren: () => import('./access/reward/rewardconfirm/rewardconfirm.module').then(m => m.RewardonfirmPageModule) },
  { path: 'tucpaytransactions', loadChildren: () => import('./access/tucpay/tucpay.module').then(m => m.TucPayHistoryPageModule) },
  { path: 'rewardhistory', loadChildren: () => import('./access/rewardhistory/rewardhistory.module').then(m => m.RewardHistoryPageModule) },
  { path: 'pendingrewardhistory', loadChildren: () => import('./access/pendingrewardshistory/pendingrewardshistory.module').then(m => m.PendingRewardHistoryPageModule) },
  { path: 'bankmanager', loadChildren: () => import("./access/v3/bankmanager/bankmanager.module").then(m => m.BankManagerPageModule) },
  { path: 'cashout', loadChildren: () => import("./access/v3/cashout/cashout.module").then(m => m.CashOutPageModule) },
  { path: 'reginfo', loadChildren: () => import("./registration/info/info.module").then(m => m.InfoPageModule) },
  { path: 'mobileverifypage', loadChildren: () => import("./registration/mobileverify/mobileverify.module").then(m => m.MobileVerifyPageModule) },
  { path: 'addresslocator', loadChildren: () => import("./registration/addresslocator/addresslocator.module").then(m => m.AddressLocatorPageModule) },

  { path: 'cashierdashboard', loadChildren: () => import("./access/cashieraccess/dashboard/dashboard.module").then(m => m.CashierDashboardPageModule) },


  { path: 'cashierscandealcode', loadChildren: () => import('./access/cashieraccess/deals/scandealcode/scandealcode.module').then(m => m.ScanDealCodePageModule) },
  { path: 'cashierredeemdealconfirm', loadChildren: () => import('./access/cashieraccess/deals/dealredeem/dealredeem.module').then(m => m.DealRedeemPageModule) },
  { path: 'cashierredeemhistory', loadChildren: () => import('./access/cashieraccess/deals/redeemhistory/redeemhistory.module').then(m => m.RedeemHistoryPageModule) },
  { path: 'cashierdealdetails', loadChildren: () => import('./access/cashieraccess/deals/dealdetails/dealdetails.module').then(m => m.DealDetailsPageModule) },


  { path: 'cashierrewardscancode', loadChildren: () => import('./access/cashieraccess/reward/rewardscancode/rewardscancode.module').then(m => m.RewardScanCodePageModule) },
  { path: 'cashierrewardinitialize', loadChildren: () => import('./access/cashieraccess/reward/rewardinitialize/rewardinitialize.module').then(m => m.RewardInitializePageModule) },
  { path: 'cashierrewardconfirm', loadChildren: () => import('./access/cashieraccess/reward/rewardconfirm/rewardconfirm.module').then(m => m.RewardonfirmPageModule) },
  { path: 'cashierrewardhistory', loadChildren: () => import('./access/cashieraccess/rewardhistory/rewardhistory.module').then(m => m.RewardHistoryPageModule) },
  { path: 'cashiertucpaytransactions', loadChildren: () => import('./access/cashieraccess/tucpay/tucpay.module').then(m => m.TucPayHistoryPageModule) },

  { path: 'redeemst1', loadChildren: () => import('./access/v3/redeem/st1/st1.module').then(m => m.RedeemSt1PageModule) },

  //Loyalty
  { path: 'loyaltydashboard', loadChildren: () => import('./access/loyalty/dashboard/dashboard.module').then(m => m.DashboardPageModule) },

  // Deals
  { path: 'dealsdashboard', loadChildren: () => import('./access/deals/dashboard/dashboard.module').then(m => m.DashboardPageModule) },
  { path: 'scandealcode', loadChildren: () => import('./access/deals/scandealcode/scandealcode.module').then(m => m.ScanDealCodePageModule) },
  { path: 'redeemdealconfirm', loadChildren: () => import('./access/deals/dealredeem/dealredeem.module').then(m => m.DealRedeemPageModule) },
  { path: 'redeemhistory', loadChildren: () => import('./access/deals/redeemhistory/redeemhistory.module').then(m => m.RedeemHistoryPageModule) },
  { path: 'deals', loadChildren: () => import('./access/deals/deals/deals.module').then(m => m.DealsPageModule) },
  { path: 'deal', loadChildren: () => import('./access/deals/deal/deal.module').then(m => m.DealPageModule) },
  { path: 'dealdetails', loadChildren: () => import('./access/deals/dealdetails/dealdetails.module').then(m => m.DealDetailsPageModule) },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
