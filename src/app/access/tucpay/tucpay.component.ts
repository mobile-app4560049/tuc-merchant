import { Component, ViewChild } from '@angular/core';
import { NavController, MenuController, AlertController, ModalController, IonDatetime } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from '../../service/helper.service';
import { OResponse, ODeviceInformation } from '../../service/object.service';
import { TransactionDetailsModal } from '../modals/transactiondetails/transactiondetails.modal.component';
declare var moment: any;
@Component({
    selector: 'app-tucpay',
    templateUrl: 'tucpay.component.html'
})
export class TucPayHistoryPage {
    @ViewChild("startTimePicker", { static: true }) _IonDatetime: IonDatetime;
    public DeviceInformation: ODeviceInformation;
    constructor(
        public _ModalController: ModalController,
        public _HelperService: HelperService,
        public _AlertController: AlertController,
    ) {
        var DeviceInformationStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
        if (DeviceInformationStorage != null) {
            this.DeviceInformation = DeviceInformationStorage;
        }
    }
    SelectedDate = null;
    Type = 1;
    StartTime = null;
    StartTimeS = null;
    EndTime = null;
    EndTimeS = null;
    public MinDate = null;
    public MaxDate = null;
    public SelectedItemDate = null;
    UserAnalytics_DateChange(Type) {
        this.Type = Type;
        var SDate;
        if (Type == 1) {
            SDate =
            {
                start: moment().startOf('day'),
                end: moment().endOf('day'),
            }
        }
        else if (Type == 2) {
            SDate =
            {
                start: moment().subtract(1, 'days').startOf('day'),
                end: moment().subtract(1, 'days').endOf('day'),
            }
        }
        else if (Type == 3) {
            SDate =
            {
                start: moment().startOf('isoWeek'),
                end: moment().endOf('isoWeek'),
            }
        }
        else if (Type == 4) {
            SDate =
            {
                start: moment().startOf('month'),
                end: moment().endOf('month'),
            }
        }
        else if (Type == 5) {
            this._IonDatetime.open();
        }
        if (Type != 5) {
            this.StartTime = SDate.start;
            this.EndTime = SDate.end;
            this.LoadData();
        }

    }
    CustomDateChange() {
        this.StartTime = moment(this.SelectedItemDate).startOf('day');
        this.EndTime = moment(this.SelectedItemDate).endOf('day');
        this.LoadData();
    }
    ngOnInit() {

        this.MaxDate = this._HelperService.FormatDate(moment().startOf('day'), "YYYY-MM-DD");
        this.MinDate = this._HelperService.FormatDate(moment(this._HelperService.AccountInfo.UserAccount.CreateDate).startOf('day'), "YYYY-MM-DD");
        if (this.StartTime == undefined) {
            this.StartTime = moment().startOf('day');
            this.EndTime = moment().endOf('day');
        }
        this.LoadData();

    }

    LoadData() {
        this.StartTimeS = this._HelperService.GetDateS(this.StartTime);
        this.EndTimeS = this._HelperService.GetDateS(this.EndTime);
        this.TranList_Data =
        {
            SearchContent: "",
            TotalRecords: 0,
            Offset: -1,
            Limit: 10,
            Data: []
        };
        this._HelperService.GetSalesSummary(this._HelperService.AccountOwner.AccountId, 108, 0, 0, this.StartTime, this.EndTime);
        this._HelperService.GetStoreVisits(this.StartTime, this.EndTime);
        this.TranList_Setup();
    }
    public TranList_Data =
        {
            SearchContent: "",
            TotalRecords: 0,
            Offset: -1,
            Limit: 10,
            Data: []
        };
    TranList_Setup() {
        if (this.TranList_Data.Offset == -1) {
            this._HelperService.ShowSpinner();
            this.TranList_Data.Offset = 0;
        }
        var SCon = "";

        if (this.TranList_Data.SearchContent != undefined && this.TranList_Data.SearchContent != null && this.TranList_Data.SearchContent != '') {
            SCon = this._HelperService.GetSearchCondition(SCon, 'UserDisplayName', this._HelperService.AppConfig.DataType.Text, this.TranList_Data.SearchContent);
            SCon = this._HelperService.GetSearchCondition(SCon, 'UserMobileNumber', this._HelperService.AppConfig.DataType.Text, this.TranList_Data.SearchContent);
            // SCon = this._HelperService.GetSearchCondition(SCon, 'CashierDisplayName', this._HelperService.AppConfig.DataType.Text, this.TranList_Data.SearchContent);
            // SCon = this._HelperService.GetSearchCondition(SCon, 'CreatedByDisplayName', this._HelperService.AppConfig.DataType.Text, this.TranList_Data.SearchContent);
            // SCon = this._HelperService.GetSearchCondition(SCon, 'SubParentDisplayName', this._HelperService.AppConfig.DataType.Text, this.TranList_Data.SearchContent);
            SCon = this._HelperService.GetSearchCondition(SCon, 'ReferenceNumber', this._HelperService.AppConfig.DataType.Text, this.TranList_Data.SearchContent);
        }

        SCon = this._HelperService.GetDateCondition(SCon, 'TransactionDate', this.StartTime, this.EndTime);
        SCon = this._HelperService.GetSearchConditionStrict(SCon, 'ParentId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AccountOwner.AccountId, '==');
        SCon = this._HelperService.GetSearchConditionStrict(SCon, 'TypeId', this._HelperService.AppConfig.DataType.Number, 658, '==');
        var pData = {
            Task: 'getredeemtransactions',
            TotalRecords: this.TranList_Data.TotalRecords,
            Offset: this.TranList_Data.Offset,
            Limit: this.TranList_Data.Limit,
            RefreshCount: true,
            SearchCondition: SCon,
            SortExpression: 'TransactionDate desc',
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.TUCTransCore, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideSpinner();
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this.TranList_Data.Offset = this.TranList_Data.Offset + this.TranList_Data.Limit;
                    this.TranList_Data.TotalRecords = _Response.Result.TotalRecords;
                    var TranList_Data = _Response.Result.Data;
                    TranList_Data.forEach(element => {
                        if (element.CardBrandName != undefined) {
                            element.CardBrandName = element.CardBrandName.toLowerCase().trim();
                        }
                        element.TypeName = element.TypeName.toLowerCase().trim();
                        element.TransactionDate = this._HelperService.GetDateTimeS(element.TransactionDate);
                        this.TranList_Data.Data.push(element);
                    });
                    if (this.LoaderEvent != undefined) {
                        this.LoaderEvent.target.complete();
                        if (this.TranList_Data.TotalRecords == this.TranList_Data.Data.length) {
                            this.LoaderEvent.target.disabled = true;
                        }
                    }
                }
                else {
                    this._HelperService.HideSpinner();
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    TranList_RowSelected(ReferenceData) {
        if (ReferenceData.PaymentStatusCode == 'paymentstatus.pending') {
            var _OrderDetails =
            {
                OrderId: ReferenceData.OrderId,
                OrderKey: ReferenceData.OrderReference,
            };
            this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveOrder, _OrderDetails);
            this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.orderpaymentprocess);
        }
        else {
            var _OrderDetails =
            {
                OrderId: ReferenceData.OrderId,
                OrderKey: ReferenceData.OrderReference,
            };
            this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveOrder, _OrderDetails);
            this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.orderdetails);
        }


    }
    public LoaderEvent: any = undefined;
    NextLoad(event) {
        this.LoaderEvent = event;
        this.TranList_Setup();

    }
    private delayTimer;
    doSearch(text) {
        clearTimeout(this.delayTimer);
        this.delayTimer = setTimeout(x => {
            this.TranList_Data =
            {
                SearchContent: text,
                TotalRecords: 0,
                Offset: -1,
                Limit: 10,
                Data: []
            };
            this.TranList_Setup();
        }, 1000);
    }
    async OpenAddModal(Item) {
        const modal = await this._ModalController.create({
            component: TransactionDetailsModal,
            componentProps: Item
        });
        return await modal.present();
    }
}