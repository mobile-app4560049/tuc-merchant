import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { RedeemSt1Page } from './st1.component';
import { ChartsModule } from 'ng2-charts';
import { Angular4PaystackModule } from 'angular4-paystack';
import { QRCodeModule } from 'angularx-qrcode';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ChartsModule,
        QRCodeModule,
        RouterModule.forChild([
            {
                path: '',
                component: RedeemSt1Page
            }
        ])
    ],
    declarations: [RedeemSt1Page]
})
export class RedeemSt1PageModule { }
