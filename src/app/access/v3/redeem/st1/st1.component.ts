import { Component, ViewChild } from '@angular/core';
import { NavController, MenuController, AlertController, ModalController, IonDatetime } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from '../../../../service/helper.service';
import { OResponse, ODeviceInformation } from '../../../../service/object.service';
import { DataService } from '../../../../service/data.service';
declare var moment: any;

@Component({
    selector: 'app-redeemst1',
    templateUrl: 'st1.component.html'
})
export class RedeemSt1Page {
    public DeviceInformation: ODeviceInformation;
    constructor(
        public _MenuController: MenuController,
        public _ModalController: ModalController,
        public _HelperService: HelperService,
        public _AlertController: AlertController,
        public _DataService: DataService,
    ) {
        var DeviceInformationStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
        if (DeviceInformationStorage != null) {
            this.DeviceInformation = DeviceInformationStorage;
        }
    }
    public ionViewDidEnter() {
    }
    public RedeemStage = 0;
    RedeemStageChange(Stage) {
        this._CustomerDetails =
        {
            ReferenceId: null,
            ReferenceKey: null,

            Name: null,
            MobileNumber: null,
            Pin: null,

            IconUrl: null,
            // StatusId: null,
            // IsNewCustomer: false,
            AccountBalance: 0,
            // RewardPercentage: 0,


            // EmailAddress: null,
            // GenderCode: null,
            // DateOfBirth: null,


            RedeemAmount: null,
            InvoiceAmount: null,
            // Comment: null,
            // AccountNumber: null,
            // CashierCode: null,
            TransactionReferenceId: 0,
            TransactionDate: null,
            StatusName: null,
        }
        this.RedeemStage = Stage;
    }

    _CustomerDetails =
        {
            ReferenceId: null,
            ReferenceKey: null,

            Name: null,
            MobileNumber: null,
            Pin: null,

            IconUrl: null,
            // StatusId: null,
            // IsNewCustomer: false,
            AccountBalance: 0,
            // RewardPercentage: 0,


            // EmailAddress: null,
            // GenderCode: null,
            // DateOfBirth: null,


            RedeemAmount: null,
            InvoiceAmount: null,
            // Comment: null,
            // AccountNumber: null,
            // CashierCode: null,
            TransactionReferenceId: 0,
            TransactionDate: null,
            StatusName: null,
        }
    ngOnInit() {
        // this._CustomerDetails = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference);
    }
    InitializeRedeemCheck() {
        if (this._CustomerDetails.MobileNumber == undefined || this._CustomerDetails.MobileNumber == null || isNaN(this._CustomerDetails.MobileNumber) == true) {
            this._HelperService.Notify('Invalid mobile number', "Please enter valid mobile number");
        }
        else if (this._CustomerDetails.Pin == undefined || this._CustomerDetails.Pin == null || isNaN(this._CustomerDetails.Pin) == true) {
            this._HelperService.Notify('Invalid pin', "Please enter valid redeem pin");
        }
        //    else if (this._CustomerDetails.InvoiceAmount == undefined || this._CustomerDetails.InvoiceAmount == null || isNaN(this._CustomerDetails.InvoiceAmount) == true) {
        //         this._HelperService.Notify('Invalid amount', "Please enter valid  purchase amount");
        //     }
        //     else if (this._CustomerDetails.InvoiceAmount < 1) {
        //         this._HelperService.Notify("Invalid amount", "Amount must be greater than 0");
        //     }
        else {
            this.InitializeRedeem();
        }
    }
    InitializeRedeem() {
        this._HelperService.ShowSpinner('Processing...');
        this._HelperService.IsFormProcessing = true;
        var pData = {
            Task: "redeeminitialize",
            AccountId: this._HelperService.AccountOwner.AccountId,
            AccountKey: this._HelperService.AccountOwner.AccountKey,
            MobileNumber: this._CustomerDetails.MobileNumber,
            Pin: this._CustomerDetails.Pin,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Rewards, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideSpinner();
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this._CustomerDetails.IconUrl = _Response.Result.IconUrl;
                    this._CustomerDetails.Name = _Response.Result.Name;
                    this._CustomerDetails.ReferenceId = _Response.Result.ReferenceId;
                    this._CustomerDetails.ReferenceKey = _Response.Result.ReferenceKey;
                    this._CustomerDetails.AccountBalance = _Response.Result.AccountBalance;
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference, _Response.Result);
                    this.RedeemStage = 2;
                    // this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Reward.Confirm);
                }
                else {

                    this._HelperService.Notify(_Response.Status, _Response.Message);
                }
            },
            _Error => {
                this._HelperService.HideSpinner();
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });
    }

    async ConfirmRedeemCheck() {
        if (this._CustomerDetails.InvoiceAmount == undefined || this._CustomerDetails.InvoiceAmount == null || isNaN(this._CustomerDetails.InvoiceAmount) == true) {
            this._HelperService.Notify('Invalid amount', "Please enter valid  purchase amount");
        }
        else if (this._CustomerDetails.InvoiceAmount < 1) {
            this._HelperService.Notify("Invalid amount", "Amount must be greater than 0");
        }
        else if (this._CustomerDetails.RedeemAmount == undefined || this._CustomerDetails.RedeemAmount == null || isNaN(this._CustomerDetails.RedeemAmount) == true) {
            this._HelperService.Notify('Invalid amount', "Please enter valid  redeem amount");
        }
        else if (this._CustomerDetails.RedeemAmount < 1) {
            this._HelperService.Notify("Invalid amount", "Redeem amount must be greater than 0");
        }
        else {
            const alert = await this._AlertController.create({
                header: 'Confirm redeem',
                message: 'Click on confirm to redeem ' + this._HelperService.AppConfig.CurrencySymbol + " " + this._CustomerDetails.InvoiceAmount + '.',
                buttons: [
                    {
                        text: 'Cancel',
                        role: 'cancel',
                        handler: () => {
                        }
                    },
                    {
                        text: 'Confirm',
                        cssClass: 'en-text-primary',
                        handler: () => {
                            this.ConfirmRedeem();
                        }
                    }
                ]
            });
            return await alert.present();
        }
    }
    ConfirmRedeem() {
        this._HelperService.ShowSpinner('Processing...');
        this._HelperService.IsFormProcessing = true;
        var pData = {
            Task: "redeemconfirm",
            AccountId: this._HelperService.AccountOwner.AccountId,
            AccountKey: this._HelperService.AccountOwner.AccountKey,

            ReferenceId: this._CustomerDetails.ReferenceId,
            ReferenceKey: this._CustomerDetails.ReferenceKey,

            RedeemAmount: this._CustomerDetails.RedeemAmount,
            InvoiceAmount: this._CustomerDetails.InvoiceAmount,
            // Comment: this._CustomerDetails.Comment,
            // AccountNumber: this._CustomerDetails.AccountNumber,
            // CashierCode: this._CustomerDetails.CashierCode,
            // Nmae: this._CustomerDetails.Name,
            MobileNumber: this._CustomerDetails.MobileNumber,
            Pin: this._CustomerDetails.Pin,
            // EmailAddress: this._CustomerDetails.EmailAddress,
            // GenderCode: this._CustomerDetails.GenderCode,
            // DateOfBirth: this._CustomerDetails.DateOfBirth,


        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Rewards, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideSpinner();
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    console.log(_Response);
                    this._CustomerDetails.TransactionReferenceId = _Response.Result.TransactionReferenceId;
                    this._CustomerDetails.TransactionDate = this._HelperService.GetDateTimeS(_Response.Result.TransactionDate);
                    this._CustomerDetails.StatusName = _Response.Result.StatusName;
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference, _Response.Result);
                    this.RedeemStage = 3;
                    // this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Reward.Confirm);
                }
                else {

                    this._HelperService.Notify(_Response.Status, _Response.Message);
                }
            },
            _Error => {
                this._HelperService.HideSpinner();
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });
    }

    ToggleGender(Gender) {
        // this._CustomerDetails.GenderCode = Gender;
    }
    // InitializeRedeem() {
    //     if (this._CustomerDetails.InvoiceAmount > 0 && this._CustomerDetails.RewardPercentage > 0) {
    //         var TPercentage = (this._CustomerDetails.InvoiceAmount * this._CustomerDetails.RewardPercentage) / 100;
    //         if (TPercentage > 0 && TPercentage < 100) {
    //             TPercentage = Math.round(TPercentage);
    //         }
    //         else {
    //             TPercentage = 0;
    //         }
    //         this._CustomerDetails.RewardAmount = TPercentage;
    //     }
    // }

    ProcessOnline_Cancel() {
        this._CustomerDetails =
        {
            ReferenceId: null,
            ReferenceKey: null,

            Name: null,
            MobileNumber: null,
            Pin: null,

            IconUrl: null,
            // StatusId: null,
            // IsNewCustomer: false,
            AccountBalance: 0,
            // RewardPercentage: 0,


            // EmailAddress: null,
            // GenderCode: null,
            // DateOfBirth: null,


            RedeemAmount: null,
            InvoiceAmount: null,
            // Comment: null,
            // AccountNumber: null,
            // CashierCode: null,
            TransactionReferenceId: 0,
            TransactionDate: null,
            StatusName: null,
        }
        this.RedeemStage = 0;
        this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Dashboard);
    }






    // LoadData() {
    //     this.TranList_Data =
    //         {
    //             SearchContent: "",
    //             TotalRecords: 0,
    //             Offset: -1,
    //             Limit: 10,
    //             Data: []
    //         };
    //     this.TranList_Setup();
    // }
    // public TranList_Data =
    //     {
    //         SearchContent: "",
    //         TotalRecords: 0,
    //         Offset: -1,
    //         Limit: 10,
    //         Data: []
    //     };
    // TranList_Setup() {
    //     // if (this.TranList_Data.Offset == -1) {
    //     //     this._HelperService.ShowSpinner();
    //     //     this.TranList_Data.Offset = 0;
    //     // }
    //     var SCon = "";
    //     // SCon = this._HelperService.GetSearchConditionStrict(SCon, 'UserAccountId', 'number', this._HelperService.AccountInfo.UserAccount.AccountId, "=");
    //     // if (this.TranList_Data.SearchContent != undefined && this.TranList_Data.SearchContent != null && this.TranList_Data.SearchContent != '') {
    //     //     SCon = this._HelperService.GetSearchCondition(SCon, 'UserDisplayName', this._HelperService.AppConfig.DataType.Text, this.TranList_Data.SearchContent);
    //     //     SCon = this._HelperService.GetSearchCondition(SCon, 'UserMobileNumber', this._HelperService.AppConfig.DataType.Text, this.TranList_Data.SearchContent);
    //     //     SCon = this._HelperService.GetSearchCondition(SCon, 'ReferenceNumber', this._HelperService.AppConfig.DataType.Text, this.TranList_Data.SearchContent);
    //     // }
    //     // SCon = this._HelperService.GetDateCondition(SCon, 'TransactionDate', this.StartTime, this.EndTime);
    //     // SCon = this._HelperService.GetSearchConditionStrict(SCon, 'ParentId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AccountOwner.AccountId, '==');
    //     var pData = {
    //         Task: 'getsaletransactions',
    //         TotalRecords: this.TranList_Data.TotalRecords,
    //         Offset: this.TranList_Data.Offset,
    //         Limit: this.TranList_Data.Limit,
    //         RefreshCount: true,
    //         SearchCondition: SCon,
    //         SortExpression: 'TransactionDate desc',
    //     };
    //     let _OResponse: Observable<OResponse>;
    //     _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.TUCTransCore, pData);
    //     _OResponse.subscribe(
    //         _Response => {
    //             this._HelperService.HideSpinner();
    //             if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
    //                 this.TranList_Data.Offset = this.TranList_Data.Offset + this.TranList_Data.Limit;
    //                 this.TranList_Data.TotalRecords = _Response.Result.TotalRecords;
    //                 var TranList_Data = _Response.Result.Data;
    //                 TranList_Data.forEach(element => {
    //                     if (element.CardBrandName != undefined) {
    //                         element.CardBrandName = element.CardBrandName.toLowerCase().trim();
    //                     }
    //                     element.TypeName = element.TypeName.toLowerCase().trim();
    //                     element.TransactionDate = this._HelperService.GetDateTimeS(element.TransactionDate);
    //                     this.TranList_Data.Data.push(element);
    //                 });
    //                 if (this.LoaderEvent != undefined) {
    //                     this.LoaderEvent.target.complete();
    //                     if (this.TranList_Data.TotalRecords == this.TranList_Data.Data.length) {
    //                         this.LoaderEvent.target.disabled = true;
    //                     }
    //                 }
    //             }
    //             else {
    //                 this._HelperService.HideSpinner();
    //             }
    //         },
    //         _Error => {
    //             this._HelperService.HandleException(_Error);
    //         });
    // }
    // TranList_RowSelected(ReferenceData) {
    //     if (ReferenceData.PaymentStatusCode == 'paymentstatus.pending') {
    //         var _OrderDetails =
    //         {
    //             OrderId: ReferenceData.OrderId,
    //             OrderKey: ReferenceData.OrderReference,
    //         };
    //         this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveOrder, _OrderDetails);
    //         this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.orderpaymentprocess);
    //     }
    //     else {
    //         var _OrderDetails =
    //         {
    //             OrderId: ReferenceData.OrderId,
    //             OrderKey: ReferenceData.OrderReference,
    //         };
    //         this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveOrder, _OrderDetails);
    //         this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.orderdetails);
    //     }


    // }
    // public LoaderEvent: any = undefined;
    // NextLoad(event) {
    //     this.LoaderEvent = event;
    //     this.TranList_Setup();

    // }

    // private delayTimer;
    // doSearch(text) {
    //     clearTimeout(this.delayTimer);
    //     this.delayTimer = setTimeout(x => {
    //         this.TranList_Data =
    //             {
    //                 SearchContent: text,
    //                 TotalRecords: 0,
    //                 Offset: -1,
    //                 Limit: 10,
    //                 Data: []
    //             };
    //         this.TranList_Setup();
    //     }, 1000);
    // }

    // async OpenAddModal(Item) {
    //     const modal = await this._ModalController.create({
    //         component: TransactionDetailsModal,
    //         componentProps: Item
    //     });
    //     return await modal.present();
    // }




}