import { Component } from '@angular/core';
import { ModalController, AlertController, NavParams } from '@ionic/angular';
import { HelperService } from '../../../../service/helper.service';
import { Observable } from 'rxjs';
import { OResponse } from '../../../../service/object.service';
@Component({
    templateUrl: 'modalcashoutdetails.component.html',
    selector: 'modal-modalcashoutdetails'
})
export class CashOutDetailsModal {
    public _TransactionDetails =
        {
            ReferenceId: null,
            ReferenceKey: null,
            BankAccountName: null,
            BankName: null,
            BankAccountNumber: null,
            StartDate: null,
            EndDate: null,
            Amount: null,
            Charge: null,
            TotalAmount: null,
            ReferenceNumber: null,
            StatusCode: null,
            StatusName: null,
            SystemComment: null,
        }
    constructor(
        public navParams: NavParams,
        public _ModalController: ModalController,
        public _AlertController: AlertController,
        public _HelperService: HelperService
    ) {
    }
    ngOnInit() {

        this._TransactionDetails = this.navParams.data as any;
    }
    async ModalDismiss(Type) {
        await this._ModalController.dismiss(Type);
    }
}