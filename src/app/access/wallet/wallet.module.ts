import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { WalletPage } from './wallet.component';
import { NgCalendarModule } from 'ionic2-calendar';
import { Angular4PaystackModule } from 'angular4-paystack';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        NgCalendarModule,
        // Angular4PaystackModule.forRoot('pk_live_f1a25b6e99480d01122947b682facf27572256a1'),
        Angular4PaystackModule.forRoot('pk_live_4acd36db0e852af843e16e83e59e7dc0f89efe12'),
        RouterModule.forChild([
            {
                path: '',
                component: WalletPage
            }
        ])
    ],
    declarations: [WalletPage]
})
export class WalletPageModule { }
