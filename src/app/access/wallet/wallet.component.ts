import { Component } from '@angular/core';
import { NavController, MenuController, AlertController, ModalController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from '../../service/helper.service';
declare let L;
import 'leaflet';
import { PlaceOrderModal } from '../modals/placeorder/placeorder.modal.component';
import { DataService } from '../../service/data.service';
import { DatePipe } from '@angular/common';
import { OResponse, OSalesSummary } from '../../service/object.service';
declare var moment: any;

@Component({
    selector: 'app-wallet',
    templateUrl: 'wallet.component.html'
})
export class WalletPage {
    // Calendar End
    constructor(
        public _DatePipe: DatePipe,
        public _HelperService: HelperService,
        public _AlertController: AlertController,
        public _MenuController: MenuController,
        public _ModalController: ModalController,
        public _DataService: DataService,
    ) {
        this._MenuController.enable(true);
    }




    ngOnInit() {

        this._HelperService.RefreshProfile();
        this._HelperService.RefreshLocation();
        this._MenuController.enable(true);
        var DeviceStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
        if (DeviceStorage != null) {
            this._HelperService.AppConfig.ActiveLocation.Lat = DeviceStorage.Latitude;
            this._HelperService.AppConfig.ActiveLocation.Lon = DeviceStorage.Longitude;
        }
        else {
        }
        this.LoadData();
    }

    LoadData() {
        this._PaymentDetails.transactionReference = this._HelperService.AccountOwner.AccountId + "O" + this._HelperService.GenerateGuid(),
            this._PaymentDetails.amount = 0;
        this.GetMerchantConfigurations();
    }





    public _Configurations = {
        RewardPercentage: null,
        RewardMaximumInvoiceAmount: null,
        RewardDeductionTypeCode: null,
        RewardDeductionType: null,
        MaximumRewardDeductionAmount: null,

        ThankUPlus: null,
        ThankUPlusCriteriaType: null,
        ThankUPlusCriteriaTypeCode: null,
        ThankUPlusCriteriaValue: null,
        ThankUPlusMinimumTransferAmount: null,

        SettlmentShedule: null,
        SettlmentSheduleCode: null,
        MinimumSettlementAmount: null,
        enablethankucash: null,
        merchantappvisiblity: null,
        merchantreward: null,
        merchantredeem: null,
        rewardsmstucplus: null,
        rewardsms: null,
        redeemsms: null,
        tucuserrewardonly: null,
        cagiftpoints: null,
        cagiftcards: null,
        securitymaximumtransactionperdaycap: 3,
        securitymaximuminvoiceamountperdaycap: 50000,
        userrewardpercentage: null,
    };
    GetMerchantConfigurations() {
        var PData =
        {
            Task: "getuserparameters",
            SearchCondition: this._HelperService.GetSearchConditionStrict('', 'StatusCode', this._HelperService.AppConfig.DataType.Text, 'default.active', '='),
            SortExpression: 'Name asc',
            Offset: 0,
            Limit: 1000,
        }
        PData.SearchCondition = this._HelperService.GetSearchConditionStrict(
            PData.SearchCondition,
            "TypeCode",
            this._HelperService.AppConfig.DataType.Text,
            'hcore.configurationvalue',
            "="
        );
        PData.SearchCondition = this._HelperService.GetSearchConditionStrict(
            PData.SearchCondition,
            "UserAccountKey",
            this._HelperService.AppConfig.DataType.Text,
            this._HelperService.AccountOwner.AccountKey,
            "="
        );
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V1.System, PData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    if (_Response.Result.Data != undefined) {
                        var ResposeData = _Response.Result.Data;
                        ResposeData.forEach(element => {
                            if (element.CommonSystemName == "rewardpercentage") {
                                this._Configurations.RewardPercentage = element.Value;
                            }
                            if (element.CommonSystemName == "rewardmaxinvoiceamount") {
                                this._Configurations.RewardMaximumInvoiceAmount = element.Value;
                            }
                            if (element.CommonSystemName == "rewarddeductiontype") {
                                this._Configurations.RewardDeductionType = element.HelperName;
                                this._Configurations.RewardDeductionTypeCode = element.HelperCode;
                                if (
                                    this._Configurations.RewardDeductionTypeCode == 'rewarddeductiontype.prepay' ||
                                    this._Configurations.RewardDeductionTypeCode == 'rewarddeductiontype.postpay' ||
                                    this._Configurations.RewardDeductionTypeCode == 'rewarddeductiontype.prepayandpostpay'
                                ) {
                                    this.GetBalance();
                                }
                            }
                            if (element.CommonSystemName == "maximumrewarddeductionamount") {
                                this._Configurations.MaximumRewardDeductionAmount = element.Value;
                            }
                            if (element.CommonSystemName == "cagiftpoints") {
                                this._Configurations.cagiftpoints = element.Value;
                            }
                            if (element.CommonSystemName == "cagiftcards") {
                                this._Configurations.cagiftcards = element.Value;
                            }
                            if (element.CommonSystemName == "userrewardpercentage") {
                                this._Configurations.userrewardpercentage = element.Value;
                            }

                            if (element.CommonSystemName == "settlementsheduletype") {
                                this._Configurations.SettlmentShedule = element.HelperName;
                                this._Configurations.SettlmentSheduleCode = element.HelperCode;
                            }
                            if (element.CommonSystemName == "minimumsettlementamount") {
                                this._Configurations.MinimumSettlementAmount = element.Value;
                            }

                            if (element.CommonSystemName == "thankucashplus") {
                                this._Configurations.ThankUPlus = element.Value;
                            }
                            if (element.CommonSystemName == "enablethankucash") {
                                this._Configurations.enablethankucash = element.Value;
                            }

                            if (element.CommonSystemName == "merchantappvisiblity") {
                                this._Configurations.merchantappvisiblity = element.Value;
                            }
                            if (element.CommonSystemName == "merchantreward") {
                                this._Configurations.merchantreward = element.Value;
                            }
                            if (element.CommonSystemName == "merchantredeem") {
                                this._Configurations.merchantredeem = element.Value;
                            }
                            if (element.CommonSystemName == "rewardsmstucplus") {
                                this._Configurations.rewardsmstucplus = element.Value;
                            }
                            if (element.CommonSystemName == "rewardsms") {
                                this._Configurations.rewardsms = element.Value;
                            }
                            if (element.CommonSystemName == "redeemsms") {
                                this._Configurations.redeemsms = element.Value;
                            }
                            if (element.CommonSystemName == "thankucashplusrewardcriteria") {
                                this._Configurations.ThankUPlusCriteriaValue = element.Value;
                                this._Configurations.ThankUPlusCriteriaType = element.HelperName;
                                this._Configurations.ThankUPlusCriteriaTypeCode = element.HelperCode;
                            }
                            if (element.CommonSystemName == "thankucashplusmintransferamount") {
                                this._Configurations.ThankUPlusMinimumTransferAmount = element.Value;
                            }

                            if (element.CommonSystemName == "securitymaximumtransactionperdaycap") {
                                this._Configurations.securitymaximumtransactionperdaycap = element.Value;
                            }
                            if (element.CommonSystemName == "securitymaximuminvoiceamountperdaycap") {
                                this._Configurations.securitymaximuminvoiceamountperdaycap = element.Value;
                            }
                            if (element.CommonSystemName == "tucuserrewardonly") {
                                this._Configurations.tucuserrewardonly = element.Value;
                            }
                        });
                    }
                }
                else {
                    this._HelperService.NotifySimple(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    public _PaymentDetails =
        {
            amount: 0,
            transactionReference: "",
        };
    public _AccountBalance =
        {
            Credit: 0,
            Debit: 0,
            Balance: 0
        }
    public GetBalance() {
        var pData = {
            Task: 'getuseraccountbalance',
            UserAccountKey: this._HelperService.AccountOwner.AccountKey,
            Source: "transaction.source.merchant"
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.TUCAccCore, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this._AccountBalance = _Response.Result;
                    this._AccountBalance.Balance = _Response.Result.Balance / 100;
                    this._AccountBalance.Credit = _Response.Result.Credit / 100;
                    this._AccountBalance.Debit = _Response.Result.Debit / 100;
                } else {
                    this._HelperService.NotifySimple(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }
        );
    }
    async StartPayment() {
        if (this._PaymentDetails.amount == undefined || this._PaymentDetails.amount == null) {
            this._HelperService.NotifySimple('Enter credit amount');
        }
        else if (isNaN(this._PaymentDetails.amount)) {
            this._HelperService.NotifySimple("Enter valid credit amount");
        }
        else if (this._PaymentDetails.amount < 1) {
            this._HelperService.NotifySimple("credit amount must be greater than 0");
        }
        else if (this._PaymentDetails.amount > 10000000) {
            this._HelperService.NotifySimple('Enter valid credit amount');
        }
        else {
            const alert = await this._AlertController.create({
                header: 'Confirm payment ',
                message: this._HelperService.AppConfig.CurrencySymbol + '' + this._PaymentDetails.amount + '  amount will be credited to your reward wallet. Do you want to continue ?',
                buttons: [
                    {
                        text: 'Edit',
                        role: 'cancel',
                        handler: () => {
                        }
                    },
                    {
                        text: 'Confirm',
                        cssClass: 'c-en-b-light',
                        handler: () => {
                            this._PaymentDetails.transactionReference = this._HelperService.AccountOwner.AccountId + "O" + this._HelperService.GenerateGuid(),
                                document.getElementById('paymentbutton').click();
                        }
                    }
                ]
            });
            return await alert.present();
        }
    }
    ProcessOnline_Confirm(_OnlineReference: any) {
        if (_OnlineReference.status == "success") {
            this._HelperService.ShowSpinner('Processing payment ...');
            var PostData = {
                Task: "creditmerchantwallet",
                AccountId: this._HelperService.AccountOwner.AccountId,
                InvoiceAmount: this._PaymentDetails.amount,
                ReferenceNumber: this._PaymentDetails.transactionReference,
            };
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.ThankU, PostData);
            _OResponse.subscribe(
                _Response => {
                    this._HelperService.HideSpinner();
                    if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                        this._HelperService.Notify('Payment successful', this._PaymentDetails.amount + " credited to account.");
                        this._PaymentDetails.amount = 0;
                        this._PaymentDetails.transactionReference = null;
                        this.GetMerchantConfigurations();
                        this.GetBalance();
                    } else {
                        this._HelperService.NotifySimple(_Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.HideSpinner();
                    this._HelperService.HandleException(_Error);
                }
            );
        }
        else {
            this.ProcessOnline_Cancel();
        }
    }
    ProcessOnline_Cancel() {
        this._HelperService.NotifySimple("Payment cancelled");
    }
}