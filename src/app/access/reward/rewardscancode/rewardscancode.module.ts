import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { RewardScanCodePage } from './rewardscancode.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild([
            {
                path: '',
                component: RewardScanCodePage
            }
        ])
    ],
    declarations: [RewardScanCodePage]
})
export class RewardScanCodePageModule { }
