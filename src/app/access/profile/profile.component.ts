import { Component } from '@angular/core';
import { NavController, MenuController, AlertController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from '../../service/helper.service';
import { OResponse, ODeviceInformation } from '../../service/object.service';

@Component({
    selector: 'app-accessprofile',
    templateUrl: 'profile.component.html'
})
export class AccessProfilePage {
    public DeviceInformation: ODeviceInformation;
    constructor(
        public _HelperService: HelperService,
        public _AlertController: AlertController,
    ) {
        var DeviceInformationStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
        if (DeviceInformationStorage != null) {
            this.DeviceInformation = DeviceInformationStorage;
        }
    }



    ngOnInit() {
        this.GetProfile();
    }


    UpdateProfile() {
        if (this._Profile.DisplayName == undefined || this._Profile.DisplayName == null || this._Profile.DisplayName == "") {
            this._HelperService.NotifySimple('Enter business display name');
        }
        else if (this._Profile.Name == undefined || this._Profile.Name == null || this._Profile.Name == "") {
            this._HelperService.NotifySimple('Enter business/company name');
        }
        else if (this._Profile.ContactNumber == undefined || this._Profile.ContactNumber == null || this._Profile.ContactNumber == "") {
            this._HelperService.NotifySimple('Enter business contact number');
        }
        else if (this._Profile.EmailAddress == undefined || this._Profile.EmailAddress == null || this._Profile.EmailAddress == "") {
            this._HelperService.NotifySimple('Enter business email address');
        }
        else if (this._Profile.Address == undefined || this._Profile.Address == null || this._Profile.Address == "") {
            this._HelperService.NotifySimple('Enter business address');
        }
        else if (this._Profile.FirstName == undefined || this._Profile.FirstName == null || this._Profile.FirstName == "") {
            this._HelperService.NotifySimple('Enter contact person first name');
        }
        else if (this._Profile.LastName == undefined || this._Profile.LastName == null || this._Profile.LastName == "") {
            this._HelperService.NotifySimple('Enter contact person last name');
        }
        else if (this._Profile.SecondaryEmailAddress == undefined || this._Profile.EmailAddress == null || this._Profile.EmailAddress == "") {
            this._HelperService.NotifySimple('Enter contact person email address');
        }
        else if (this._Profile.MobileNumber == undefined || this._Profile.MobileNumber == null || this._Profile.MobileNumber == "") {
            this._HelperService.NotifySimple('Enter contact person mobile number');
        }
        else {
            this.AppLogin();
        }
    }

    AppLogin() {
        var _RequestData = {
            Task: "updateuseraccount",
            ReferenceKey: this._HelperService.AccountOwner.AccountKey,
            DisplayName: this._Profile.DisplayName,
            Description: this._Profile.Description,
            // UserName: this._Profile.UserName,
            // Password: this._Profile.Password,
            Name: this._Profile.Name,
            FirstName: this._Profile.FirstName,
            LastName: this._Profile.LastName,
            MobileNumber: this._Profile.MobileNumber,
            ContactNumber: this._Profile.ContactNumber,
            EmailAddress: this._Profile.EmailAddress,
            SecondaryEmailAddress: this._Profile.SecondaryEmailAddress,
            Address: this._Profile.Address,
            WebsiteUrl: this._Profile.WebsiteUrl,
            OsName: this.DeviceInformation.OsName,
            OsVersion: this.DeviceInformation.OsVersion,
            SerialNumber: this.DeviceInformation.SerialNumber,
            Brand: this.DeviceInformation.Brand,
            Model: this.DeviceInformation.Model,
            Width: this.DeviceInformation.Width,
            Height: this.DeviceInformation.Height,
            CarrierName: this.DeviceInformation.CarrierName,
            Mcc: this.DeviceInformation.Mcc,
            Mnc: this.DeviceInformation.Mnc,
        };
        this._HelperService.ShowSpinner('updating profile ...');
        try {
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.System, _RequestData);
            _OResponse.subscribe(
                (_Response) => {
                    this._HelperService.HideSpinner();
                    if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                        this._HelperService.NotifySimple('Profile updated');
                    } else {
                        this._HelperService.Notify('Update failed', _Response.Message);
                    }
                },
                (_Error) => {
                    this._HelperService.HideSpinner();
                    this._HelperService.HandleException(_Error);
                }
            );
        } catch (_Error) {
            this._HelperService.HideSpinner();
            if (_Error.status == 0) {
                this._HelperService.Notify('Operation failed', "Please check your internet connection");
            }
            else if (_Error.status == 401) {
                var EMessage = JSON.parse(_Error._body).error;
                this._HelperService.Notify('Operation failed', EMessage + ' Unable to start verification. Please contact support')

            }
            else {
                this._HelperService.Notify('Operation failed', ' Unable to start verification. Please contact support')
            }
        }
    }


    public _Profile =
        {
            ReferenceId: null,
            ReferenceKey: null,
            AccountTypeCode: null,
            AccountTypeName: null,
            AccountOperationTypeCode: null,
            AccountOperationTypeName: null,
            OwnerIconUrl: null,
            DisplayName: null,
            AccessPin: null,
            AccountCode: null,
            IconUrl: null,
            PosterUrl: null,
            ReferralCode: null,
            Description: null,
            RegistrationSourceCode: null,
            RegistrationSourceName: null,
            AppKey: null,
            AppName: null,
            AppVersionKey: null,
            AppVersionName: null,
            LastLoginDate: null,
            RequestKey: null,
            CreateDate: null,
            CreatedByKey: null,
            CreatedByDisplayName: null,
            CreatedByIconUrl: null,
            ModifyDate: null,
            ModifyByKey: null,
            ModifyByDisplayName: null,
            ModifyByIconUrl: null,
            StatusId: null,
            StatusCode: null,
            StatusName: null,
            UserName: null,
            Password: null,
            SystemPassword: null,
            Name: null,
            FirstName: null,
            LastName: null,
            MobileNumber: null,
            ContactNumber: null,
            EmailAddress: null,
            SecondaryEmailAddress: null,
            Address: null,
            Latitude: null,
            Longitude: null,
            CountryKey: null,
            CountryName: null,
            WebsiteUrl: null,
            EmailVerificationStatus: null,
            EmailVerificationStatusDate: null,
            NumberVerificationStatus: null,
            NumberVerificationStatusDate: null,
            SubAccounts: null,
        }

    GetProfile() {
        this._HelperService.ShowSpinner();
        var pData = {
            Task: "getuseraccount",
            Reference: this._HelperService.GetSearchConditionStrict('', 'ReferenceKey', this._HelperService.AppConfig.DataType.Text, this._HelperService.AccountOwner.AccountKey, '='),
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.System, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this._HelperService.HideSpinner();
                    this._Profile = _Response.Result;
                }
                else {
                    this._HelperService.HideSpinner();
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
}