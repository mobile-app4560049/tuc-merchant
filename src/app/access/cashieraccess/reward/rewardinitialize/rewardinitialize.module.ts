import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { RewardInitializePage } from './rewardinitialize.component';
import { ChartsModule } from 'ng2-charts';
import { Angular4PaystackModule } from 'angular4-paystack';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ChartsModule,
        RouterModule.forChild([
            {
                path: '',
                component: RewardInitializePage
            }
        ])
    ],
    declarations: [RewardInitializePage]
})
export class RewardInitializePageModule { }
