import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { RewardConfirmPage } from './rewardconfirm.component';
import { ChartsModule } from 'ng2-charts';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ChartsModule,
        RouterModule.forChild([
            {
                path: '',
                component: RewardConfirmPage
            }
        ])
    ],
    declarations: [RewardConfirmPage]
})
export class RewardonfirmPageModule { }
