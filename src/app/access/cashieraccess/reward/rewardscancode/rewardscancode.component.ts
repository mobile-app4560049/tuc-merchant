import { Component } from '@angular/core';
import { NavController, MenuController, AlertController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from '../../../../service/helper.service';
import { OResponse, ODeviceInformation } from '../../../../service/object.service';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner/ngx';


@Component({
    selector: 'app-rewardscancode',
    templateUrl: 'rewardscancode.component.html'
})
export class RewardScanCodePage {
    public DeviceInformation: ODeviceInformation;
    constructor(
        private qrScanner: QRScanner,
        public _HelperService: HelperService,
        public _AlertController: AlertController,
    ) {
        var DeviceInformationStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
        if (DeviceInformationStorage != null) {
            this.DeviceInformation = DeviceInformationStorage;
        }
    }
    ngOnInit() {
        // this.DealCode = '2349009009000';
        // this.Redeem_Initialize();
        setTimeout(() => {
            this.qrScanner.prepare()
                .then((status: QRScannerStatus) => {
                    if (status.authorized) {
                        this.qrScanner.show().then(x => {
                            let scanSub = this.qrScanner.scan().subscribe((text: string) => {
                                this.DealCode = text.trim();
                                this.qrScanner.hide(); // hide camera preview
                                scanSub.unsubscribe(); // stop scanning
                                this.Redeem_Initialize();
                            });
                        });

                    } else if (status.denied) {
                        this._HelperService.NotifySimple('Camera permission required to scan qr code');
                    } else {
                        this._HelperService.NotifySimple('Camera permission required to scan qr code');
                        // permission was denied, but not permanently. You can ask for permission again at a later time.
                    }
                })
                .catch((e: any) => console.log('Error is', e));
        }, 300);
    }

    // public CashierCode = null;
    public DealCode = null;
    public MobileNumber = null;
    public Redeem_Initialize() {
        if ((this.DealCode == undefined || this.DealCode == null || this.DealCode == '') && (this.MobileNumber == undefined || this.MobileNumber == null || this.MobileNumber == '')) {
            this._HelperService.NotifySimple('Enter customer mobile number or scan customer code.');
        }
        else {
            this._HelperService.ShowSpinner("please wait");
            var pData = {
                Task: 'rewardinitialize',
                AccountId: this._HelperService.AccountOwner.AccountId,
                AccountKey: this._HelperService.AccountOwner.AccountKey,
                AccountNumber: this.DealCode,
                MobileNumber: this.MobileNumber
            };
            this._HelperService.IsFormProcessing = true;
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Rewards, pData);
            _OResponse.subscribe(
                _Response => {
                    this._HelperService.HideSpinner();
                    this._HelperService.IsFormProcessing = false;
                    if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference, _Response.Result);
                        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.CashierAccess.Reward.Initialize);
                    } else {
                        this._HelperService.HideSpinner();
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.NotifySimple(_Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.HideSpinner();
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.HandleException(_Error);
                }
            );
        }
    }

    Redeem_Initialize_Cancel() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.CashierAccess.cashierdashboard);
    }

}