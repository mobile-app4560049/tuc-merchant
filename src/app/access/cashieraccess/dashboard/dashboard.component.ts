import { Component } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { HelperService } from '../../../service/helper.service';

@Component({
    selector: 'app-cashierdashboard',
    templateUrl: 'dashboard.component.html',
})
export class CashierDashboardPage {

    constructor(
        public _HelperService: HelperService,
        public _AlertController: AlertController
    ) {

    }

    NavScanDeal() {
        this._HelperService.NavigateRootNoAnimate(this._HelperService.AppConfig.Pages.Access.CashierAccess.Deals.scandealcode);
    }
    NavScanDealHistory() {
        this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.CashierAccess.Deals.redeemhistory);
    }
    NavReward() {
        this._HelperService.NavigateRootNoAnimate(this._HelperService.AppConfig.Pages.Access.CashierAccess.Reward.ScanCode);
    }
    NavRewardHistory() {
        this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.CashierAccess.Reward.RewardHistory);
    }

    NavTucPayTransactions() {
        this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.CashierAccess.cashiertucpaytransactions);
    }
    ShowMyCode() { }
    async Logout() {
        const alert = await this._AlertController.create({
            header: 'Do you want to logout ?',
            message: 'Click on confirm to logout.',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: () => {
                    }
                },
                {
                    text: 'Confirm',
                    cssClass: 'en-text-primary',
                    handler: () => {
                        this._HelperService.DeleteStorage(this._HelperService.AppConfig.StorageHelper.Account);
                        this._HelperService.DeleteStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference);
                        this._HelperService.DeleteStorage(this._HelperService.AppConfig.StorageHelper.Verification);
                        this._HelperService.DeleteStorage(this._HelperService.AppConfig.StorageHelper.Stations);
                        this._HelperService.DeleteStorage(this._HelperService.AppConfig.StorageHelper.Products);
                        this._HelperService.DeleteStorage(this._HelperService.AppConfig.StorageHelper.PaymentsHistory);
                        this._HelperService.DeleteStorage(this._HelperService.AppConfig.StorageHelper.OrdersHistory);
                        this._HelperService.DeleteStorage(this._HelperService.AppConfig.StorageHelper.ActiveOrder);
                        this._HelperService.DeleteStorage(this._HelperService.AppConfig.StorageHelper.SelectedProduct);
                        this._HelperService.DeleteStorage(this._HelperService.AppConfig.StorageHelper.CustomLocation);
                        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Auth.Login);
                    }
                }
            ]
        });
        return await alert.present();
    }
}
