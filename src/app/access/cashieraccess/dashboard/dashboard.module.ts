import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { CashierDashboardPage } from './dashboard.component';

import { QRCodeModule } from 'angularx-qrcode';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner/ngx';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule, QRCodeModule,
        RouterModule.forChild([
            {
                path: '',
                component: CashierDashboardPage
            }
        ])
    ],
    declarations: [CashierDashboardPage]
})
export class CashierDashboardPageModule { }
