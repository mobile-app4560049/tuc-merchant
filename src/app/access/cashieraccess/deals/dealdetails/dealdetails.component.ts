import { Component } from '@angular/core';
import { NavController, MenuController, AlertController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from '../../../../service/helper.service';
import { OResponse, ODeviceInformation } from '../../../../service/object.service';

@Component({
    selector: 'app-dealdetails',
    templateUrl: 'dealdetails.component.html'
})
export class DealDetailsPage {
    public DeviceInformation: ODeviceInformation;
    constructor(
        public _HelperService: HelperService,
        public _AlertController: AlertController,
    ) {
        var DeviceInformationStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
        if (DeviceInformationStorage != null) {
            this.DeviceInformation = DeviceInformationStorage;
        }
    }
    _DealInfo: any =
        {

            ReferenceId: null,
            ReferenceKey: null,
            DealReferenceId: null,
            DealReferenceKey: null,
            UseDate: null,
            UseLocationId: null,
            UseLocationKey: null,
            UseLocationDisplayName: null,
            UseLocationAddress: null,
            ItemCode: null,
            StartDate: null,
            EndDate: null,
            StatusCode: null,
            StatusName: null,
            Amount: null,
            Title: null,
            Description: null,
            ImageUrl: null,
            UsageInformation: null,
            Terms: null,
            MerchantReferenceId: null,
            MerchantReferenceKey: null,
            MerchantDisplayName: null,
            MerchantIconUrl: null,
            RedeemInstruction: null,
            StatusId: null,
            CreateDate: null,
            DealStartDate: null,
            DealEndDate: null,
            CustomerId: null,
            CustomerKey: null,
            CustomerDisplayName: null,
            CustomerMobileNumber: null,
            CustomerIconUrl: null,
            Locations: [],
        }
    ngOnInit() {
        this._DealInfo = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference);
        if (this._DealInfo == null) {
            this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.CashierAccess.cashierdashboard);
        }
        else {
            this.GetMerchantDealCodeDetails();
        }
    }

    Close() {
        this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.CashierAccess.Deals.redeemhistory);
    }

    public GetMerchantDealCodeDetails() {
        this._HelperService.ShowSpinner();
        var pData = {
            Task: 'getmerchantdealcodedetails',
            AccountId: this._HelperService.AccountOwner.AccountId,
            AccountKey: this._HelperService.AccountOwner.AccountKey,
            ReferenceKey: this._DealInfo.ReferenceKey,
            ReferenceId: this._DealInfo.ReferenceId,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Deals, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideSpinner();
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this._DealInfo = _Response.Result;
                } else {
                    this._HelperService.NotifySimple(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }
        );

    }

}