import { Component } from '@angular/core';
import { NavController, MenuController, AlertController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from '../../../../service/helper.service';
import { OResponse, ODeviceInformation } from '../../../../service/object.service';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner/ngx';


@Component({
    selector: 'app-scandealcode',
    templateUrl: 'scandealcode.component.html'
})
export class ScanDealCodePage {
    public DeviceInformation: ODeviceInformation;
    constructor(
        private qrScanner: QRScanner,
        public _HelperService: HelperService,
        public _AlertController: AlertController,
    ) {
        var DeviceInformationStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
        if (DeviceInformationStorage != null) {
            this.DeviceInformation = DeviceInformationStorage;
        }
    }
    ngOnInit() {
        setTimeout(() => {
            this.qrScanner.prepare()
                .then((status: QRScannerStatus) => {
                    if (status.authorized) {
                        // camera permission was granted
                        // start scanning
                        this.qrScanner.show().then(x => {
                            let scanSub = this.qrScanner.scan().subscribe((text: string) => {
                                this.DealCode = text;
                                this.qrScanner.hide(); // hide camera preview
                                scanSub.unsubscribe(); // stop scanning
                                this.Redeem_Initialize();
                            });
                        });

                    } else if (status.denied) {
                        this._HelperService.NotifySimple('Camera permission required to scan qr code');
                        // camera permission was permanently denied
                        // you must use QRScanner.openSettings() method to guide the user to the settings page
                        // then they can grant the permission from there
                    } else {
                        this._HelperService.NotifySimple('Camera permission required to scan qr code');
                        // permission was denied, but not permanently. You can ask for permission again at a later time.
                    }
                })
                .catch((e: any) => console.log('Error is', e));
        }, 300);
    }

    public DealCode = null;
    public Redeem_Initialize() {
        if (this.DealCode == undefined || this.DealCode == null || this.DealCode == '') {
            this._HelperService.NotifySimple('Enter deal redeem code');
        }
        else {
            this._HelperService.ShowSpinner("validating code ...");
            var pData = {
                Task: 'dealredeeminitialize',
                AccountId: this._HelperService.AccountOwner.AccountId,
                AccountKey: this._HelperService.AccountOwner.AccountKey,
                ReferenceKey: this.DealCode,
            };
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Deals, pData);
            _OResponse.subscribe(
                _Response => {
                    this._HelperService.HideSpinner();
                    if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                        _Response.Result.StartDate = this._HelperService.GetDateTimeS(_Response.Result.StartDate);
                        _Response.Result.EndDate = this._HelperService.GetDateTimeS(_Response.Result.EndDate);
                        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference, _Response.Result);
                        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.CashierAccess.Deals.redeemdealconfirm);
                    } else {
                        this._HelperService.NotifySimple(_Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.HandleException(_Error);
                }
            );
        }

    }

    Redeem_Initialize_Cancel() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.CashierAccess.cashierdashboard);
    }

}