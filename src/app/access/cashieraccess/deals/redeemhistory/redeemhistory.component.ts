import { Component } from '@angular/core';
import { NavController, MenuController, AlertController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from '../../../../service/helper.service';
import { OResponse, ODeviceInformation } from '../../../../service/object.service';

@Component({
    selector: 'app-redeemhistory',
    templateUrl: 'redeemhistory.component.html'
})
export class RedeemHistoryPage {
    public DeviceInformation: ODeviceInformation;
    constructor(
        public _HelperService: HelperService,
        public _AlertController: AlertController,
    ) {
        var DeviceInformationStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
        if (DeviceInformationStorage != null) {
            this.DeviceInformation = DeviceInformationStorage;
        }
    }
    ngOnInit() {
        this.StoresList_Data =
        {
            SearchContent: "",
            TotalRecords: 0,
            Offset: -1,
            Limit: 10,
            Data: []
        };
        this.StoresList_Setup();
    }

    public StoresList_Data =
        {
            SearchContent: "",
            TotalRecords: 0,
            Offset: -1,
            Limit: 10,
            Data: []
        };
    StoresList_Setup() {
        if (this.StoresList_Data.Offset == -1) {
            this._HelperService.ShowSpinner();
            this.StoresList_Data.Offset = 0;
        }
        var SCon = "";
        if (this.StoresList_Data.SearchContent != undefined && this.StoresList_Data.SearchContent != null && this.StoresList_Data.SearchContent != '') {
            SCon = this._HelperService.GetSearchCondition(SCon, 'Title', this._HelperService.AppConfig.DataType.Text, this.StoresList_Data.SearchContent);
            SCon = this._HelperService.GetSearchCondition(SCon, 'CustomerDisplayName', this._HelperService.AppConfig.DataType.Text, this.StoresList_Data.SearchContent);
            SCon = this._HelperService.GetSearchCondition(SCon, 'CustomerMobileNumber', this._HelperService.AppConfig.DataType.Text, this.StoresList_Data.SearchContent);
        }
        // SCon = this._HelperService.GetSearchConditionStrict(SCon, 'OwnerId', this._HelperService.AppConfig.DataType.Text, this._HelperService.AccountOwner.AccountId, '==');
        var pData = {
            Task: 'getmerchantredeemhistory',
            TotalRecords: this.StoresList_Data.TotalRecords,
            Offset: this.StoresList_Data.Offset,
            Limit: this.StoresList_Data.Limit,
            RefreshCount: true,
            SearchCondition: SCon,
            SortExpression: 'UseDate desc',
            AccountId: this._HelperService.AccountOwner.AccountId,
            AccountKey: this._HelperService.AccountOwner.AccountKey,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Deals, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideSpinner();
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this.StoresList_Data.Offset = this.StoresList_Data.Offset + this.StoresList_Data.Limit;
                    this.StoresList_Data.TotalRecords = _Response.Result.TotalRecords;
                    var StoresList_Data = _Response.Result.Data;
                    StoresList_Data.forEach(element => {
                        element.CreateDate = this._HelperService.GetDateTimeS(element.CreateDate);
                        element.UseDate = this._HelperService.GetDateTimeS(element.UseDate);
                        element.LastTransactionDate = this._HelperService.GetDateTimeS(element.LastTransactionDate);
                        this.StoresList_Data.Data.push(element);
                    });
                    if (this.LoaderEvent != undefined) {
                        this.LoaderEvent.target.complete();
                        if (this.StoresList_Data.TotalRecords == this.StoresList_Data.Data.length) {
                            this.LoaderEvent.target.disabled = true;
                        }
                    }
                }
                else {
                    this._HelperService.HideSpinner();
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }

    public LoaderEvent: any = undefined;
    NextLoad(event) {
        this.LoaderEvent = event;
        this.StoresList_Setup();
    }
    NavScanDeal() {
        var PreviousPage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.NavPreviousPageReference);
        if (PreviousPage != null && PreviousPage == this._HelperService.AppConfig.Pages.Access.CashierAccess.Reward.RewardHistory) {
            this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Deals.scandealcode);
        }
        else {
            this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Deals.scandealcode);
        }
    }
    NavStore(Item) {
        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference, Item);
        this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.CashierAccess.Deals.dealdetails);
    }

    private delayTimer;
    doSearch(text) {
        clearTimeout(this.delayTimer);
        this.delayTimer = setTimeout(x => {
            this.StoresList_Data =
            {
                SearchContent: text,
                TotalRecords: 0,
                Offset: -1,
                Limit: 10,
                Data: []
            };
            this.StoresList_Setup();
        }, 1000);
    }
}