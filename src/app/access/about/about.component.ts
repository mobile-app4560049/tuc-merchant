import { Component } from '@angular/core';
import { HelperService } from '../../service/helper.service';

@Component({
    selector: 'app-about',
    templateUrl: 'about.component.html',
})
export class AboutPage {

    constructor(
        public _HelperService: HelperService
    ) { }

}
