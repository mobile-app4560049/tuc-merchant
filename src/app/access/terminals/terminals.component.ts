import { Component } from '@angular/core';
import { NavController, MenuController, AlertController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from '../../service/helper.service';
import { OResponse, ODeviceInformation, OTerminalStatusCount } from '../../service/object.service';

@Component({
    selector: 'app-terminals',
    templateUrl: 'terminals.component.html'
})
export class TerminalsPage {
    public DeviceInformation: ODeviceInformation;
    constructor(
        public _HelperService: HelperService,
        public _AlertController: AlertController,
    ) {
        var DeviceInformationStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
        if (DeviceInformationStorage != null) {
            this.DeviceInformation = DeviceInformationStorage;
        }
    }
    ngOnInit() {
        this.TerminalsList_Data =
            {
                SearchContent: "",
                TotalRecords: 0,
                Offset: -1,
                Limit: 10,
                Data: []
            };
        this.TerminalsList_Setup();
        this.GetTerminalStatusCount(this._HelperService.AccountOwner.AccountId, 108, 0, 0);
    }
    public _TerminalStatusCount: OTerminalStatusCount =
        {

            Total: 0,
            Unused: 0,
            Active: 0,
            Idle: 0,
            Dead: 0,
        };

    GetTerminalStatusCount(UserAccountId, UserAccountTypeId, SubUserAccountId, SubUserAccountTypeId) {
        var Data = {
            Task: "getterminalstatuscount",
            UserAccountId: UserAccountId,
            UserAccountTypeId: UserAccountTypeId,
            SubUserAccountId: SubUserAccountId,
            SubUserAccountTypeId: SubUserAccountTypeId,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.TUCAnalytics, Data);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this._TerminalStatusCount = _Response.Result as OTerminalStatusCount;
                } else {
                    this._HelperService.NotifySimple(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }
        );
    }
    public TerminalsList_Data =
        {
            SearchContent: "",
            TotalRecords: 0,
            Offset: -1,
            Limit: 10,
            Data: []
        };
    TerminalsList_Setup() {
        if (this.TerminalsList_Data.Offset == -1) {
            this._HelperService.ShowSpinner();
            this.TerminalsList_Data.Offset = 0;
        }
        var SCon = "";
        if (this.TerminalsList_Data.SearchContent != undefined && this.TerminalsList_Data.SearchContent != null && this.TerminalsList_Data.SearchContent != '') {
            SCon = this._HelperService.GetSearchCondition(SCon, 'DisplayName', this._HelperService.AppConfig.DataType.Text, this.TerminalsList_Data.SearchContent);
            SCon = this._HelperService.GetSearchCondition(SCon, 'Address', this._HelperService.AppConfig.DataType.Text, this.TerminalsList_Data.SearchContent);
            SCon = this._HelperService.GetSearchCondition(SCon, 'AcquirerDisplayName', this._HelperService.AppConfig.DataType.Text, this.TerminalsList_Data.SearchContent);
            SCon = this._HelperService.GetSearchCondition(SCon, 'ProviderDisplayName', this._HelperService.AppConfig.DataType.Text, this.TerminalsList_Data.SearchContent);
        }

        SCon = this._HelperService.GetSearchConditionStrict(SCon, 'MerchantId', this._HelperService.AppConfig.DataType.Text, this._HelperService.AccountOwner.AccountId, '==');
        // SCon = this._HelperService.GetSearchConditionStrict(SCon, 'StatusCode', this._HelperService.AppConfig.DataType.Text, "default.active", '==');
        if (this.SelectedAppStatus != 0) {
            SCon = this._HelperService.GetSearchConditionStrict(SCon, 'ActivityStatusId', this._HelperService.AppConfig.DataType.Number, this.SelectedAppStatus, '=');
        }
        var pData = {
            Task: 'getterminals',
            TotalRecords: this.TerminalsList_Data.TotalRecords,
            Offset: this.TerminalsList_Data.Offset,
            Limit: this.TerminalsList_Data.Limit,
            RefreshCount: true,
            SearchCondition: SCon,
            SortExpression: 'DisplayName asc',
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.TUCAccCore, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideSpinner();
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this.TerminalsList_Data.Offset = this.TerminalsList_Data.Offset + this.TerminalsList_Data.Limit;
                    this.TerminalsList_Data.TotalRecords = _Response.Result.TotalRecords;
                    var TerminalsList_Data = _Response.Result.Data;
                    TerminalsList_Data.forEach(element => {
                        element.CreateDate = this._HelperService.GetDateTimeS(element.CreateDate);
                        element.LastTransactionDate = this._HelperService.GetDateTimeS(element.LastTransactionDate);
                        this.TerminalsList_Data.Data.push(element);
                    });
                    if (this.LoaderEvent != undefined) {
                        this.LoaderEvent.target.complete();
                        if (this.TerminalsList_Data.TotalRecords == this.TerminalsList_Data.Data.length) {
                            this.LoaderEvent.target.disabled = true;
                        }
                    }
                }
                else {
                    this._HelperService.HideSpinner();
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    TerminalsList_RowSelected(ReferenceData) {
        if (ReferenceData.PaymentStatusCode == 'paymentstatus.pending') {
            var _OrderDetails =
            {
                OrderId: ReferenceData.OrderId,
                OrderKey: ReferenceData.OrderReference,
            };
            this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveOrder, _OrderDetails);
            this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.orderpaymentprocess);
        }
        else {
            var _OrderDetails =
            {
                OrderId: ReferenceData.OrderId,
                OrderKey: ReferenceData.OrderReference,
            };
            this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveOrder, _OrderDetails);
            this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.orderdetails);
        }


    }
    public LoaderEvent: any = undefined;
    NextLoad(event) {
        this.LoaderEvent = event;
        this.TerminalsList_Setup();
    }

    public SelectedAppStatus = 2;
    ToggleActivityStatus(value: any) {
        this.SelectedAppStatus = value;
        this.TerminalsList_Data =
            {
                SearchContent: "",
                TotalRecords: 0,
                Offset: -1,
                Limit: 10,
                Data: []
            };
        this.TerminalsList_Setup();
    }


    private delayTimer;
    doSearch(text) {
        clearTimeout(this.delayTimer);
        this.delayTimer = setTimeout(x => {
            this.TerminalsList_Data =
                {
                    SearchContent: text,
                    TotalRecords: 0,
                    Offset: -1,
                    Limit: 10,
                    Data: []
                };
            this.TerminalsList_Setup();
        }, 1000);
    }
}