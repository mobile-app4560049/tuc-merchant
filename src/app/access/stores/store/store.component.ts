import { Component } from '@angular/core';
import { NavController, MenuController, AlertController, ModalController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from '../../../service/helper.service';
import { OResponse, ODeviceInformation, OTerminalStatusCount } from '../../../service/object.service';
import { TransactionDetailsModal } from '../../modals/transactiondetails/transactiondetails.modal.component';

@Component({
    selector: 'app-store',
    templateUrl: 'store.component.html'
})
export class StorePage {
    public ActiveTab = 1;
    public DeviceInformation: ODeviceInformation;
    constructor(
        public _ModalController: ModalController,
        public _HelperService: HelperService,
        public _AlertController: AlertController,
    ) {
        var DeviceInformationStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
        if (DeviceInformationStorage != null) {
            this.DeviceInformation = DeviceInformationStorage;
        }
    }

    public StoreInfo =
        {
            ReferenceId: null,
            ReferenceKey: null,
            DisplayName: null,
            Address: null,
            Latitude: null,
            Longitude: null,
            IconUrl: null,
            OwnerId: null,
            OwnerKey: null,
            OwnerDisplayName: null,
            LastTransactionDate: null,
            CreateDate: null,
            StatusId: null,
            StatusCode: null,
            StatusName: null,
            Terminals: null,
            RewardPercentage: null,
        }

    ActiveTabChange(Item) {
        this.ActiveTab = Item;
        if (Item == 1) {
            this.TranList_Setup();
        }
        else {
            this.TerminalsList_Setup();
            this.GetTerminalStatusCount(this._HelperService.AccountOwner.AccountId, 108, this.StoreInfo.ReferenceKey, 109);
        }
    }

    ngOnInit() {
        this.StoreInfo = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference);
        this.TranList_Data =
        {
            SearchContent: "",
            TotalRecords: 0,
            Offset: -1,
            Limit: 10,
            Data: []
        };
        this.TerminalsList_Data =
        {
            SearchContent: "",
            TotalRecords: 0,
            Offset: -1,
            Limit: 10,
            Data: []
        };
        // this.TerminalsList_Setup();
        this.TranList_Setup();
    }

    public TranList_Data =
        {
            SearchContent: "",
            TotalRecords: 0,
            Offset: -1,
            Limit: 10,
            Data: []
        };
    TranList_Setup() {
        if (this.TranList_Data.Offset == -1) {
            this._HelperService.ShowSpinner();
            this.TranList_Data.Offset = 0;
        }
        var SCon = "";
        if (this.TranList_Data.SearchContent != undefined && this.TranList_Data.SearchContent != null && this.TranList_Data.SearchContent != '') {
            SCon = this._HelperService.GetSearchCondition(SCon, 'UserDisplayName', this._HelperService.AppConfig.DataType.Text, this.TranList_Data.SearchContent);
            SCon = this._HelperService.GetSearchCondition(SCon, 'UserMobileNumber', this._HelperService.AppConfig.DataType.Text, this.TranList_Data.SearchContent);
            SCon = this._HelperService.GetSearchCondition(SCon, 'ReferenceNumber', this._HelperService.AppConfig.DataType.Text, this.TranList_Data.SearchContent);
        }
        SCon = this._HelperService.GetSearchConditionStrict(SCon, 'ParentId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AccountOwner.AccountId, '==');
        SCon = this._HelperService.GetSearchConditionStrict(SCon, 'SubParentId', this._HelperService.AppConfig.DataType.Number, this.StoreInfo.ReferenceId, '==');
        var pData = {
            Task: 'getsaletransactions',
            TotalRecords: this.TranList_Data.TotalRecords,
            Offset: this.TranList_Data.Offset,
            Limit: this.TranList_Data.Limit,
            RefreshCount: true,
            SearchCondition: SCon,
            SortExpression: 'TransactionDate desc',
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.TUCTransCore, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideSpinner();
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this.TranList_Data.Offset = this.TranList_Data.Offset + this.TranList_Data.Limit;
                    this.TranList_Data.TotalRecords = _Response.Result.TotalRecords;
                    var TranList_Data = _Response.Result.Data;
                    TranList_Data.forEach(element => {
                        if (element.CardBrandName != undefined) {
                            element.CardBrandName = element.CardBrandName.toLowerCase().trim();
                        }
                        element.TypeName = element.TypeName.toLowerCase().trim();
                        element.TransactionDate = this._HelperService.GetDateTimeS(element.TransactionDate);
                        this.TranList_Data.Data.push(element);
                    });
                    if (this.LoaderEvent != undefined) {
                        this.LoaderEvent.target.complete();
                        if (this.TranList_Data.TotalRecords == this.TranList_Data.Data.length) {
                            this.LoaderEvent.target.disabled = true;
                        }
                    }
                }
                else {
                    this._HelperService.HideSpinner();
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    TranList_RowSelected(ReferenceData) {
        if (ReferenceData.PaymentStatusCode == 'paymentstatus.pending') {
            var _OrderDetails =
            {
                OrderId: ReferenceData.OrderId,
                OrderKey: ReferenceData.OrderReference,
            };
            this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveOrder, _OrderDetails);
            this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.orderpaymentprocess);
        }
        else {
            var _OrderDetails =
            {
                OrderId: ReferenceData.OrderId,
                OrderKey: ReferenceData.OrderReference,
            };
            this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveOrder, _OrderDetails);
            this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.orderdetails);
        }


    }
    public TranList_LoaderEvent: any = undefined;
    TranList_NextLoad(event) {
        this.TranList_LoaderEvent = event;
        this.TranList_Setup();
    }

    private TranList_delayTimer;
    TranList_Search(text) {
        clearTimeout(this.TranList_delayTimer);
        this.TranList_delayTimer = setTimeout(x => {
            this.TranList_Data =
            {
                SearchContent: text,
                TotalRecords: 0,
                Offset: -1,
                Limit: 10,
                Data: []
            };
            this.TranList_Setup();
        }, 1000);
    }

    async OpenAddModal(Item) {
        const modal = await this._ModalController.create({
            component: TransactionDetailsModal,
            componentProps: Item
        });
        return await modal.present();
    }

    public _TerminalStatusCount: OTerminalStatusCount =
        {

            Total: 0,
            Unused: 0,
            Active: 0,
            Idle: 0,
            Dead: 0,
        };

    GetTerminalStatusCount(UserAccountId, UserAccountTypeId, SubUserAccountId, SubUserAccountTypeId) {
        var Data = {
            Task: "getterminalstatuscount",
            UserAccountId: UserAccountId,
            UserAccountTypeId: UserAccountTypeId,
            SubUserAccountId: SubUserAccountId,
            SubUserAccountTypeId: SubUserAccountTypeId,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.TUCAnalytics, Data);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this._TerminalStatusCount = _Response.Result as OTerminalStatusCount;
                } else {
                    this._HelperService.NotifySimple(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }
        );
    }
    public TerminalsList_Data =
        {
            SearchContent: "",
            TotalRecords: 0,
            Offset: -1,
            Limit: 10,
            Data: []
        };
    TerminalsList_Setup() {
        if (this.TerminalsList_Data.Offset == -1) {
            this._HelperService.ShowSpinner();
            this.TerminalsList_Data.Offset = 0;
        }
        var SCon = "";
        if (this.TerminalsList_Data.SearchContent != undefined && this.TerminalsList_Data.SearchContent != null && this.TerminalsList_Data.SearchContent != '') {
            SCon = this._HelperService.GetSearchCondition(SCon, 'DisplayName', this._HelperService.AppConfig.DataType.Text, this.TerminalsList_Data.SearchContent);
            SCon = this._HelperService.GetSearchCondition(SCon, 'Address', this._HelperService.AppConfig.DataType.Text, this.TerminalsList_Data.SearchContent);
            SCon = this._HelperService.GetSearchCondition(SCon, 'AcquirerDisplayName', this._HelperService.AppConfig.DataType.Text, this.TerminalsList_Data.SearchContent);
            SCon = this._HelperService.GetSearchCondition(SCon, 'ProviderDisplayName', this._HelperService.AppConfig.DataType.Text, this.TerminalsList_Data.SearchContent);
        }
        SCon = this._HelperService.GetSearchConditionStrict(SCon, 'MerchantId', this._HelperService.AppConfig.DataType.Text, this._HelperService.AccountOwner.AccountId, '==');
        SCon = this._HelperService.GetSearchConditionStrict(SCon, 'StoreId', this._HelperService.AppConfig.DataType.Text, this.StoreInfo.ReferenceId, '==');
        // SCon = this._HelperService.GetSearchConditionStrict(SCon, 'StatusCode', this._HelperService.AppConfig.DataType.Text, "default.active", '==');
        // if (this.SelectedAppStatus != 0) {
        //     SCon = this._HelperService.GetSearchConditionStrict(SCon, 'ActivityStatusId', this._HelperService.AppConfig.DataType.Number, this.SelectedAppStatus, '=');
        // }
        var pData = {
            Task: 'getterminals',
            TotalRecords: this.TerminalsList_Data.TotalRecords,
            Offset: this.TerminalsList_Data.Offset,
            Limit: this.TerminalsList_Data.Limit,
            RefreshCount: true,
            SearchCondition: SCon,
            SortExpression: 'DisplayName asc',
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.TUCAccCore, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideSpinner();
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this.TerminalsList_Data.Offset = this.TerminalsList_Data.Offset + this.TerminalsList_Data.Limit;
                    this.TerminalsList_Data.TotalRecords = _Response.Result.TotalRecords;
                    var TerminalsList_Data = _Response.Result.Data;
                    TerminalsList_Data.forEach(element => {
                        element.CreateDate = this._HelperService.GetDateTimeS(element.CreateDate);
                        element.LastTransactionDate = this._HelperService.GetDateTimeS(element.LastTransactionDate);
                        this.TerminalsList_Data.Data.push(element);
                    });
                    if (this.LoaderEvent != undefined) {
                        this.LoaderEvent.target.complete();
                        if (this.TerminalsList_Data.TotalRecords == this.TerminalsList_Data.Data.length) {
                            this.LoaderEvent.target.disabled = true;
                        }
                    }
                }
                else {
                    this._HelperService.HideSpinner();
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    TerminalsList_RowSelected(ReferenceData) {
        if (ReferenceData.PaymentStatusCode == 'paymentstatus.pending') {
            var _OrderDetails =
            {
                OrderId: ReferenceData.OrderId,
                OrderKey: ReferenceData.OrderReference,
            };
            this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveOrder, _OrderDetails);
            this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.orderpaymentprocess);
        }
        else {
            var _OrderDetails =
            {
                OrderId: ReferenceData.OrderId,
                OrderKey: ReferenceData.OrderReference,
            };
            this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveOrder, _OrderDetails);
            this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.orderdetails);
        }


    }
    public LoaderEvent: any = undefined;
    NextLoad(event) {
        this.LoaderEvent = event;
        this.TerminalsList_Setup();
    }

    public SelectedAppStatus = 2;
    ToggleActivityStatus(value: any) {
        this.SelectedAppStatus = value;
        this.TerminalsList_Data =
        {
            SearchContent: "",
            TotalRecords: 0,
            Offset: -1,
            Limit: 10,
            Data: []
        };
        this.TerminalsList_Setup();
    }


    private delayTimer;
    doSearch(text) {
        clearTimeout(this.delayTimer);
        this.delayTimer = setTimeout(x => {
            this.TerminalsList_Data =
            {
                SearchContent: text,
                TotalRecords: 0,
                Offset: -1,
                Limit: 10,
                Data: []
            };
            this.TerminalsList_Setup();
        }, 1000);
    }
}