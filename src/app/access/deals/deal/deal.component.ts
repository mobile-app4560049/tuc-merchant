import { Component, ViewChild, Sanitizer } from '@angular/core';
import { NavController, MenuController, AlertController, ModalController, IonDatetime } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from '../../../service/helper.service';
declare let L;
import 'leaflet';
import { PlaceOrderModal } from '../../modals/placeorder/placeorder.modal.component';
import { DataService } from '../../../service/data.service';
import { DatePipe } from '@angular/common';
import { OResponse, OSalesSummary, ORewardsSummary, OTerminalStatusCount, } from '../../../service/object.service';
declare var moment: any;
import { DomSanitizer } from "@angular/platform-browser";
import { FCM } from '@ionic-native/fcm/ngx';
import { ScanMyCodeModal } from '../../modals/scanmycode/scanmycode.modal.component';
import { AddCustomerModal } from '../../modals/addcustomer/addcustomer.modal.component';
@Component({
    selector: 'app-deal',
    templateUrl: 'deal.component.html'
})
export class DealPage {
    @ViewChild("startTimePicker", { static: true }) _IonDatetime: IonDatetime;
    constructor(
        public _FireBase: FCM,
        private sanitizer: DomSanitizer,
        public _DatePipe: DatePipe,
        public _HelperService: HelperService,
        public _AlertController: AlertController,
        public _MenuController: MenuController,
        public _ModalController: ModalController,
        public _DataService: DataService,
    ) {
        var OOwnerId = this._HelperService.AccountInfo.UserAccount.AccountId;
        if (this._HelperService.AccountInfo.UserOwner != undefined && this._HelperService.AccountInfo.UserOwner != null && this._HelperService.AccountInfo.UserOwner.AccountId != 0) {
            OOwnerId = this._HelperService.AccountInfo.UserOwner.AccountId;
        }
        if (OOwnerId != undefined && OOwnerId != null && OOwnerId != 0) {
            // this._FireBase.subscribeToTopic('tucmerchant_' + this._HelperService.AccountOwner.AccountId).then(x => {
            // }).catch(x => {
            // });
        }

        this._MenuController.enable(true);
    }
    Type = 1;
    StartTime = null;
    StartTimeS = null;
    EndTime = null;
    EndTimeS = null;
    public MinDate = null;
    public MaxDate = null;
    UserAnalytics_DateChange(Type) {
        this.Type = Type;
        var SDate;
        if (Type == 1) {
            SDate =
            {
                start: moment().startOf('day'),
                end: moment().endOf('day'),
            }
        }
        else if (Type == 2) {
            SDate =
            {
                start: moment().subtract(1, 'days').startOf('day'),
                end: moment().subtract(1, 'days').endOf('day'),
            }
        }
        else if (Type == 3) {
            SDate =
            {
                start: moment().startOf('isoWeek'),
                end: moment().endOf('isoWeek'),
            }
        }
        else if (Type == 4) {
            SDate =
            {
                start: moment().startOf('month'),
                end: moment().endOf('month'),
            }
        }
        else if (Type == 5) {
            this._IonDatetime.open();
        }
        if (Type != 5) {
            this.StartTime = SDate.start;
            this.EndTime = SDate.end;
            this.LoadData();
        }

    }
    CustomDateChange() {
        this.StartTime = moment(this.SelectedItemDate).startOf('day');
        this.EndTime = moment(this.SelectedItemDate).endOf('day');
        this.LoadData();
    }
    ngAfterViewInit() {
        setTimeout(() => {
            this._MenuController.enable(true);
        }, 300);
    }

    ngOnInit() {
        this.MaxDate = this._HelperService.FormatDate(moment().startOf('day'), "YYYY-MM-DD");
        this.MinDate = this._HelperService.FormatDate(moment(this._HelperService.AccountInfo.UserAccount.CreateDate).startOf('day'), "YYYY-MM-DD");
        if (this.StartTime == undefined) {
            this.StartTime = moment().startOf('day');
            this.EndTime = moment().endOf('day');
        }
        this._HelperService.RefreshProfile();
        this._HelperService.RefreshLocation();
        this.LoadData();
    }


    public SelectedItemDate = null;
    public DStartTimeS = "";
    public DEndTimeS = "";
    public Items = [];
    LoadData() {
        this.DStartTimeS = this._HelperService.GetDateS(moment().startOf('day').subtract(15, 'day'));
        this.DEndTimeS = this._HelperService.GetDateS(moment().startOf('day').subtract(1, 'day'));
        this.StartTimeS = this._HelperService.GetDateS(this.StartTime);
        this.EndTimeS = this._HelperService.GetDateS(this.EndTime);
        // this._HelperService.GetSalesSummary(this._HelperService.AccountOwner.AccountId, 108, 0, 0, this.StartTime, this.EndTime);
        // this._HelperService.GetUserCounts(this._HelperService.AccountOwner.AccountId, 108, 0, 0, this.StartTime, this.EndTime);
        // this._HelperService.GetRewardsSummary(this._HelperService.AccountOwner.AccountId, 108, 0, 0, this.StartTime, this.EndTime);
        // this._HelperService.GetSalesHistory(this._HelperService.AccountOwner.AccountId, 108, 0, 0, moment().startOf('day').subtract(15, 'day'), moment().startOf('day').subtract(1, 'day'));
        // this._HelperService.GetStoreVisits(this.StartTime, this.EndTime);
        this.StoresList_Data =
        {
            Type: "running",
            SearchContent: "",
            TotalRecords: 0,
            Offset: -1,
            Limit: 10,
            Data: []
        };
        this.StoresList_Setup();
    }

    DealStatusType(Type) {
        this.StoresList_Data =
        {
            Type: Type,
            SearchContent: "",
            TotalRecords: 0,
            Offset: -1,
            Limit: 10,
            Data: []
        };
        this.StoresList_Setup();
    }
    public StoresList_Data =
        {
            Type: "running",
            SearchContent: "",
            TotalRecords: 0,
            Offset: -1,
            Limit: 10,
            Data: []
        };
    StoresList_Setup() {
        if (this.StoresList_Data.Offset == -1) {
            this._HelperService.ShowSpinner();
            this.StoresList_Data.Offset = 0;
        }
        var SCon = "";
        if (this.StoresList_Data.Type == "running") {

        }
        else if (this.StoresList_Data.Type == "upcoming") {
        }
        else if (this.StoresList_Data.Type == "paused") {
            SCon = this._HelperService.GetSearchCondition(SCon, 'StatusCode', this._HelperService.AppConfig.DataType.Text, "deal.paused");
        }
        else if (this.StoresList_Data.Type == "draft") {
            SCon = this._HelperService.GetSearchCondition(SCon, 'StatusCode', this._HelperService.AppConfig.DataType.Text, "deal.draft");
        }
        else if (this.StoresList_Data.Type == "pendingapproval") {
            SCon = this._HelperService.GetSearchCondition(SCon, 'StatusCode', this._HelperService.AppConfig.DataType.Text, "deal.approvalpending");
        }
        else if (this.StoresList_Data.Type == "expired") {
            SCon = this._HelperService.GetSearchCondition(SCon, 'StatusCode', this._HelperService.AppConfig.DataType.Text, "deal.expired");
        }
        else {

        }
        // if (this.StoresList_Data.SearchContent != undefined && this.StoresList_Data.SearchContent != null && this.StoresList_Data.SearchContent != '') {
        //     SCon = this._HelperService.GetSearchCondition(SCon, 'Title', this._HelperService.AppConfig.DataType.Text, this.StoresList_Data.SearchContent);
        //     SCon = this._HelperService.GetSearchCondition(SCon, 'CustomerDisplayName', this._HelperService.AppConfig.DataType.Text, this.StoresList_Data.SearchContent);
        //     SCon = this._HelperService.GetSearchCondition(SCon, 'CustomerMobileNumber', this._HelperService.AppConfig.DataType.Text, this.StoresList_Data.SearchContent);
        // }
        // SCon = this._HelperService.GetSearchConditionStrict(SCon, 'OwnerId', this._HelperService.AppConfig.DataType.Text, this._HelperService.AccountOwner.AccountId, '==');
        var pData = {
            Task: 'getdeals',
            TotalRecords: this.StoresList_Data.TotalRecords,
            Offset: this.StoresList_Data.Offset,
            Limit: this.StoresList_Data.Limit,
            RefreshCount: true,
            SearchCondition: SCon,
            SortExpression: 'StartDate asc',
            // AccountId: this._HelperService.AccountOwner.AccountId,
            // AccountKey: this._HelperService.AccountOwner.AccountKey,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Deals, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideSpinner();
                console.log(_Response);
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this.StoresList_Data.Offset = this.StoresList_Data.Offset + this.StoresList_Data.Limit;
                    this.StoresList_Data.TotalRecords = _Response.Result.TotalRecords;
                    var StoresList_Data = _Response.Result.Data;
                    StoresList_Data.forEach(element => {
                        element.StartDateS = this._HelperService.GetDateTimeS(element.StartDate);
                        element.EndDateS = this._HelperService.GetDateTimeS(element.EndDate);
                        element.CreateDate = this._HelperService.GetDateTimeS(element.CreateDate);
                        element.UseDate = this._HelperService.GetDateTimeS(element.UseDate);
                        element.LastTransactionDate = this._HelperService.GetDateTimeS(element.LastTransactionDate);
                        this.StoresList_Data.Data.push(element);
                    });
                    if (this.LoaderEvent != undefined) {
                        this.LoaderEvent.target.complete();
                        if (this.StoresList_Data.TotalRecords == this.StoresList_Data.Data.length) {
                            this.LoaderEvent.target.disabled = true;
                        }
                    }
                }
                else {
                    this._HelperService.HideSpinner();
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    StoresList_RowSelected(ReferenceData) {
        if (ReferenceData.PaymentStatusCode == 'paymentstatus.pending') {
            var _OrderDetails =
            {
                OrderId: ReferenceData.OrderId,
                OrderKey: ReferenceData.OrderReference,
            };
            this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveOrder, _OrderDetails);
            this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.orderpaymentprocess);
        }
        else {
            var _OrderDetails =
            {
                OrderId: ReferenceData.OrderId,
                OrderKey: ReferenceData.OrderReference,
            };
            this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveOrder, _OrderDetails);
            this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.orderdetails);
        }


    }
    public LoaderEvent: any = undefined;
    NextLoad(event) {
        this.LoaderEvent = event;
        this.StoresList_Setup();

    }
    NavScanDeal() {
        this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Deals.scandealcode);
    }
    NavStore(Item) {
        this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference, Item);
        this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Deals.dealdetails);
    }

    private delayTimer;
    doSearch(text) {
        clearTimeout(this.delayTimer);
        this.delayTimer = setTimeout(x => {
            this.StoresList_Data =
            {
                Type: 'running',
                SearchContent: text,
                TotalRecords: 0,
                Offset: -1,
                Limit: 10,
                Data: []
            };
            this.StoresList_Setup();
        }, 1000);
    }

}