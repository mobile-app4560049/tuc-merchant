import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { DealPage } from './deal.component';
import { NgCalendarModule } from 'ionic2-calendar';
import { ChartsModule } from 'ng2-charts';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        NgCalendarModule,
        ChartsModule,
        RouterModule.forChild([
            {
                path: '',
                component: DealPage
            }
        ])
    ],
    declarations: [DealPage]
})
export class DealPageModule { }
