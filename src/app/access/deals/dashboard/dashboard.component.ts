import { Component, ViewChild, Sanitizer } from '@angular/core';
import { NavController, MenuController, AlertController, ModalController, IonDatetime } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from '../../../service/helper.service';
declare let L;
import 'leaflet';
import { PlaceOrderModal } from '../../modals/placeorder/placeorder.modal.component';
import { DataService } from '../../../service/data.service';
import { DatePipe } from '@angular/common';
import { OResponse, ORewardsSummary, OTerminalStatusCount, } from '../../../service/object.service';
declare var moment: any;
import { DomSanitizer } from "@angular/platform-browser";
import { FCM } from '@ionic-native/fcm/ngx';
import { ScanMyCodeModal } from '../../modals/scanmycode/scanmycode.modal.component';
import { AddCustomerModal } from '../../modals/addcustomer/addcustomer.modal.component';
@Component({
    selector: 'app-dashboard',
    templateUrl: 'dashboard.component.html'
})
export class DashboardPage {

    slideOpts = {
        initialSlide: 1,
        speed: 400,
        slidesPerView: 1.6,
        spaceBetween: 32
    }

    PageType = 1;
    constructor(
        public _FireBase: FCM,
        private sanitizer: DomSanitizer,
        public _DatePipe: DatePipe,
        public _HelperService: HelperService,
        public _AlertController: AlertController,
        public _MenuController: MenuController,
        public _ModalController: ModalController,
        public _DataService: DataService,
    ) {
        var OOwnerId = this._HelperService.AccountInfo.UserAccount.AccountId;
        if (this._HelperService.AccountInfo.UserOwner != undefined && this._HelperService.AccountInfo.UserOwner != null && this._HelperService.AccountInfo.UserOwner.AccountId != 0) {
            OOwnerId = this._HelperService.AccountInfo.UserOwner.AccountId;
        }
        this._MenuController.enable(true);
    }

    PageTypeChange(PageType) {
        this.PageType = PageType;
        this.LoadTab();
    }
    LoadTab() {
        if (this.PageType == 1) {
            this.LoadOverview();
        }
        if (this.PageType == 2) {
            this.Reward_Load();
        }
        if (this.PageType == 3) {
            this.Redeem_Load();
        }
        if (this.PageType == 4) {
            this.PendingReward_Load();
        }
    }
    ngOnInit() {
        this._DateConfig.MaxDate = this._HelperService.FormatDate(moment().startOf('day'), "YYYY-MM-DD");
        this._DateConfig.MinDate = this._HelperService.FormatDate(moment(this._HelperService.AccountInfo.UserAccount.CreateDate).startOf('day'), "YYYY-MM-DD");
        if (this._DateConfig.StartTime == undefined) {
            this._DateConfig.StartTime = moment().startOf('day');
            this._DateConfig.EndTime = moment().endOf('day');
        }
        this.LoadData();
    }




    _DateConfig =
        {
            TypeName: 'Today',
            Type: 4,
            MinDate: null,
            MaxDate: null,
            StartTime: null,
            EndTime: null,
            StartTimeS: null,

            EndTimeS: null,
            SelectedStartDate: null,
            SelectedEndDate: null,
        }
    DateManager_Open() {
        this._HelperService.ShowTucDateModal();
    }
    DateManager_Confirm() {
        this._HelperService.HideTucDateModal();
        this.LoadTab();
    }
    DateManager_StartDateChange() {
        this._DateConfig.StartTime = moment(this._DateConfig.SelectedStartDate).startOf('day');
        this._DateConfig.SelectedEndDate = this._DateConfig.SelectedStartDate;
    }
    DateManager_EndDateChange() {
        this._DateConfig.EndTime = moment(this._DateConfig.SelectedEndDate).endOf('day');
    }
    DateManager_RangeClick(Type) {
        this._DateConfig.Type = Type;
        var SDate;
        if (Type == 1) {
            this._DateConfig.TypeName = "Today";
            SDate =
            {
                start: moment().startOf('day'),
                end: moment().endOf('day'),
            }
        }
        else if (Type == 2) {
            this._DateConfig.TypeName = "Yesterday";
            SDate =
            {
                start: moment().subtract(1, 'days').startOf('day'),
                end: moment().subtract(1, 'days').endOf('day'),
            }
        }
        else if (Type == 3) {
            this._DateConfig.TypeName = "Last 7 Days";
            SDate =
            {
                start: moment().subtract(6, 'days').startOf('day'),
                end: moment().endOf('days'),
            }
        }
        else if (Type == 4) {
            this._DateConfig.TypeName = "Last 15 Days";
            SDate =
            {
                start: moment().subtract(15, 'days').startOf('day'),
                end: moment().endOf('days'),
            }
        }
        else if (Type == 5) {
            this._DateConfig.TypeName = "Last 30 Days";
            SDate =
            {
                start: moment().subtract(30, 'days').startOf('day'),
                end: moment().endOf('days'),
            }
        }
        else if (Type == 6) {
            this._DateConfig.TypeName = "This Week";
            SDate =
            {
                start: moment().startOf('isoWeek').startOf('day'),
                end: moment().endOf('isoWeek').endOf('day'),
            }
        }
        else if (Type == 7) {
            this._DateConfig.TypeName = "Last Week";
            SDate =
            {
                start: moment().subtract(1, 'isoWeek').startOf('isoWeek').startOf('day'),
                end: moment().subtract(1, 'isoWeek').endOf('isoWeek').endOf('day'),
            }
        }
        else if (Type == 8) {
            this._DateConfig.TypeName = "This Month";
            SDate =
            {
                start: moment().startOf('month'),
                end: moment().endOf('month'),
            }
        }
        else if (Type == 9) {
            this._DateConfig.TypeName = "Last Month";
            SDate =
            {
                start: moment().subtract(1, 'month').startOf('month'),
                end: moment().subtract(1, 'month').endOf('month'),
            }
        }
        else if (Type == 10) {
            this._DateConfig.TypeName = "Last 3 Months";
            SDate =
            {
                start: moment().subtract(2, 'month').startOf('month'),
                end: moment().endOf('month'),
            }
        }
        else if (Type == 11) {
            this._DateConfig.TypeName = "Last 6 Months";
            SDate =
            {
                start: moment().subtract(5, 'month').startOf('month'),
                end: moment().endOf('month'),
            }
        }
        this._DateConfig.StartTime = SDate.start;
        this._DateConfig.EndTime = SDate.end;
        this.DateManager_Confirm();
    }
    LoadData() {
        this._DateConfig.StartTimeS = this._HelperService.GetDateS(this._DateConfig.StartTime);
        this._DateConfig.EndTimeS = this._HelperService.GetDateS(this._DateConfig.EndTime);
        this.LoadTab();
    }

    //#region  Rewards
    DealStatusType(Type) {
        this.Reward_TranList_Data =
        {
            Type: Type,
            SearchContent: "",
            TotalRecords: 0,
            Offset: -1,
            Limit: 10,
            Data: []
        };
        this.Reward_TranList_Setup();
    }

    Reward_Load() {
        this.Reward_TranList_Data =
        {
            Type: "running",
            SearchContent: "",
            TotalRecords: 0,
            Offset: -1,
            Limit: 10,
            Data: []
        };
        this.Reward_TranList_Setup();
    }
    public Reward_TranList_Data =
        {
            Type: "running",
            SearchContent: "",
            TotalRecords: 0,
            Offset: -1,
            Limit: 10,
            Data: []
        };
    public Reward_TranList_Data_Overview =
        {
            Running: 0,
            Upcoming: 0,
            Total: 0,
            Draft: 0,
            ApprovalPending: 0,
            Approved: 0,
            Published: 0,
            Paused: 0,
            Expired: 0,
            Rejected: 0,
        };
    private Reward_TranList_delayTimer;
    public Reward_TranList_LoaderEvent: any = undefined;
    Reward_TranList_Setup() {
        this.Reward_TranList_Setup_Overview();
        if (this.Reward_TranList_Data.Offset == -1) {
            this._HelperService.ShowSpinner();
            this.Reward_TranList_Data.Offset = 0;
        }
        var SCon = "";
        if (this.Reward_TranList_Data.SearchContent != undefined && this.Reward_TranList_Data.SearchContent != null && this.Reward_TranList_Data.SearchContent != '') {
            SCon = this._HelperService.GetSearchCondition(SCon, 'Title', this._HelperService.AppConfig.DataType.Text, this.Reward_TranList_Data.SearchContent);
        }
        if (this.Reward_TranList_Data.Type == "running") //  approvalpending
        {
        }
        if (this.Reward_TranList_Data.Type == "upcoming") //  approvalpending
        {
        }
        else if (this.Reward_TranList_Data.Type == "draft") {
            SCon = this._HelperService.GetSearchConditionStrict(SCon, "StatusCode", 'number', 'deal.draft', "=");
        }
        else if (this.Reward_TranList_Data.Type == "approvalpending") {
            SCon = this._HelperService.GetSearchConditionStrict(SCon, "StatusCode", 'number', 'deal.approvalpending', "=");
        }
        else if (this.Reward_TranList_Data.Type == "approved") {
            SCon = this._HelperService.GetSearchConditionStrict(SCon, "StatusCode", 'number', 'deal.approved', "=");
        }
        else if (this.Reward_TranList_Data.Type == "published") {
            SCon = this._HelperService.GetSearchConditionStrict(SCon, "StatusCode", 'number', 'deal.approved', "=");
        }
        else if (this.Reward_TranList_Data.Type == "paused") {
            SCon = this._HelperService.GetSearchConditionStrict(SCon, "StatusCode", 'number', 'deal.paused', "=");
        }
        else if (this.Reward_TranList_Data.Type == "expired") {
            SCon = this._HelperService.GetSearchConditionStrict(SCon, "StatusCode", 'number', 'deal.expired', "=");
        }
        else if (this.Reward_TranList_Data.Type == "expired") {
            SCon = this._HelperService.GetSearchConditionStrict(SCon, "StatusCode", 'number', 'deal.expired', "=");
        }
        else {
            // SCon = this._HelperService.GetDateCondition(SCon, 'CreateDate', this._DateConfig.StartTime, this._DateConfig.EndTime);
        }
        SCon = this._HelperService.GetSearchConditionStrict(SCon, "AccountId", 'number', this._HelperService.AccountOwner.AccountId, "=");
        var pData = {
            Task: 'getdeals',
            TotalRecords: this.Reward_TranList_Data.TotalRecords,
            Offset: this.Reward_TranList_Data.Offset,
            Limit: this.Reward_TranList_Data.Limit,
            RefreshCount: true,
            SearchCondition: SCon,
            SortExpression: 'StartDate desc',
            AccountId: this._HelperService.AccountOwner.AccountId,
            AccountKey: this._HelperService.AccountOwner.AccountKey,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.DealManager, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideSpinner();
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this.Reward_TranList_Data.Offset = this.Reward_TranList_Data.Offset + this.Reward_TranList_Data.Limit;
                    this.Reward_TranList_Data.TotalRecords = _Response.Result.TotalRecords;
                    var Reward_TranList_Data = _Response.Result.Data;
                    Reward_TranList_Data.forEach(element => {
                        element.TypeName = element.TypeName.toLowerCase().trim();
                        element.TransactionDate = this._HelperService.GetDateTimeS(element.TransactionDate);
                        element.StartDateS = this._HelperService.GetDateTimeS(element.StartDate);
                        element.EndDateS = this._HelperService.GetDateTimeS(element.EndDate);
                        this.Reward_TranList_Data.Data.push(element);
                    });
                    if (this.Reward_TranList_LoaderEvent != undefined) {
                        this.Reward_TranList_LoaderEvent.target.complete();
                        if (this.Reward_TranList_Data.TotalRecords == this.Reward_TranList_Data.Data.length) {
                            this.Reward_TranList_LoaderEvent.target.disabled = true;
                        }
                    }
                }
                else {
                    this._HelperService.HideSpinner();
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    Reward_TranList_Setup_Overview() {
        var pData = {
            Task: 'getdealsoverview',
            AccountKey: this._HelperService.AccountOwner.AccountKey,
            AccountId: this._HelperService.AccountOwner.AccountId,
            StartDate: this._DateConfig.StartTime,
            EndDate: this._DateConfig.EndTime,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.DealManager, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideSpinner();
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this.Reward_TranList_Data_Overview = _Response.Result;
                    // this.Reward_TranList_Data.Offset = this.Reward_TranList_Data.Offset + this.Reward_TranList_Data.Limit;
                    // this.Reward_TranList_Data.TotalRecords = _Response.Result.TotalRecords;
                    // var Reward_TranList_Data = _Response.Result.Data;
                    // Reward_TranList_Data.forEach(element => {
                    //     if (element.CardBrandName != undefined) {
                    //         element.CardBrandName = element.CardBrandName.toLowerCase().trim();
                    //     }
                    //     element.TypeName = element.TypeName.toLowerCase().trim();
                    //     element.TransactionDate = this._HelperService.GetDateTimeS(element.TransactionDate);
                    //     this.Reward_TranList_Data.Data.push(element);
                    // });
                    // if (this.Reward_TranList_LoaderEvent != undefined) {
                    //     this.Reward_TranList_LoaderEvent.target.complete();
                    //     if (this.Reward_TranList_Data.TotalRecords == this.Reward_TranList_Data.Data.length) {
                    //         this.Reward_TranList_LoaderEvent.target.disabled = true;
                    //     }
                    // }
                }
                else {
                    this._HelperService.HideSpinner();
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    Reward_TranList_RowSelected(ReferenceData) {
        if (ReferenceData.PaymentStatusCode == 'paymentstatus.pending') {
            var _OrderDetails =
            {
                OrderId: ReferenceData.OrderId,
                OrderKey: ReferenceData.OrderReference,
            };
            this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveOrder, _OrderDetails);
            this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.orderpaymentprocess);
        }
        else {
            var _OrderDetails =
            {
                OrderId: ReferenceData.OrderId,
                OrderKey: ReferenceData.OrderReference,
            };
            this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveOrder, _OrderDetails);
            this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.orderdetails);
        }


    }
    Reward_TranList_NextLoad(event) {
        this.Reward_TranList_LoaderEvent = event;
        this.Reward_TranList_Setup();

    }
    Reward_TranList_Search(text) {
        clearTimeout(this.Reward_TranList_delayTimer);
        this.Reward_TranList_delayTimer = setTimeout(x => {
            this.Reward_TranList_Data =
            {
                Type: "running",
                SearchContent: text,
                TotalRecords: 0,
                Offset: -1,
                Limit: 10,
                Data: []
            };
            this.Reward_TranList_Setup();
        }, 1000);
    }
    async Reward_TranList_OpenAddModal(Item) {
        Item.MType = "reward";
        // const modal = await this._ModalController.create({
        //     component: ModalRewardReceipt,
        //     componentProps: Item
        // });
        // return await modal.present();
    }
    //#endregion

    //#region Redeem
    Redeem_Load() {
        this.Redeem_TranList_Data =
        {
            Type: "all",
            SearchContent: "",
            TotalRecords: 0,
            Offset: -1,
            Limit: 10,
            Data: []
        };
        this.Redeem_TranList_Setup();
    }
    DealPurchaseTypeChange(Type) {
        this.Redeem_TranList_Data =
        {
            Type: Type,
            SearchContent: "",
            TotalRecords: 0,
            Offset: -1,
            Limit: 10,
            Data: []
        };
        this.Redeem_TranList_Setup();
    }
    public Redeem_TranList_Data =
        {
            Type: "all",
            SearchContent: "",
            TotalRecords: 0,
            Offset: -1,
            Limit: 10,
            Data: []
        };
    public Redeem_TranList_Data_Overview =
        {
            Total: 0,
            TotalAmount: 0,
            Customer: 0,
            Unused: 0,
            Used: 0,
            UsedAmount: 0,
            Expired: 0,
        };
    private Redeem_TranList_delayTimer;
    public Redeem_TranList_LoaderEvent: any = undefined;
    Redeem_TranList_Setup() {
        this.Redeem_TranList_Setup_Overview();
        if (this.Redeem_TranList_Data.Offset == -1) {
            this._HelperService.ShowSpinner();
            this.Redeem_TranList_Data.Offset = 0;
        }
        var SCon = "";
        if (this.Redeem_TranList_Data.SearchContent != undefined && this.Redeem_TranList_Data.SearchContent != null && this.Redeem_TranList_Data.SearchContent != '') {
            SCon = this._HelperService.GetSearchCondition(SCon, 'UserDisplayName', this._HelperService.AppConfig.DataType.Text, this.Redeem_TranList_Data.SearchContent);
            SCon = this._HelperService.GetSearchCondition(SCon, 'UserMobileNumber', this._HelperService.AppConfig.DataType.Text, this.Redeem_TranList_Data.SearchContent);
            SCon = this._HelperService.GetSearchCondition(SCon, 'CashierDisplayName', this._HelperService.AppConfig.DataType.Text, this.Redeem_TranList_Data.SearchContent);
            SCon = this._HelperService.GetSearchCondition(SCon, 'CashierCode', this._HelperService.AppConfig.DataType.Text, this.Redeem_TranList_Data.SearchContent);
            SCon = this._HelperService.GetSearchCondition(SCon, 'ReferenceNumber', this._HelperService.AppConfig.DataType.Text, this.Redeem_TranList_Data.SearchContent);
        }
        if (this.Redeem_TranList_Data.Type != "all") {
            SCon = this._HelperService.GetSearchConditionStrict(SCon, 'StatusCode', this._HelperService.AppConfig.DataType.Text, this.Redeem_TranList_Data.Type, '=');
        }
        // SCon = this._HelperService.GetDateCondition(SCon, 'TransactionDate', this._DateConfig.StartTime, this._DateConfig.EndTime);
        var pData = {
            Task: 'getpurchasehistory',
            TotalRecords: this.Redeem_TranList_Data.TotalRecords,
            Offset: this.Redeem_TranList_Data.Offset,
            Limit: this.Redeem_TranList_Data.Limit,
            RefreshCount: true,
            SearchCondition: SCon,
            SortExpression: 'CreateDate desc',
            AccountId: this._HelperService.AccountOwner.AccountId,
            AccountKey: this._HelperService.AccountOwner.AccountKey,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.DealManager, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideSpinner();
                console.log(_Response);
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this.Redeem_TranList_Data.Offset = this.Redeem_TranList_Data.Offset + this.Redeem_TranList_Data.Limit;
                    this.Redeem_TranList_Data.TotalRecords = _Response.Result.TotalRecords;
                    var Redeem_TranList_Data = _Response.Result.Data;
                    Redeem_TranList_Data.forEach(element => {
                        element.StartDateS = this._HelperService.GetDateS(element.StartDate);
                        element.EndDateS = this._HelperService.GetDateTimeS(element.EndDate);
                        this.Redeem_TranList_Data.Data.push(element);
                    });
                    if (this.Redeem_TranList_LoaderEvent != undefined) {
                        this.Redeem_TranList_LoaderEvent.target.complete();
                        if (this.Redeem_TranList_Data.TotalRecords == this.Redeem_TranList_Data.Data.length) {
                            this.Redeem_TranList_LoaderEvent.target.disabled = true;
                        }
                    }
                }
                else {
                    this._HelperService.HideSpinner();
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    Redeem_TranList_Setup_Overview() {
        var SCon = "";
        if (this.Redeem_TranList_Data.SearchContent != undefined && this.Redeem_TranList_Data.SearchContent != null && this.Redeem_TranList_Data.SearchContent != '') {
            SCon = this._HelperService.GetSearchCondition(SCon, 'UserDisplayName', this._HelperService.AppConfig.DataType.Text, this.Redeem_TranList_Data.SearchContent);
            SCon = this._HelperService.GetSearchCondition(SCon, 'UserMobileNumber', this._HelperService.AppConfig.DataType.Text, this.Redeem_TranList_Data.SearchContent);
            SCon = this._HelperService.GetSearchCondition(SCon, 'CashierDisplayName', this._HelperService.AppConfig.DataType.Text, this.Redeem_TranList_Data.SearchContent);
            SCon = this._HelperService.GetSearchCondition(SCon, 'CashierCode', this._HelperService.AppConfig.DataType.Text, this.Redeem_TranList_Data.SearchContent);
            SCon = this._HelperService.GetSearchCondition(SCon, 'ReferenceNumber', this._HelperService.AppConfig.DataType.Text, this.Redeem_TranList_Data.SearchContent);
        }
        SCon = this._HelperService.GetDateCondition(SCon, 'TransactionDate', this._DateConfig.StartTime, this._DateConfig.EndTime);
        var pData = {
            Task: 'getdealspurchaseoverview',
            TotalRecords: this.Redeem_TranList_Data.TotalRecords,
            Offset: this.Redeem_TranList_Data.Offset,
            Limit: this.Redeem_TranList_Data.Limit,
            RefreshCount: true,
            SearchCondition: SCon,
            SortExpression: 'TransactionDate desc',
            AccountId: this._HelperService.AccountOwner.AccountId,
            AccountKey: this._HelperService.AccountOwner.AccountKey,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.DealManager, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideSpinner();
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this.Redeem_TranList_Data_Overview = _Response.Result;
                }
                else {
                    this._HelperService.HideSpinner();
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    Redeem_TranList_RowSelected(ReferenceData) {
        if (ReferenceData.PaymentStatusCode == 'paymentstatus.pending') {
            var _OrderDetails =
            {
                OrderId: ReferenceData.OrderId,
                OrderKey: ReferenceData.OrderReference,
            };
            this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveOrder, _OrderDetails);
            this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.orderpaymentprocess);
        }
        else {
            var _OrderDetails =
            {
                OrderId: ReferenceData.OrderId,
                OrderKey: ReferenceData.OrderReference,
            };
            this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveOrder, _OrderDetails);
            this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.orderdetails);
        }


    }
    Redeem_TranList_NextLoad(event) {
        this.Redeem_TranList_LoaderEvent = event;
        this.Redeem_TranList_Setup();

    }
    Redeem_TranList_Search(text) {
        clearTimeout(this.Redeem_TranList_delayTimer);
        this.Redeem_TranList_delayTimer = setTimeout(x => {
            this.Redeem_TranList_Data =
            {
                Type: "all",
                SearchContent: text,
                TotalRecords: 0,
                Offset: -1,
                Limit: 10,
                Data: []
            };
            this.Redeem_TranList_Setup();
        }, 1000);
    }
    async Redeem_TranList_OpenAddModal(Item) {
        Item.MType = "redeem";
        // const modal = await this._ModalController.create({
        //     component: ModalRewardReceipt,
        //     componentProps: Item
        // });
        // return await modal.present();
    }
    //#endregion

    //#region Pending Rewards
    PendingReward_Load() {
        this.PendingReward_TranList_Data =
        {
            SearchContent: "",
            TotalRecords: 0,
            Offset: -1,
            Limit: 10,
            Data: []
        };
        this.PendingReward_TranList_Setup();
    }
    public PendingReward_TranList_Data =
        {
            SearchContent: "",
            TotalRecords: 0,
            Offset: -1,
            Limit: 10,
            Data: []
        };
    public PendingReward_TranList_Data_Overview =
        {
            Transactions: 0,
            InvoiceAmount: 0,
            RewardAmount: 0,
            UserAmount: 0,
        };
    private PendingReward_TranList_delayTimer;
    public PendingReward_TranList_LoaderEvent: any = undefined;
    PendingReward_TranList_Setup() {
        this.PendingReward_TranList_Setup_Overview();
        if (this.PendingReward_TranList_Data.Offset == -1) {
            this._HelperService.ShowSpinner();
            this.PendingReward_TranList_Data.Offset = 0;
        }
        var SCon = "";
        if (this.PendingReward_TranList_Data.SearchContent != undefined && this.PendingReward_TranList_Data.SearchContent != null && this.PendingReward_TranList_Data.SearchContent != '') {
            SCon = this._HelperService.GetSearchCondition(SCon, 'UserDisplayName', this._HelperService.AppConfig.DataType.Text, this.PendingReward_TranList_Data.SearchContent);
            SCon = this._HelperService.GetSearchCondition(SCon, 'UserMobileNumber', this._HelperService.AppConfig.DataType.Text, this.PendingReward_TranList_Data.SearchContent);
            SCon = this._HelperService.GetSearchCondition(SCon, 'CashierDisplayName', this._HelperService.AppConfig.DataType.Text, this.PendingReward_TranList_Data.SearchContent);
            SCon = this._HelperService.GetSearchCondition(SCon, 'CashierCode', this._HelperService.AppConfig.DataType.Text, this.PendingReward_TranList_Data.SearchContent);
            SCon = this._HelperService.GetSearchCondition(SCon, 'ReferenceNumber', this._HelperService.AppConfig.DataType.Text, this.PendingReward_TranList_Data.SearchContent);
        }
        SCon = this._HelperService.GetDateCondition(SCon, 'TransactionDate', this._DateConfig.StartTime, this._DateConfig.EndTime);
        var pData = {
            Task: 'getpendingrewardtransactions',
            TotalRecords: this.PendingReward_TranList_Data.TotalRecords,
            Offset: this.PendingReward_TranList_Data.Offset,
            Limit: this.PendingReward_TranList_Data.Limit,
            RefreshCount: true,
            SearchCondition: SCon,
            SortExpression: 'TransactionDate desc',
            AccountId: this._HelperService.AccountOwner.AccountId,
            AccountKey: this._HelperService.AccountOwner.AccountKey,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Transactions, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideSpinner();
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this.PendingReward_TranList_Data.Offset = this.PendingReward_TranList_Data.Offset + this.PendingReward_TranList_Data.Limit;
                    this.PendingReward_TranList_Data.TotalRecords = _Response.Result.TotalRecords;
                    var PendingReward_TranList_Data = _Response.Result.Data;
                    PendingReward_TranList_Data.forEach(element => {
                        if (element.CardBrandName != undefined) {
                            element.CardBrandName = element.CardBrandName.toLowerCase().trim();
                        }
                        element.TypeName = element.TypeName.toLowerCase().trim();
                        element.TransactionDate = this._HelperService.GetDateTimeS(element.TransactionDate);
                        this.PendingReward_TranList_Data.Data.push(element);
                    });
                    if (this.PendingReward_TranList_LoaderEvent != undefined) {
                        this.PendingReward_TranList_LoaderEvent.target.complete();
                        if (this.PendingReward_TranList_Data.TotalRecords == this.PendingReward_TranList_Data.Data.length) {
                            this.PendingReward_TranList_LoaderEvent.target.disabled = true;
                        }
                    }
                }
                else {
                    this._HelperService.HideSpinner();
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    PendingReward_TranList_Setup_Overview() {
        var SCon = "";
        if (this.PendingReward_TranList_Data.SearchContent != undefined && this.PendingReward_TranList_Data.SearchContent != null && this.PendingReward_TranList_Data.SearchContent != '') {
            SCon = this._HelperService.GetSearchCondition(SCon, 'UserDisplayName', this._HelperService.AppConfig.DataType.Text, this.PendingReward_TranList_Data.SearchContent);
            SCon = this._HelperService.GetSearchCondition(SCon, 'UserMobileNumber', this._HelperService.AppConfig.DataType.Text, this.PendingReward_TranList_Data.SearchContent);
            SCon = this._HelperService.GetSearchCondition(SCon, 'CashierDisplayName', this._HelperService.AppConfig.DataType.Text, this.PendingReward_TranList_Data.SearchContent);
            SCon = this._HelperService.GetSearchCondition(SCon, 'CashierCode', this._HelperService.AppConfig.DataType.Text, this.PendingReward_TranList_Data.SearchContent);
            SCon = this._HelperService.GetSearchCondition(SCon, 'ReferenceNumber', this._HelperService.AppConfig.DataType.Text, this.PendingReward_TranList_Data.SearchContent);
        }
        SCon = this._HelperService.GetDateCondition(SCon, 'TransactionDate', this._DateConfig.StartTime, this._DateConfig.EndTime);
        var pData = {
            Task: 'getpendingrewardtransactionsoverview',
            TotalRecords: this.PendingReward_TranList_Data.TotalRecords,
            Offset: this.PendingReward_TranList_Data.Offset,
            Limit: this.PendingReward_TranList_Data.Limit,
            RefreshCount: true,
            SearchCondition: SCon,
            SortExpression: 'TransactionDate desc',
            AccountId: this._HelperService.AccountOwner.AccountId,
            AccountKey: this._HelperService.AccountOwner.AccountKey,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Transactions, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideSpinner();
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this.PendingReward_TranList_Data_Overview = _Response.Result;
                    // this.PendingReward_TranList_Data.Offset = this.PendingReward_TranList_Data.Offset + this.PendingReward_TranList_Data.Limit;
                    // this.PendingReward_TranList_Data.TotalRecords = _Response.Result.TotalRecords;
                    // var PendingReward_TranList_Data = _Response.Result.Data;
                    // PendingReward_TranList_Data.forEach(element => {
                    //     if (element.CardBrandName != undefined) {
                    //         element.CardBrandName = element.CardBrandName.toLowerCase().trim();
                    //     }
                    //     element.TypeName = element.TypeName.toLowerCase().trim();
                    //     element.TransactionDate = this._HelperService.GetDateTimeS(element.TransactionDate);
                    //     this.PendingReward_TranList_Data.Data.push(element);
                    // });
                    // if (this.PendingReward_TranList_LoaderEvent != undefined) {
                    //     this.PendingReward_TranList_LoaderEvent.target.complete();
                    //     if (this.PendingReward_TranList_Data.TotalRecords == this.PendingReward_TranList_Data.Data.length) {
                    //         this.PendingReward_TranList_LoaderEvent.target.disabled = true;
                    //     }
                    // }
                }
                else {
                    this._HelperService.HideSpinner();
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    PendingReward_TranList_RowSelected(ReferenceData) {
        if (ReferenceData.PaymentStatusCode == 'paymentstatus.pending') {
            var _OrderDetails =
            {
                OrderId: ReferenceData.OrderId,
                OrderKey: ReferenceData.OrderReference,
            };
            this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveOrder, _OrderDetails);
            this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.orderpaymentprocess);
        }
        else {
            var _OrderDetails =
            {
                OrderId: ReferenceData.OrderId,
                OrderKey: ReferenceData.OrderReference,
            };
            this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveOrder, _OrderDetails);
            this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.orderdetails);
        }


    }
    PendingReward_TranList_NextLoad(event) {
        this.PendingReward_TranList_LoaderEvent = event;
        this.PendingReward_TranList_Setup();

    }
    PendingReward_TranList_Search(text) {
        clearTimeout(this.PendingReward_TranList_delayTimer);
        this.PendingReward_TranList_delayTimer = setTimeout(x => {
            this.PendingReward_TranList_Data =
            {
                SearchContent: text,
                TotalRecords: 0,
                Offset: -1,
                Limit: 10,
                Data: []
            };
            this.PendingReward_TranList_Setup();
        }, 1000);
    }
    async PendingReward_TranList_OpenAddModal(Item) {
        Item.MType = "reward";
        // const modal = await this._ModalController.create({
        //     component: ModalRewardReceipt,
        //     componentProps: Item
        // });
        // return await modal.present();
    }
    //#endregion

    //#region 
    LoadOverview() {
        this.GetDealsOverview();
        this.GetSalesHistory();
    }
    _DealStatusOverview =
        {
            Running: 0,
            Upcoming: 0,
            Total: 0,
            Draft: 0,
            ApprovalPending: 0,
            Approved: 0,
            Published: 0,
            Paused: 0,
            Expired: 0,
            Rejected: 0,
        };
    public GetDealsOverview() {
        var pData = {
            Task: 'getdealsoverview',
            AccountKey: this._HelperService.AccountOwner.AccountKey,
            AccountId: this._HelperService.AccountOwner.AccountId,
            StartDate: this._DateConfig.StartTime,
            EndDate: this._DateConfig.EndTime,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.DealManager, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this._DealStatusOverview = _Response.Result;
                } else {
                    this._HelperService.IsFormProcessing = false;
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }
        );
    }

    _SalesHistory =
        {
            TotalDeals: 0,
            TotalDealAmount: 0,

            Total: 0,
            Amount: 0,

            Unused: 0,
            UnusedAmount: 0,

            Used: 0,
            UsedAmount: 0,

            Expired: 0,
            ExpiredAmount: 0,

            Customer: 0,

            Data: [],
        }
    public _ODealSalesHistory =
        {
            Labels: [],
            SaleColors: [],
            SaleDataSet: [],
            SaleCustomersColors: [],
            SaleCustomersDataSet: [],

            RedeemColors: [],
            RedeemDataSet: [],
        }
    public GetSalesHistory() {
        var pData = {
            Task: 'getdealhistory',
            AccountKey: this._HelperService.AccountOwner.AccountKey,
            AccountId: this._HelperService.AccountOwner.AccountId,
            StartDate: this._DateConfig.StartTime,
            EndDate: this._DateConfig.EndTime,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.DealManager, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    var _Data = _Response.Result;
                    this._SalesHistory.Data = _Data;
                    _Data.forEach(data => {
                        this._SalesHistory.TotalDeals += data.TotalDeals;
                        this._SalesHistory.TotalDealAmount += data.TotalDealAmount;

                        this._SalesHistory.Total += data.Total;
                        this._SalesHistory.Amount += data.Amount;

                        this._SalesHistory.Unused += data.Unused;
                        this._SalesHistory.UnusedAmount += data.UnusedAmount;

                        this._SalesHistory.Used += data.Used;
                        this._SalesHistory.UsedAmount += data.UsedAmount;

                        this._SalesHistory.Expired += data.Expired;
                        this._SalesHistory.ExpiredAmount += data.ExpiredAmount;

                    });


                    this._ODealSalesHistory.Labels = [];
                    var ResponseData = _Response.Result;
                    var TSaleColor = [{
                        backgroundColor: [],
                    }];
                    var TRedeemedColor = [{
                        backgroundColor: [],
                    }];
                    var TSaleDataSet = [{
                        label: 'Sale',
                        fill: false,
                        borderColor: "#303f9fc4",
                        borderDash: [6, 6],
                        backgroundColor: "#303f9fc4",
                        pointBackgroundColor: "#303f9fc4",
                        pointBorderColor: "#303f9fc4",
                        pointHoverBackgroundColor: "#303f9fc4",
                        pointHoverBorderColor: "#303f9fc4",
                        data: []
                    }];
                    var TRedeemedDataSet = [{
                        label: 'Visits',
                        fill: false,
                        borderColor: "#303f9fc4",
                        borderDash: [4, 4],
                        backgroundColor: "#303f9fc4",
                        pointBackgroundColor: "#303f9fc4",
                        pointBorderColor: "#303f9fc4",
                        pointHoverBackgroundColor: "#303f9fc4",
                        pointHoverBorderColor: "#303f9fc4",
                        data: []
                    }];
                    ResponseData.forEach(element => {
                        var Data = element;
                        this._ODealSalesHistory.Labels.push(element.Title);
                        TSaleDataSet[0].data.push(Math.round(Data.TotalDeals));
                        TSaleColor[0].backgroundColor.push("rgba(97, 192, 60, 0.7)");
                        TRedeemedDataSet[0].data.push(Math.round(Data.Used));
                        TRedeemedColor[0].backgroundColor.push("rgba(97, 192, 60, 0.7)");
                    });
                    this._ODealSalesHistory.SaleDataSet = TSaleDataSet;
                    this._ODealSalesHistory.SaleColors = TSaleColor;

                    this._ODealSalesHistory.RedeemDataSet = TRedeemedDataSet;
                    this._ODealSalesHistory.RedeemColors = TRedeemedColor;

                } else {
                    this._HelperService.IsFormProcessing = false;
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }
        );
    }
    //#endregion





    // @ViewChild("startTimePicker", { static: true }) _IonDatetime: IonDatetime;
    // constructor(
    //     public _FireBase: FCM,
    //     private sanitizer: DomSanitizer,
    //     public _DatePipe: DatePipe,
    //     public _HelperService: HelperService,
    //     public _AlertController: AlertController,
    //     public _MenuController: MenuController,
    //     public _ModalController: ModalController,
    //     public _DataService: DataService,
    // ) {
    //     var OOwnerId = this._HelperService.AccountInfo.UserAccount.AccountId;
    //     if (this._HelperService.AccountInfo.UserOwner != undefined && this._HelperService.AccountInfo.UserOwner != null && this._HelperService.AccountInfo.UserOwner.AccountId != 0) {
    //         OOwnerId = this._HelperService.AccountInfo.UserOwner.AccountId;
    //     }
    //     if (OOwnerId != undefined && OOwnerId != null && OOwnerId != 0) {
    //     }

    //     this._MenuController.enable(true);
    // }
    // Type = 1;
    // StartTime = null;
    // StartTimeS = null;
    // EndTime = null;
    // EndTimeS = null;
    // public MinDate = null;
    // public MaxDate = null;
    // UserAnalytics_DateChange(Type) {
    //     this.Type = Type;
    //     var SDate;
    //     if (Type == 1) {
    //         SDate =
    //         {
    //             start: moment().startOf('day'),
    //             end: moment().endOf('day'),
    //         }
    //     }
    //     else if (Type == 2) {
    //         SDate =
    //         {
    //             start: moment().subtract(1, 'days').startOf('day'),
    //             end: moment().subtract(1, 'days').endOf('day'),
    //         }
    //     }
    //     else if (Type == 3) {
    //         SDate =
    //         {
    //             start: moment().startOf('isoWeek'),
    //             end: moment().endOf('isoWeek'),
    //         }
    //     }
    //     else if (Type == 4) {
    //         SDate =
    //         {
    //             start: moment().startOf('month'),
    //             end: moment().endOf('month'),
    //         }
    //     }
    //     else if (Type == 5) {
    //         this._IonDatetime.open();
    //     }
    //     if (Type != 5) {
    //         this.StartTime = SDate.start;
    //         this.EndTime = SDate.end;
    //         this.LoadData();
    //     }

    // }
    // CustomDateChange() {
    //     this.StartTime = moment(this.SelectedItemDate).startOf('day');
    //     this.EndTime = moment(this.SelectedItemDate).endOf('day');
    //     this.LoadData();
    // }
    // ngAfterViewInit() {
    //     setTimeout(() => {
    //         this._MenuController.enable(true);
    //     }, 300);
    // }

    // ngOnInit() {
    //     this.MaxDate = this._HelperService.FormatDate(moment().startOf('day'), "YYYY-MM-DD");
    //     this.MinDate = this._HelperService.FormatDate(moment(this._HelperService.AccountInfo.UserAccount.CreateDate).startOf('day'), "YYYY-MM-DD");
    //     if (this.StartTime == undefined) {
    //         this.StartTime = moment().startOf('day');
    //         this.EndTime = moment().endOf('day');
    //     }
    //     this._HelperService.RefreshProfile();
    //     this._HelperService.RefreshLocation();
    //     this.LoadData();
    // }


    // public SelectedItemDate = null;
    // public DStartTimeS = "";
    // public DEndTimeS = "";
    // public Items = [];
    // LoadData() {
    //     this.DStartTimeS = this._HelperService.GetDateS(moment().startOf('day').subtract(15, 'day'));
    //     this.DEndTimeS = this._HelperService.GetDateS(moment().startOf('day').subtract(1, 'day'));
    //     this.StartTimeS = this._HelperService.GetDateS(this.StartTime);
    //     this.EndTimeS = this._HelperService.GetDateS(this.EndTime);
    //     this.StoresList_Data =
    //     {
    //         Type: "running",
    //         SearchContent: "",
    //         TotalRecords: 0,
    //         Offset: -1,
    //         Limit: 10,
    //         Data: []
    //     };
    //     this.StoresList_Setup();
    // }

    // DealStatusType(Type) {
    //     this.StoresList_Data =
    //     {
    //         Type: Type,
    //         SearchContent: "",
    //         TotalRecords: 0,
    //         Offset: -1,
    //         Limit: 10,
    //         Data: []
    //     };
    //     this.StoresList_Setup();
    // }
    // public StoresList_Data =
    //     {
    //         Type: "running",
    //         SearchContent: "",
    //         TotalRecords: 0,
    //         Offset: -1,
    //         Limit: 10,
    //         Data: []
    //     };
    // StoresList_Setup() {
    //     if (this.StoresList_Data.Offset == -1) {
    //         this._HelperService.ShowSpinner();
    //         this.StoresList_Data.Offset = 0;
    //     }
    //     var SCon = "";
    //     if (this.StoresList_Data.SearchContent != undefined && this.StoresList_Data.SearchContent != null && this.StoresList_Data.SearchContent != '') {
    //         SCon = this._HelperService.GetSearchCondition(SCon, 'Title', this._HelperService.AppConfig.DataType.Text, this.StoresList_Data.SearchContent);
    //         // SCon = this._HelperService.GetSearchCondition(SCon, 'CustomerDisplayName', this._HelperService.AppConfig.DataType.Text, this.StoresList_Data.SearchContent);
    //         // SCon = this._HelperService.GetSearchCondition(SCon, 'CustomerMobileNumber', this._HelperService.AppConfig.DataType.Text, this.StoresList_Data.SearchContent);
    //     }

    //     if (this.StoresList_Data.Type == "running") {

    //     }
    //     else if (this.StoresList_Data.Type == "upcoming") {
    //     }
    //     else if (this.StoresList_Data.Type == "paused") {
    //         SCon = this._HelperService.GetSearchCondition(SCon, 'StatusCode', this._HelperService.AppConfig.DataType.Text, "deal.paused");
    //     }
    //     else if (this.StoresList_Data.Type == "draft") {
    //         SCon = this._HelperService.GetSearchCondition(SCon, 'StatusCode', this._HelperService.AppConfig.DataType.Text, "deal.draft");
    //     }
    //     else if (this.StoresList_Data.Type == "pendingapproval") {
    //         SCon = this._HelperService.GetSearchCondition(SCon, 'StatusCode', this._HelperService.AppConfig.DataType.Text, "deal.approvalpending");
    //     }
    //     else if (this.StoresList_Data.Type == "expired") {
    //         SCon = this._HelperService.GetSearchCondition(SCon, 'StatusCode', this._HelperService.AppConfig.DataType.Text, "deal.expired");
    //     }
    //     else {

    //     }
    //     var pData = {
    //         Task: 'getdeals',
    //         TotalRecords: this.StoresList_Data.TotalRecords,
    //         Offset: this.StoresList_Data.Offset,
    //         Limit: this.StoresList_Data.Limit,
    //         RefreshCount: true,
    //         SearchCondition: SCon,
    //         SortExpression: 'StartDate asc',
    //         // AccountId: this._HelperService.AccountOwner.AccountId,
    //         // AccountKey: this._HelperService.AccountOwner.AccountKey,
    //     };
    //     let _OResponse: Observable<OResponse>;
    //     _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Deals, pData);
    //     _OResponse.subscribe(
    //         _Response => {
    //             this._HelperService.HideSpinner();
    //             console.log(_Response);
    //             if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
    //                 this.StoresList_Data.Offset = this.StoresList_Data.Offset + this.StoresList_Data.Limit;
    //                 this.StoresList_Data.TotalRecords = _Response.Result.TotalRecords;
    //                 var StoresList_Data = _Response.Result.Data;
    //                 StoresList_Data.forEach(element => {
    //                     element.StartDateS = this._HelperService.GetDateTimeS(element.StartDate);
    //                     element.EndDateS = this._HelperService.GetDateTimeS(element.EndDate);
    //                     element.CreateDate = this._HelperService.GetDateTimeS(element.CreateDate);
    //                     element.UseDate = this._HelperService.GetDateTimeS(element.UseDate);
    //                     element.LastTransactionDate = this._HelperService.GetDateTimeS(element.LastTransactionDate);
    //                     this.StoresList_Data.Data.push(element);
    //                 });
    //                 if (this.LoaderEvent != undefined) {
    //                     this.LoaderEvent.target.complete();
    //                     if (this.StoresList_Data.TotalRecords == this.StoresList_Data.Data.length) {
    //                         this.LoaderEvent.target.disabled = true;
    //                     }
    //                 }
    //             }
    //             else {
    //                 this._HelperService.HideSpinner();
    //             }
    //         },
    //         _Error => {
    //             this._HelperService.HandleException(_Error);
    //         });
    // }
    // StoresList_RowSelected(ReferenceData) {
    //     if (ReferenceData.PaymentStatusCode == 'paymentstatus.pending') {
    //         var _OrderDetails =
    //         {
    //             OrderId: ReferenceData.OrderId,
    //             OrderKey: ReferenceData.OrderReference,
    //         };
    //         this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveOrder, _OrderDetails);
    //         this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.orderpaymentprocess);
    //     }
    //     else {
    //         var _OrderDetails =
    //         {
    //             OrderId: ReferenceData.OrderId,
    //             OrderKey: ReferenceData.OrderReference,
    //         };
    //         this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveOrder, _OrderDetails);
    //         this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.orderdetails);
    //     }


    // }
    // public LoaderEvent: any = undefined;
    // NextLoad(event) {
    //     this.LoaderEvent = event;
    //     this.StoresList_Setup();

    // }
    // NavScanDeal() {
    //     this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Deals.scandealcode);
    // }
    // NavStore(Item) {
    //     this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference, Item);
    //     this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.Deals.dealdetails);
    // }

    // private delayTimer;
    // doSearch(text) {
    //     clearTimeout(this.delayTimer);
    //     this.delayTimer = setTimeout(x => {
    //         this.StoresList_Data =
    //         {
    //             Type: 'running',
    //             SearchContent: text,
    //             TotalRecords: 0,
    //             Offset: -1,
    //             Limit: 10,
    //             Data: []
    //         };
    //         this.StoresList_Setup();
    //     }, 1000);
    // }

}