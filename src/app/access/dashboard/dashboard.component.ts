import { Component, ViewChild, Sanitizer } from '@angular/core';
import { NavController, MenuController, AlertController, ModalController, IonDatetime } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from '../../service/helper.service';
declare let L;
import 'leaflet';
import { PlaceOrderModal } from '../modals/placeorder/placeorder.modal.component';
import { DataService } from '../../service/data.service';
import { DatePipe } from '@angular/common';
import { OResponse, OSalesSummary, ORewardsSummary, OTerminalStatusCount, } from '../../service/object.service';
declare var moment: any;
import { DomSanitizer } from "@angular/platform-browser";
import { FCM } from '@ionic-native/fcm/ngx';
import { ScanMyCodeModal } from '../modals/scanmycode/scanmycode.modal.component';
import { AddCustomerModal } from '../modals/addcustomer/addcustomer.modal.component';
import { TransactionDetailsModal } from '../modals/transactiondetails/transactiondetails.modal.component';
import { ModalRewardReceipt } from '../modals/rewardreceipt/rewardreceipt.modal.component';
import { DateSelectorModal } from '../modals/dateselector/dateselector.modal.component';

@Component({
    selector: 'app-dashboard',
    templateUrl: 'dashboard.component.html'
})
export class DashboardPage {

    slideOpts = {
        initialSlide: 1,
        speed: 400,
        slidesPerView: 1.6,
        spaceBetween: 32
    }

    PageType = 1;
    constructor(
        public _FireBase: FCM,
        private sanitizer: DomSanitizer,
        public _DatePipe: DatePipe,
        public _HelperService: HelperService,
        public _AlertController: AlertController,
        public _MenuController: MenuController,
        public _ModalController: ModalController,
        public _DataService: DataService,
    ) {
        var OOwnerId = this._HelperService.AccountInfo.UserAccount.AccountId;
        if (this._HelperService.AccountInfo.UserOwner != undefined && this._HelperService.AccountInfo.UserOwner != null && this._HelperService.AccountInfo.UserOwner.AccountId != 0) {
            OOwnerId = this._HelperService.AccountInfo.UserOwner.AccountId;
        }
        this._MenuController.enable(true);
    }

    PageTypeChange(PageType) {
        this.PageType = PageType;
        this.LoadTab();
    }
    LoadTab() {
        if (this.PageType == 1) {
            this.LoadOverview();
        }
         
    }
    ngOnInit() {
        this._DateConfig.MaxDate = this._HelperService.FormatDate(moment().startOf('day'), "YYYY-MM-DD");
        this._DateConfig.MinDate = this._HelperService.FormatDate(moment(this._HelperService.AccountInfo.UserAccount.CreateDate).startOf('day'), "YYYY-MM-DD");
        if (this._DateConfig.StartTime == undefined) {
            this._DateConfig.StartTime = moment().startOf('day');
            this._DateConfig.EndTime = moment().endOf('day');
        }
        this.LoadData();
    }

    NavReward() {
        this._HelperService.NavigateRootNoAnimate(this._HelperService.AppConfig.Pages.Access.Reward.ScanCode);
    }
    RedeemSt1() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Redeem.St1);
    }
    NavScanDeal() {
        this._HelperService.NavigateRootNoAnimate(this._HelperService.AppConfig.Pages.Access.Deals.scandealcode);
    }
      async ModalSaveCustomer() {
        const modal = await this._ModalController.create({
            component: AddCustomerModal,
        });
        modal.onDidDismiss().then(data => {
        });
        return await modal.present();
    }
    _DateConfig =
        {
            TypeName: 'Today',
            Type: 4,
            MinDate: null,
            MaxDate: null,
            StartTime: null,
            EndTime: null,
            StartTimeS: null,

            EndTimeS: null,
            SelectedStartDate: null,
            SelectedEndDate: null,
        }
    DateManager_Open() {
        this._HelperService.ShowTucDateModal();
    }
    DateManager_Confirm() {
        this._HelperService.HideTucDateModal();
        this.LoadTab();
    }
    DateManager_StartDateChange() {
        this._DateConfig.StartTime = moment(this._DateConfig.SelectedStartDate).startOf('day');
        this._DateConfig.SelectedEndDate = this._DateConfig.SelectedStartDate;
    }
    DateManager_EndDateChange() {
        this._DateConfig.EndTime = moment(this._DateConfig.SelectedEndDate).endOf('day');
    }
    DateManager_RangeClick(Type) {
        this._DateConfig.Type = Type;
        var SDate;
        if (Type == 1) {
            this._DateConfig.TypeName = "Today";
            SDate =
            {
                start: moment().startOf('day'),
                end: moment().endOf('day'),
            }
        }
        else if (Type == 2) {
            this._DateConfig.TypeName = "Yesterday";
            SDate =
            {
                start: moment().subtract(1, 'days').startOf('day'),
                end: moment().subtract(1, 'days').endOf('day'),
            }
        }
        else if (Type == 3) {
            this._DateConfig.TypeName = "Last 7 Days";
            SDate =
            {
                start: moment().subtract(6, 'days').startOf('day'),
                end: moment().endOf('days'),
            }
        }
        else if (Type == 4) {
            this._DateConfig.TypeName = "Last 15 Days";
            SDate =
            {
                start: moment().subtract(15, 'days').startOf('day'),
                end: moment().endOf('days'),
            }
        }
        else if (Type == 5) {
            this._DateConfig.TypeName = "Last 30 Days";
            SDate =
            {
                start: moment().subtract(30, 'days').startOf('day'),
                end: moment().endOf('days'),
            }
        }
        else if (Type == 6) {
            this._DateConfig.TypeName = "This Week";
            SDate =
            {
                start: moment().startOf('isoWeek').startOf('day'),
                end: moment().endOf('isoWeek').endOf('day'),
            }
        }
        else if (Type == 7) {
            this._DateConfig.TypeName = "Last Week";
            SDate =
            {
                start: moment().subtract(1, 'isoWeek').startOf('isoWeek').startOf('day'),
                end: moment().subtract(1, 'isoWeek').endOf('isoWeek').endOf('day'),
            }
        }
        else if (Type == 8) {
            this._DateConfig.TypeName = "This Month";
            SDate =
            {
                start: moment().startOf('month'),
                end: moment().endOf('month'),
            }
        }
        else if (Type == 9) {
            this._DateConfig.TypeName = "Last Month";
            SDate =
            {
                start: moment().subtract(1, 'month').startOf('month'),
                end: moment().subtract(1, 'month').endOf('month'),
            }
        }
        else if (Type == 10) {
            this._DateConfig.TypeName = "Last 3 Months";
            SDate =
            {
                start: moment().subtract(2, 'month').startOf('month'),
                end: moment().endOf('month'),
            }
        }
        else if (Type == 11) {
            this._DateConfig.TypeName = "Last 6 Months";
            SDate =
            {
                start: moment().subtract(5, 'month').startOf('month'),
                end: moment().endOf('month'),
            }
        }
        this._DateConfig.StartTime = SDate.start;
        this._DateConfig.EndTime = SDate.end;
        this.DateManager_Confirm();
    }
    LoadData() {
        this._DateConfig.StartTimeS = this._HelperService.GetDateS(this._DateConfig.StartTime);
        this._DateConfig.EndTimeS = this._HelperService.GetDateS(this._DateConfig.EndTime);
        this.LoadTab();
    }
  
    //#region 
    LoadOverview() {
        this.GetLoyaltyOverview();
        this.GetSalesHistory();
        this.GetLoyaltyVisitHistory();
        this.GetDealsOverview();
    }
    _LoyaltyOverview =
        {
            NewCustomerInvoiceAmount: 0,
            NewCustomers: 0,
            RedeemAmount: 0,
            RedeemInvoiceAmount: 0,
            RedeemTransaction: 0,
            RepeatingCustomerInvoiceAmount: 0,
            RepeatingCustomers: 0,
            RewardAmount: 0,
            RewardCommissionAmount: 0,
            RewardInvoiceAmount: 0,
            RewardTransaction: 0,
            Transaction: 0,
            TransactionInvoiceAmount: 0,
            TucPlusRewardClaimTransaction: 0,
            TucPlusRewardTransaction: 0,
            TucRewardAmount: 0,
            TucRewardCommissionAmount: 0,
            TucRewardInvoiceAmount: 0,
            TucRewardTransaction: 0,
            VisitsByRepeatingCustomers: 0,
        };

    public GetLoyaltyOverview() {
        var pData = {
            Task: 'getloyaltyoverview',
            AccountKey: this._HelperService.AccountOwner.AccountKey,
            AccountId: this._HelperService.AccountOwner.AccountId,
            StartDate: this._DateConfig.StartTime,
            EndDate: this._DateConfig.EndTime,
            Source: "transaction.source.merchant"
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Analytics, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    console.log(_Response);
                    this._LoyaltyOverview = _Response.Result;
                    console.log("_LoyaltyOverview", this._LoyaltyOverview);
                    // this._AccountBalance = _Response.Result;
                    // this._AccountBalance.Balance = this._HelperService.GetFixedDecimalNumber(_Response.Result.Balance / 100);
                    // this._AccountBalance.Credit = this._HelperService.GetFixedDecimalNumber(_Response.Result.Credit / 100);
                    // this._AccountBalance.Debit = this._HelperService.GetFixedDecimalNumber(_Response.Result.Debit / 100);
                    // this._AccountBalance.LastTransactionAmount = this._HelperService.GetFixedDecimalNumber(_Response.Result.LastTransactionAmount / 100);
                    // this._AccountBalance.LastTopupBalance = this._HelperService.GetFixedDecimalNumber(_Response.Result.LastTopupBalance / 100);
                    // this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.Balance, this._AccountBalance);
                } else {
                    this._HelperService.IsFormProcessing = false;
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }
        );
    }
    _DealStatusOverview =
    {
        Running: 0,
        Upcoming: 0,
        Total: 0,
        Draft: 0,
        ApprovalPending: 0,
        Approved: 0,
        Published: 0,
        Paused: 0,
        Expired: 0,
        Rejected: 0,
    };
public GetDealsOverview() {
    var pData = {
        Task: 'getdealsoverview',
        AccountKey: this._HelperService.AccountOwner.AccountKey,
        AccountId: this._HelperService.AccountOwner.AccountId,
        StartDate: this._DateConfig.StartTime,
        EndDate: this._DateConfig.EndTime,
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.DealManager, pData);
    _OResponse.subscribe(
        _Response => {
            if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                this._DealStatusOverview = _Response.Result;
            } else {
                this._HelperService.IsFormProcessing = false;
            }
        },
        _Error => {
            this._HelperService.HandleException(_Error);
        }
    );
}
    _SalesHistory =
        {
            TotalCustomer: 0,
            TotalInvoiceAmount: 0,
            TotalTransaction: 0,
            Data: [],
        }
    public _OSalesHistory =
        {
            Labels: [],
            SaleColors: [],
            SaleDataSet: [],
            SaleCustomersColors: [],
            SaleCustomersDataSet: [],

            TransactionsColors: [],
            TransactionsDataSet: [],
        }
    public GetSalesHistory() {
        var pData = {
            Task: 'getsaleshistory',
            AccountKey: this._HelperService.AccountOwner.AccountKey,
            AccountId: this._HelperService.AccountOwner.AccountId,
            StartDate: this._DateConfig.StartTime,
            EndDate: this._DateConfig.EndTime,
            Source: "transaction.source.merchant"
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Analytics, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    console.log('getsaleshistory', _Response);
                    var _Data = _Response.Result;
                    this._SalesHistory.Data = _Data;
                    _Data.forEach(data => {
                        this._SalesHistory.TotalCustomer += data.TotalCustomer;
                        this._SalesHistory.TotalInvoiceAmount += data.TotalInvoiceAmount;
                        this._SalesHistory.TotalTransaction += data.TotalTransaction;
                    });
                    this._OSalesHistory.Labels = [];
                    var ResponseData = _Response.Result;
                    var TSaleColor = [{
                        backgroundColor: [],
                    }];
                    var TTransactionsColor = [{
                        backgroundColor: [],
                    }];
                    var TSaleDataSet = [{
                        label: 'Sale',
                        fill: false,
                        borderColor: "#303f9fc4",
                        borderDash: [6, 6],
                        backgroundColor: "#303f9fc4",
                        pointBackgroundColor: "#303f9fc4",
                        pointBorderColor: "#303f9fc4",
                        pointHoverBackgroundColor: "#303f9fc4",
                        pointHoverBorderColor: "#303f9fc4",
                        data: []
                    }];
                    var TTransactionsDataSet = [{
                        label: 'Visits',
                        fill: false,
                        borderColor: "#303f9fc4",
                        borderDash: [4, 4],
                        backgroundColor: "#303f9fc4",
                        pointBackgroundColor: "#303f9fc4",
                        pointBorderColor: "#303f9fc4",
                        pointHoverBackgroundColor: "#303f9fc4",
                        pointHoverBorderColor: "#303f9fc4",
                        data: []
                    }];
                    ResponseData.forEach(element => {
                        var Data = element;
                        this._OSalesHistory.Labels.push(element.Date);
                        TSaleDataSet[0].data.push(Math.round(Data.TotalInvoiceAmount));
                        TSaleColor[0].backgroundColor.push("rgba(97, 192, 60, 0.7)");

                        TTransactionsDataSet[0].data.push(Math.round(Data.TotalTransaction));
                        TTransactionsColor[0].backgroundColor.push("rgba(97, 192, 60, 0.7)");
                    });
                    this._OSalesHistory.SaleDataSet = TSaleDataSet;
                    this._OSalesHistory.SaleColors = TSaleColor;

                    this._OSalesHistory.TransactionsDataSet = TTransactionsDataSet;
                    this._OSalesHistory.TransactionsColors = TTransactionsColor;





                } else {
                    this._HelperService.IsFormProcessing = false;
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }
        );
    }
    _LoyaltyHistory =
        {
            NewCustomer: 0,
            NewCustomerInvoiceAmount: 0,
            RepeatingCustomer: 0,
            RepeatingCustomerInvoiceAmount: 0,
            TotalCustomer: 0,
            TotalInvoiceAmount: 0,
            TotalTransaction: 0,
            VisitsByRepeatingCustomers: 0,
            Data: [],
        }
    public GetLoyaltyVisitHistory() {
        var pData = {
            Task: 'getloyaltyvisithistory',
            AccountKey: this._HelperService.AccountOwner.AccountKey,
            AccountId: this._HelperService.AccountOwner.AccountId,
            StartDate: this._DateConfig.StartTime,
            EndDate: this._DateConfig.EndTime,
            Source: "transaction.source.merchant"
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Analytics, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    console.log('getloyaltyvisithistory ', _Response);
                    var _Data = _Response.Result;
                    this._LoyaltyHistory =
                    {
                        NewCustomer: 0,
                        NewCustomerInvoiceAmount: 0,
                        RepeatingCustomer: 0,
                        RepeatingCustomerInvoiceAmount: 0,
                        TotalCustomer: 0,
                        TotalInvoiceAmount: 0,
                        TotalTransaction: 0,
                        VisitsByRepeatingCustomers: 0,
                        Data: [],
                    }
                    this._LoyaltyHistory.Data = _Data;
                    _Data.forEach(data => {
                        this._LoyaltyHistory.NewCustomer += data.NewCustomer;
                        this._LoyaltyHistory.NewCustomerInvoiceAmount += data.NewCustomerInvoiceAmount;
                        this._LoyaltyHistory.RepeatingCustomer += data.RepeatingCustomer;
                        this._LoyaltyHistory.RepeatingCustomerInvoiceAmount += data.RepeatingCustomerInvoiceAmount;
                        this._LoyaltyHistory.TotalCustomer += data.TotalCustomer;
                        this._LoyaltyHistory.TotalInvoiceAmount += data.TotalInvoiceAmount;
                        this._LoyaltyHistory.TotalTransaction += data.TotalTransaction;
                        this._LoyaltyHistory.VisitsByRepeatingCustomers += data.VisitsByRepeatingCustomers;
                    });

                } else {
                    this._HelperService.IsFormProcessing = false;
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }
        );
    }
    //#endregion


}

// import { Component, ViewChild, Sanitizer } from '@angular/core';
// import { NavController, MenuController, AlertController, ModalController, IonDatetime } from '@ionic/angular';
// import { Observable } from 'rxjs';
// import { HelperService } from '../../service/helper.service';
// declare let L;
// import 'leaflet';
// import { PlaceOrderModal } from '../modals/placeorder/placeorder.modal.component';
// import { DataService } from '../../service/data.service';
// import { DatePipe } from '@angular/common';
// import { OResponse, OSalesSummary, ORewardsSummary, OTerminalStatusCount, } from '../../service/object.service';
// declare var moment: any;
// import { DomSanitizer } from "@angular/platform-browser";
// import { FCM } from '@ionic-native/fcm/ngx';
// import { ScanMyCodeModal } from '../modals/scanmycode/scanmycode.modal.component';
// import { AddCustomerModal } from '../modals/addcustomer/addcustomer.modal.component';
// @Component({
//     selector: 'app-dashboard',
//     templateUrl: 'dashboard.component.html'
// })
// export class DashboardPage {
//     @ViewChild("startTimePicker", { static: true }) _IonDatetime: IonDatetime;
//     constructor(
//         public _FireBase: FCM,
//         private sanitizer: DomSanitizer,
//         public _DatePipe: DatePipe,
//         public _HelperService: HelperService,
//         public _AlertController: AlertController,
//         public _MenuController: MenuController,
//         public _ModalController: ModalController,
//         public _DataService: DataService,
//     ) {
//         var OOwnerId = this._HelperService.AccountInfo.UserAccount.AccountId;
//         if (this._HelperService.AccountInfo.UserOwner != undefined && this._HelperService.AccountInfo.UserOwner != null && this._HelperService.AccountInfo.UserOwner.AccountId != 0) {
//             OOwnerId = this._HelperService.AccountInfo.UserOwner.AccountId;
//         }
//         if (OOwnerId != undefined && OOwnerId != null && OOwnerId != 0) {
//         }
//         this._MenuController.enable(true);
//     }

//     Type = 1;
//     StartTime = null;
//     StartTimeS = null;
//     EndTime = null;
//     EndTimeS = null;
//     public MinDate = null;
//     public MaxDate = null;
//     UserAnalytics_DateChange(Type) {
//         this.Type = Type;
//         var SDate;
//         if (Type == 1) {
//             SDate =
//             {
//                 start: moment().startOf('day'),
//                 end: moment().endOf('day'),
//             }
//         }
//         else if (Type == 2) {
//             SDate =
//             {
//                 start: moment().subtract(1, 'days').startOf('day'),
//                 end: moment().subtract(1, 'days').endOf('day'),
//             }
//         }
//         else if (Type == 3) {
//             SDate =
//             {
//                 start: moment().startOf('isoWeek'),
//                 end: moment().endOf('isoWeek'),
//             }
//         }
//         else if (Type == 4) {
//             SDate =
//             {
//                 start: moment().startOf('month'),
//                 end: moment().endOf('month'),
//             }
//         }
//         else if (Type == 5) {
//             this._IonDatetime.open();
//         }
//         if (Type != 5) {
//             this.StartTime = SDate.start;
//             this.EndTime = SDate.end;
//             this.LoadData();
//         }

//     }
//     CustomDateChange() {
//         this.StartTime = moment(this.SelectedItemDate).startOf('day');
//         this.EndTime = moment(this.SelectedItemDate).endOf('day');
//         this.LoadData();
//     }
//     ngAfterViewInit() {
//         setTimeout(() => {
//             this._MenuController.enable(true);
//         }, 300);
//     }

//     ngOnInit() {
//         this._HelperService.RefreshProfile();
//         this._HelperService.RefreshLocation();
//         var DeviceStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
//         if (DeviceStorage != null) {
//             this._HelperService.AppConfig.ActiveLocation.Lat = DeviceStorage.Latitude;
//             this._HelperService.AppConfig.ActiveLocation.Lon = DeviceStorage.Longitude;
//         }
        
//         // this.MaxDate = this._HelperService.FormatDate(moment().startOf('day'), "YYYY-MM-DD");
//         // this.MinDate = this._HelperService.FormatDate(moment(this._HelperService.AccountInfo.UserAccount.CreateDate).startOf('day'), "YYYY-MM-DD");
//         // if (this.StartTime == undefined) {
//         //     this.StartTime = moment().startOf('day');
//         //     this.EndTime = moment().endOf('day');
//         // }
       
//         this._MenuController.enable(true);
       
//         this.LoadData();
//     }

//     public SelectedItemDate = null;
//     public DStartTimeS = "";
//     public DEndTimeS = "";
//     public Items = [];
//     LoadData() {
//         this.GetBalance();
//         this.Items = [];
//         if (this._HelperService.AccountOwner.IconUrl != undefined && this._HelperService.AccountOwner.IconUrl != null && this._HelperService.AccountOwner.IconUrl != "") {
//             var F =
//             {
//                 IconUrl: this.sanitizer.bypassSecurityTrustUrl(this._HelperService.AccountOwner.IconUrl.toString())
//             }
//             this.Items.push(F);
//         }

//         // this.DStartTimeS = this._HelperService.GetDateS(moment().startOf('day').subtract(15, 'day'));
//         // this.DEndTimeS = this._HelperService.GetDateS(moment().startOf('day').subtract(1, 'day'));
//         // this.StartTimeS = this._HelperService.GetDateS(this.StartTime);
//         // this.EndTimeS = this._HelperService.GetDateS(this.EndTime);
//         // this._HelperService.GetSalesSummary(this._HelperService.AccountOwner.AccountId, 108, 0, 0, this.StartTime, this.EndTime);
//         // this._HelperService.GetUserCounts(this._HelperService.AccountOwner.AccountId, 108, 0, 0, this.StartTime, this.EndTime);
//         // this._HelperService.GetRewardsSummary(this._HelperService.AccountOwner.AccountId, 108, 0, 0, this.StartTime, this.EndTime);
//         // this._HelperService.GetSalesHistory(this._HelperService.AccountOwner.AccountId, 108, 0, 0, moment().startOf('day').subtract(15, 'day'), moment().startOf('day').subtract(1, 'day'));
//         // this._HelperService.GetStoreVisits(this.StartTime, this.EndTime);
//         this.GetMerchantConfigurations();
//         this.UpdateToken();
//     }





//     //#region FCM Notification Update
//     UpdateToken() {
//         var Token = this._HelperService.GetStorageValue('dnot');
//         if (Token != null) {
//             if (this._HelperService.AccountInfo.UserAccount.AccountId != undefined && this._HelperService.AccountInfo.UserAccount.AccountId != null && this._HelperService.AccountInfo.UserAccount.AccountId != 0) {
//                 var DeviceInfo =
//                 {
//                     Task: "updatedevicenotificationurl",
//                     UserAccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
//                     UserAccountId: this._HelperService.AccountInfo.UserAccount.AccountId,
//                     NotificationUrl: Token,
//                 };
//                 let _OResponse: Observable<OResponse>;
//                 _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.System, DeviceInfo);
//                 _OResponse.subscribe(
//                     _Response => {
//                         this._HelperService.HideSpinner();
//                         if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
//                             this._HelperService.HideSpinner();
//                         }
//                         else {
//                             this._HelperService.Notify(_Response.Status, _Response.Message);
//                         }
//                     },
//                     _Error => {
//                         this._HelperService.HandleException(_Error);
//                     });

//             }
//         }
//     }
//     //#endregion

//     //#region  Home Screen Nav



//     //#endregion
//     //#region  Configuration
//     public _Configurations = {
//         RewardPercentage: null,
//         RewardMaximumInvoiceAmount: null,
//         RewardDeductionTypeCode: null,
//         RewardDeductionType: null,
//         MaximumRewardDeductionAmount: null,

//         ThankUPlus: null,
//         ThankUPlusCriteriaType: null,
//         ThankUPlusCriteriaTypeCode: null,
//         ThankUPlusCriteriaValue: null,
//         ThankUPlusMinimumTransferAmount: null,

//         SettlmentShedule: null,
//         SettlmentSheduleCode: null,
//         MinimumSettlementAmount: null,
//         enablethankucash: null,
//         merchantappvisiblity: null,
//         merchantreward: null,
//         merchantredeem: null,
//         rewardsmstucplus: null,
//         rewardsms: null,
//         redeemsms: null,
//         tucuserrewardonly: null,
//         cagiftpoints: null,
//         cagiftcards: null,
//         securitymaximumtransactionperdaycap: 3,
//         securitymaximuminvoiceamountperdaycap: 50000,
//         userrewardpercentage: null,
//     };
//     GetMerchantConfigurations() {
//         var PData =
//         {
//             Task: "getuserparameters",
//             SearchCondition: this._HelperService.GetSearchConditionStrict('', 'StatusCode', this._HelperService.AppConfig.DataType.Text, 'default.active', '='),
//             SortExpression: 'Name asc',
//             Offset: 0,
//             Limit: 1000,
//         }
//         PData.SearchCondition = this._HelperService.GetSearchConditionStrict(
//             PData.SearchCondition,
//             "TypeCode",
//             this._HelperService.AppConfig.DataType.Text,
//             'hcore.configurationvalue',
//             "="
//         );
//         PData.SearchCondition = this._HelperService.GetSearchConditionStrict(
//             PData.SearchCondition,
//             "UserAccountKey",
//             this._HelperService.AppConfig.DataType.Text,
//             this._HelperService.AccountOwner.AccountKey,
//             "="
//         );
//         let _OResponse: Observable<OResponse>;
//         _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V1.System, PData);
//         _OResponse.subscribe(
//             _Response => {
//                 if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
//                     if (_Response.Result.Data != undefined) {
//                         var ResposeData = _Response.Result.Data;
//                         ResposeData.forEach(element => {
//                             if (element.CommonSystemName == "rewardpercentage") {
//                                 this._Configurations.RewardPercentage = element.Value;
//                             }
//                             if (element.CommonSystemName == "rewardmaxinvoiceamount") {
//                                 this._Configurations.RewardMaximumInvoiceAmount = element.Value;
//                             }
//                             if (element.CommonSystemName == "rewarddeductiontype") {
//                                 this._Configurations.RewardDeductionType = element.HelperName;
//                                 this._Configurations.RewardDeductionTypeCode = element.HelperCode;
//                                 if (
//                                     this._Configurations.RewardDeductionTypeCode == 'rewarddeductiontype.prepay' ||
//                                     this._Configurations.RewardDeductionTypeCode == 'rewarddeductiontype.postpay' ||
//                                     this._Configurations.RewardDeductionTypeCode == 'rewarddeductiontype.prepayandpostpay'
//                                 ) {
//                                 }
//                             }
//                             if (element.CommonSystemName == "maximumrewarddeductionamount") {
//                                 this._Configurations.MaximumRewardDeductionAmount = element.Value;
//                             }
//                             if (element.CommonSystemName == "cagiftpoints") {
//                                 this._Configurations.cagiftpoints = element.Value;
//                             }
//                             if (element.CommonSystemName == "cagiftcards") {
//                                 this._Configurations.cagiftcards = element.Value;
//                             }
//                             if (element.CommonSystemName == "userrewardpercentage") {
//                                 this._Configurations.userrewardpercentage = element.Value;
//                             }

//                             if (element.CommonSystemName == "settlementsheduletype") {
//                                 this._Configurations.SettlmentShedule = element.HelperName;
//                                 this._Configurations.SettlmentSheduleCode = element.HelperCode;
//                             }
//                             if (element.CommonSystemName == "minimumsettlementamount") {
//                                 this._Configurations.MinimumSettlementAmount = element.Value;
//                             }

//                             if (element.CommonSystemName == "thankucashplus") {
//                                 this._Configurations.ThankUPlus = element.Value;
//                             }
//                             if (element.CommonSystemName == "enablethankucash") {
//                                 this._Configurations.enablethankucash = element.Value;
//                             }

//                             if (element.CommonSystemName == "merchantappvisiblity") {
//                                 this._Configurations.merchantappvisiblity = element.Value;
//                             }
//                             if (element.CommonSystemName == "merchantreward") {
//                                 this._Configurations.merchantreward = element.Value;
//                             }
//                             if (element.CommonSystemName == "merchantredeem") {
//                                 this._Configurations.merchantredeem = element.Value;
//                             }
//                             if (element.CommonSystemName == "rewardsmstucplus") {
//                                 this._Configurations.rewardsmstucplus = element.Value;
//                             }
//                             if (element.CommonSystemName == "rewardsms") {
//                                 this._Configurations.rewardsms = element.Value;
//                             }
//                             if (element.CommonSystemName == "redeemsms") {
//                                 this._Configurations.redeemsms = element.Value;
//                             }
//                             if (element.CommonSystemName == "thankucashplusrewardcriteria") {
//                                 this._Configurations.ThankUPlusCriteriaValue = element.Value;
//                                 this._Configurations.ThankUPlusCriteriaType = element.HelperName;
//                                 this._Configurations.ThankUPlusCriteriaTypeCode = element.HelperCode;
//                             }
//                             if (element.CommonSystemName == "thankucashplusmintransferamount") {
//                                 this._Configurations.ThankUPlusMinimumTransferAmount = element.Value;
//                             }

//                             if (element.CommonSystemName == "securitymaximumtransactionperdaycap") {
//                                 this._Configurations.securitymaximumtransactionperdaycap = element.Value;
//                             }
//                             if (element.CommonSystemName == "securitymaximuminvoiceamountperdaycap") {
//                                 this._Configurations.securitymaximuminvoiceamountperdaycap = element.Value;
//                             }
//                             if (element.CommonSystemName == "tucuserrewardonly") {
//                                 this._Configurations.tucuserrewardonly = element.Value;
//                             }
//                         });
//                     }
//                 }
//                 else {
//                     this._HelperService.NotifySimple(_Response.Message);
//                 }
//             },
//             _Error => {
//                 this._HelperService.HandleException(_Error);
//             });
//     }
//     //#endregion
//     //#region  Balance Refresh
//     public _AccountBalance =
//         {
//             Credit: 0,
//             Debit: 0,
//             Balance: 0,
//             LastTransactionAmount: 0,
//             LastTopupBalance: 0,
//             LastTransactionDate: 0,
//         }
//     public GetBalance() {
//         var Balance = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Balance);
//         if (Balance != null) {
//             this._AccountBalance = Balance;
//         }
//         var pData = {
//             Task: 'getaccountbalance',
//             AccountKey: this._HelperService.AccountOwner.AccountKey,
//             AccountId: this._HelperService.AccountOwner.AccountId,
//             Source: "transaction.source.merchant"
//         };
//         let _OResponse: Observable<OResponse>;
//         _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Subscription, pData);
//         _OResponse.subscribe(
//             _Response => {
//                 if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
//                     console.log(_Response);
//                     this._AccountBalance = _Response.Result;
//                     this._AccountBalance.Balance = this._HelperService.GetFixedDecimalNumber(_Response.Result.Balance / 100);
//                     this._AccountBalance.Credit = this._HelperService.GetFixedDecimalNumber(_Response.Result.Credit / 100);
//                     this._AccountBalance.Debit = this._HelperService.GetFixedDecimalNumber(_Response.Result.Debit / 100);
//                     this._AccountBalance.LastTransactionAmount = this._HelperService.GetFixedDecimalNumber(_Response.Result.LastTransactionAmount / 100);
//                     this._AccountBalance.LastTopupBalance = this._HelperService.GetFixedDecimalNumber(_Response.Result.LastTopupBalance / 100);
//                     this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.Balance, this._AccountBalance);
//                 } else {
//                     this._HelperService.IsFormProcessing = false;
//                 }
//             },
//             _Error => {
//                 this._HelperService.HandleException(_Error);
//             }
//         );
//     }
//     public GetBalanceT() {

//         var pData = {
//             Task: 'getuseraccountbalance',
//             UserAccountKey: this._HelperService.AccountOwner.AccountKey,
//             Source: "transaction.source.merchant"
//         };
//         let _OResponse: Observable<OResponse>;
//         _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.TUCAccCore, pData);
//         _OResponse.subscribe(
//             _Response => {
//                 if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {

//                 } else {
//                     this._HelperService.NotifySimple(_Response.Message);
//                 }
//             },
//             _Error => {
//                 this._HelperService.HandleException(_Error);
//             }
//         );
//     }
//     //#endregion
// }