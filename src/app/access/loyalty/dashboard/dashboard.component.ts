import { Component, ViewChild, Sanitizer } from '@angular/core';
import { NavController, MenuController, AlertController, ModalController, IonDatetime } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from '../../../service/helper.service';
declare let L;
import 'leaflet';
import { PlaceOrderModal } from '../../modals/placeorder/placeorder.modal.component';
import { DataService } from '../../../service/data.service';
import { DatePipe } from '@angular/common';
import { OResponse, OSalesSummary, ORewardsSummary, OTerminalStatusCount, } from '../../../service/object.service';
declare var moment: any;
import { DomSanitizer } from "@angular/platform-browser";
import { FCM } from '@ionic-native/fcm/ngx';
import { ScanMyCodeModal } from '../../modals/scanmycode/scanmycode.modal.component';
import { AddCustomerModal } from '../../modals/addcustomer/addcustomer.modal.component';
import { TransactionDetailsModal } from '../../modals/transactiondetails/transactiondetails.modal.component';
import { ModalRewardReceipt } from '../../modals/rewardreceipt/rewardreceipt.modal.component';
import { DateSelectorModal } from '../../modals/dateselector/dateselector.modal.component';

@Component({
    selector: 'app-dashboard',
    templateUrl: 'dashboard.component.html'
})
export class DashboardPage {

    slideOpts = {
        initialSlide: 1,
        speed: 400,
        slidesPerView: 1.6,
        spaceBetween: 32
    }

    PageType = 1;
    constructor(
        public _FireBase: FCM,
        private sanitizer: DomSanitizer,
        public _DatePipe: DatePipe,
        public _HelperService: HelperService,
        public _AlertController: AlertController,
        public _MenuController: MenuController,
        public _ModalController: ModalController,
        public _DataService: DataService,
    ) {
        var OOwnerId = this._HelperService.AccountInfo.UserAccount.AccountId;
        if (this._HelperService.AccountInfo.UserOwner != undefined && this._HelperService.AccountInfo.UserOwner != null && this._HelperService.AccountInfo.UserOwner.AccountId != 0) {
            OOwnerId = this._HelperService.AccountInfo.UserOwner.AccountId;
        }
        this._MenuController.enable(true);
    }

    PageTypeChange(PageType) {
        this.PageType = PageType;
        this.LoadTab();
    }
    LoadTab() {
        if (this.PageType == 1) {
            this.LoadOverview();
        }
        if (this.PageType == 2) {
            this.Reward_Load();
        }
        if (this.PageType == 3) {
            this.Redeem_Load();
        }
        if (this.PageType == 4) {
            this.PendingReward_Load();
        }
    }
    ngOnInit() {
        this._DateConfig.MaxDate = this._HelperService.FormatDate(moment().startOf('day'), "YYYY-MM-DD");
        this._DateConfig.MinDate = this._HelperService.FormatDate(moment(this._HelperService.AccountInfo.UserAccount.CreateDate).startOf('day'), "YYYY-MM-DD");
        if (this._DateConfig.StartTime == undefined) {
            this._DateConfig.StartTime = moment().startOf('day');
            this._DateConfig.EndTime = moment().endOf('day');
        }
        this.LoadData();
    }




    _DateConfig =
        {
            TypeName: 'Today',
            Type: 4,
            MinDate: null,
            MaxDate: null,
            StartTime: null,
            EndTime: null,
            StartTimeS: null,

            EndTimeS: null,
            SelectedStartDate: null,
            SelectedEndDate: null,
        }
    DateManager_Open() {
        this._HelperService.ShowTucDateModal();
    }
    DateManager_Confirm() {
        this._HelperService.HideTucDateModal();
        this.LoadTab();
    }
    DateManager_StartDateChange() {
        this._DateConfig.StartTime = moment(this._DateConfig.SelectedStartDate).startOf('day');
        this._DateConfig.SelectedEndDate = this._DateConfig.SelectedStartDate;
    }
    DateManager_EndDateChange() {
        this._DateConfig.EndTime = moment(this._DateConfig.SelectedEndDate).endOf('day');
    }
    DateManager_RangeClick(Type) {
        this._DateConfig.Type = Type;
        var SDate;
        if (Type == 1) {
            this._DateConfig.TypeName = "Today";
            SDate =
            {
                start: moment().startOf('day'),
                end: moment().endOf('day'),
            }
        }
        else if (Type == 2) {
            this._DateConfig.TypeName = "Yesterday";
            SDate =
            {
                start: moment().subtract(1, 'days').startOf('day'),
                end: moment().subtract(1, 'days').endOf('day'),
            }
        }
        else if (Type == 3) {
            this._DateConfig.TypeName = "Last 7 Days";
            SDate =
            {
                start: moment().subtract(6, 'days').startOf('day'),
                end: moment().endOf('days'),
            }
        }
        else if (Type == 4) {
            this._DateConfig.TypeName = "Last 15 Days";
            SDate =
            {
                start: moment().subtract(15, 'days').startOf('day'),
                end: moment().endOf('days'),
            }
        }
        else if (Type == 5) {
            this._DateConfig.TypeName = "Last 30 Days";
            SDate =
            {
                start: moment().subtract(30, 'days').startOf('day'),
                end: moment().endOf('days'),
            }
        }
        else if (Type == 6) {
            this._DateConfig.TypeName = "This Week";
            SDate =
            {
                start: moment().startOf('isoWeek').startOf('day'),
                end: moment().endOf('isoWeek').endOf('day'),
            }
        }
        else if (Type == 7) {
            this._DateConfig.TypeName = "Last Week";
            SDate =
            {
                start: moment().subtract(1, 'isoWeek').startOf('isoWeek').startOf('day'),
                end: moment().subtract(1, 'isoWeek').endOf('isoWeek').endOf('day'),
            }
        }
        else if (Type == 8) {
            this._DateConfig.TypeName = "This Month";
            SDate =
            {
                start: moment().startOf('month'),
                end: moment().endOf('month'),
            }
        }
        else if (Type == 9) {
            this._DateConfig.TypeName = "Last Month";
            SDate =
            {
                start: moment().subtract(1, 'month').startOf('month'),
                end: moment().subtract(1, 'month').endOf('month'),
            }
        }
        else if (Type == 10) {
            this._DateConfig.TypeName = "Last 3 Months";
            SDate =
            {
                start: moment().subtract(2, 'month').startOf('month'),
                end: moment().endOf('month'),
            }
        }
        else if (Type == 11) {
            this._DateConfig.TypeName = "Last 6 Months";
            SDate =
            {
                start: moment().subtract(5, 'month').startOf('month'),
                end: moment().endOf('month'),
            }
        }
        this._DateConfig.StartTime = SDate.start;
        this._DateConfig.EndTime = SDate.end;
        this.DateManager_Confirm();
    }
    LoadData() {
        this._DateConfig.StartTimeS = this._HelperService.GetDateS(this._DateConfig.StartTime);
        this._DateConfig.EndTimeS = this._HelperService.GetDateS(this._DateConfig.EndTime);
        this.LoadTab();
    }

    //#region  Rewards


    Reward_Load() {
        this.Reward_TranList_Data =
        {
            SearchContent: "",
            TotalRecords: 0,
            Offset: -1,
            Limit: 10,
            Data: []
        };
        this.Reward_TranList_Setup();
    }
    public Reward_TranList_Data =
        {
            SearchContent: "",
            TotalRecords: 0,
            Offset: -1,
            Limit: 10,
            Data: []
        };
    public Reward_TranList_Data_Overview =
        {
            Transactions: 0,
            InvoiceAmount: 0,
            RewardAmount: 0,
            UserAmount: 0,
        };
    private Reward_TranList_delayTimer;
    public Reward_TranList_LoaderEvent: any = undefined;
    Reward_TranList_Setup() {
        this.Reward_TranList_Setup_Overview();
        if (this.Reward_TranList_Data.Offset == -1) {
            this._HelperService.ShowSpinner();
            this.Reward_TranList_Data.Offset = 0;
        }
        var SCon = "";
        if (this.Reward_TranList_Data.SearchContent != undefined && this.Reward_TranList_Data.SearchContent != null && this.Reward_TranList_Data.SearchContent != '') {
            SCon = this._HelperService.GetSearchCondition(SCon, 'UserDisplayName', this._HelperService.AppConfig.DataType.Text, this.Reward_TranList_Data.SearchContent);
            SCon = this._HelperService.GetSearchCondition(SCon, 'UserMobileNumber', this._HelperService.AppConfig.DataType.Text, this.Reward_TranList_Data.SearchContent);
            SCon = this._HelperService.GetSearchCondition(SCon, 'CashierDisplayName', this._HelperService.AppConfig.DataType.Text, this.Reward_TranList_Data.SearchContent);
            SCon = this._HelperService.GetSearchCondition(SCon, 'CashierCode', this._HelperService.AppConfig.DataType.Text, this.Reward_TranList_Data.SearchContent);
            SCon = this._HelperService.GetSearchCondition(SCon, 'ReferenceNumber', this._HelperService.AppConfig.DataType.Text, this.Reward_TranList_Data.SearchContent);
        }
        SCon = this._HelperService.GetDateCondition(SCon, 'TransactionDate', this._DateConfig.StartTime, this._DateConfig.EndTime);
        var pData = {
            Task: 'getrewardtransactions',
            TotalRecords: this.Reward_TranList_Data.TotalRecords,
            Offset: this.Reward_TranList_Data.Offset,
            Limit: this.Reward_TranList_Data.Limit,
            RefreshCount: true,
            SearchCondition: SCon,
            SortExpression: 'TransactionDate desc',
            AccountId: this._HelperService.AccountOwner.AccountId,
            AccountKey: this._HelperService.AccountOwner.AccountKey,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Transactions, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideSpinner();
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this.Reward_TranList_Data.Offset = this.Reward_TranList_Data.Offset + this.Reward_TranList_Data.Limit;
                    this.Reward_TranList_Data.TotalRecords = _Response.Result.TotalRecords;
                    var Reward_TranList_Data = _Response.Result.Data;
                    Reward_TranList_Data.forEach(element => {
                        if (element.CardBrandName != undefined) {
                            element.CardBrandName = element.CardBrandName.toLowerCase().trim();
                        }
                        element.TypeName = element.TypeName.toLowerCase().trim();
                        element.TransactionDate = this._HelperService.GetDateTimeS(element.TransactionDate);
                        this.Reward_TranList_Data.Data.push(element);
                    });
                    if (this.Reward_TranList_LoaderEvent != undefined) {
                        this.Reward_TranList_LoaderEvent.target.complete();
                        if (this.Reward_TranList_Data.TotalRecords == this.Reward_TranList_Data.Data.length) {
                            this.Reward_TranList_LoaderEvent.target.disabled = true;
                        }
                    }
                }
                else {
                    this._HelperService.HideSpinner();
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    Reward_TranList_Setup_Overview() {
        var SCon = "";
        if (this.Reward_TranList_Data.SearchContent != undefined && this.Reward_TranList_Data.SearchContent != null && this.Reward_TranList_Data.SearchContent != '') {
            SCon = this._HelperService.GetSearchCondition(SCon, 'UserDisplayName', this._HelperService.AppConfig.DataType.Text, this.Reward_TranList_Data.SearchContent);
            SCon = this._HelperService.GetSearchCondition(SCon, 'UserMobileNumber', this._HelperService.AppConfig.DataType.Text, this.Reward_TranList_Data.SearchContent);
            SCon = this._HelperService.GetSearchCondition(SCon, 'CashierDisplayName', this._HelperService.AppConfig.DataType.Text, this.Reward_TranList_Data.SearchContent);
            SCon = this._HelperService.GetSearchCondition(SCon, 'CashierCode', this._HelperService.AppConfig.DataType.Text, this.Reward_TranList_Data.SearchContent);
            SCon = this._HelperService.GetSearchCondition(SCon, 'ReferenceNumber', this._HelperService.AppConfig.DataType.Text, this.Reward_TranList_Data.SearchContent);
        }
        SCon = this._HelperService.GetDateCondition(SCon, 'TransactionDate', this._DateConfig.StartTime, this._DateConfig.EndTime);
        var pData = {
            Task: 'getrewardtransactionsoverview',
            TotalRecords: this.Reward_TranList_Data.TotalRecords,
            Offset: this.Reward_TranList_Data.Offset,
            Limit: this.Reward_TranList_Data.Limit,
            RefreshCount: true,
            SearchCondition: SCon,
            SortExpression: 'TransactionDate desc',
            AccountId: this._HelperService.AccountOwner.AccountId,
            AccountKey: this._HelperService.AccountOwner.AccountKey,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Transactions, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideSpinner();
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this.Reward_TranList_Data_Overview = _Response.Result;
                    // this.Reward_TranList_Data.Offset = this.Reward_TranList_Data.Offset + this.Reward_TranList_Data.Limit;
                    // this.Reward_TranList_Data.TotalRecords = _Response.Result.TotalRecords;
                    // var Reward_TranList_Data = _Response.Result.Data;
                    // Reward_TranList_Data.forEach(element => {
                    //     if (element.CardBrandName != undefined) {
                    //         element.CardBrandName = element.CardBrandName.toLowerCase().trim();
                    //     }
                    //     element.TypeName = element.TypeName.toLowerCase().trim();
                    //     element.TransactionDate = this._HelperService.GetDateTimeS(element.TransactionDate);
                    //     this.Reward_TranList_Data.Data.push(element);
                    // });
                    // if (this.Reward_TranList_LoaderEvent != undefined) {
                    //     this.Reward_TranList_LoaderEvent.target.complete();
                    //     if (this.Reward_TranList_Data.TotalRecords == this.Reward_TranList_Data.Data.length) {
                    //         this.Reward_TranList_LoaderEvent.target.disabled = true;
                    //     }
                    // }
                }
                else {
                    this._HelperService.HideSpinner();
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    Reward_TranList_RowSelected(ReferenceData) {
        if (ReferenceData.PaymentStatusCode == 'paymentstatus.pending') {
            var _OrderDetails =
            {
                OrderId: ReferenceData.OrderId,
                OrderKey: ReferenceData.OrderReference,
            };
            this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveOrder, _OrderDetails);
            this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.orderpaymentprocess);
        }
        else {
            var _OrderDetails =
            {
                OrderId: ReferenceData.OrderId,
                OrderKey: ReferenceData.OrderReference,
            };
            this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveOrder, _OrderDetails);
            this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.orderdetails);
        }


    }
    Reward_TranList_NextLoad(event) {
        this.Reward_TranList_LoaderEvent = event;
        this.Reward_TranList_Setup();

    }
    Reward_TranList_Search(text) {
        clearTimeout(this.Reward_TranList_delayTimer);
        this.Reward_TranList_delayTimer = setTimeout(x => {
            this.Reward_TranList_Data =
            {
                SearchContent: text,
                TotalRecords: 0,
                Offset: -1,
                Limit: 10,
                Data: []
            };
            this.Reward_TranList_Setup();
        }, 1000);
    }
    async Reward_TranList_OpenAddModal(Item) {
        Item.MType = "reward"; const modal = await this._ModalController.create({
            component: ModalRewardReceipt,
            componentProps: Item
        });
        return await modal.present();
    }
    //#endregion

    //#region Redeem

    Redeem_Load() {
        this.Redeem_TranList_Data =
        {
            SearchContent: "",
            TotalRecords: 0,
            Offset: -1,
            Limit: 10,
            Data: []
        };
        this.Redeem_TranList_Setup();
    }
    public Redeem_TranList_Data =
        {
            SearchContent: "",
            TotalRecords: 0,
            Offset: -1,
            Limit: 10,
            Data: []
        };
    public Redeem_TranList_Data_Overview =
        {
            Transactions: 0,
            InvoiceAmount: 0,
            RedeemAmount: 0,
        };
    private Redeem_TranList_delayTimer;
    public Redeem_TranList_LoaderEvent: any = undefined;
    Redeem_TranList_Setup() {
        this.Redeem_TranList_Setup_Overview();
        if (this.Redeem_TranList_Data.Offset == -1) {
            this._HelperService.ShowSpinner();
            this.Redeem_TranList_Data.Offset = 0;
        }
        var SCon = "";
        if (this.Redeem_TranList_Data.SearchContent != undefined && this.Redeem_TranList_Data.SearchContent != null && this.Redeem_TranList_Data.SearchContent != '') {
            SCon = this._HelperService.GetSearchCondition(SCon, 'UserDisplayName', this._HelperService.AppConfig.DataType.Text, this.Redeem_TranList_Data.SearchContent);
            SCon = this._HelperService.GetSearchCondition(SCon, 'UserMobileNumber', this._HelperService.AppConfig.DataType.Text, this.Redeem_TranList_Data.SearchContent);
            SCon = this._HelperService.GetSearchCondition(SCon, 'CashierDisplayName', this._HelperService.AppConfig.DataType.Text, this.Redeem_TranList_Data.SearchContent);
            SCon = this._HelperService.GetSearchCondition(SCon, 'CashierCode', this._HelperService.AppConfig.DataType.Text, this.Redeem_TranList_Data.SearchContent);
            SCon = this._HelperService.GetSearchCondition(SCon, 'ReferenceNumber', this._HelperService.AppConfig.DataType.Text, this.Redeem_TranList_Data.SearchContent);
        }
        SCon = this._HelperService.GetDateCondition(SCon, 'TransactionDate', this._DateConfig.StartTime, this._DateConfig.EndTime);
        var pData = {
            Task: 'getredeemtransactions',
            TotalRecords: this.Redeem_TranList_Data.TotalRecords,
            Offset: this.Redeem_TranList_Data.Offset,
            Limit: this.Redeem_TranList_Data.Limit,
            RefreshCount: true,
            SearchCondition: SCon,
            SortExpression: 'TransactionDate desc',
            AccountId: this._HelperService.AccountOwner.AccountId,
            AccountKey: this._HelperService.AccountOwner.AccountKey,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Transactions, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideSpinner();
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this.Redeem_TranList_Data.Offset = this.Redeem_TranList_Data.Offset + this.Redeem_TranList_Data.Limit;
                    this.Redeem_TranList_Data.TotalRecords = _Response.Result.TotalRecords;
                    var Redeem_TranList_Data = _Response.Result.Data;
                    Redeem_TranList_Data.forEach(element => {
                        if (element.CardBrandName != undefined) {
                            element.CardBrandName = element.CardBrandName.toLowerCase().trim();
                        }
                        element.TypeName = element.TypeName.toLowerCase().trim();
                        element.TransactionDate = this._HelperService.GetDateTimeS(element.TransactionDate);
                        this.Redeem_TranList_Data.Data.push(element);
                    });
                    if (this.Redeem_TranList_LoaderEvent != undefined) {
                        this.Redeem_TranList_LoaderEvent.target.complete();
                        if (this.Redeem_TranList_Data.TotalRecords == this.Redeem_TranList_Data.Data.length) {
                            this.Redeem_TranList_LoaderEvent.target.disabled = true;
                        }
                    }
                }
                else {
                    this._HelperService.HideSpinner();
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    Redeem_TranList_Setup_Overview() {
        var SCon = "";
        if (this.Redeem_TranList_Data.SearchContent != undefined && this.Redeem_TranList_Data.SearchContent != null && this.Redeem_TranList_Data.SearchContent != '') {
            SCon = this._HelperService.GetSearchCondition(SCon, 'UserDisplayName', this._HelperService.AppConfig.DataType.Text, this.Redeem_TranList_Data.SearchContent);
            SCon = this._HelperService.GetSearchCondition(SCon, 'UserMobileNumber', this._HelperService.AppConfig.DataType.Text, this.Redeem_TranList_Data.SearchContent);
            SCon = this._HelperService.GetSearchCondition(SCon, 'CashierDisplayName', this._HelperService.AppConfig.DataType.Text, this.Redeem_TranList_Data.SearchContent);
            SCon = this._HelperService.GetSearchCondition(SCon, 'CashierCode', this._HelperService.AppConfig.DataType.Text, this.Redeem_TranList_Data.SearchContent);
            SCon = this._HelperService.GetSearchCondition(SCon, 'ReferenceNumber', this._HelperService.AppConfig.DataType.Text, this.Redeem_TranList_Data.SearchContent);
        }
        SCon = this._HelperService.GetDateCondition(SCon, 'TransactionDate', this._DateConfig.StartTime, this._DateConfig.EndTime);
        var pData = {
            Task: 'getredeemtransactionsoverview',
            TotalRecords: this.Redeem_TranList_Data.TotalRecords,
            Offset: this.Redeem_TranList_Data.Offset,
            Limit: this.Redeem_TranList_Data.Limit,
            RefreshCount: true,
            SearchCondition: SCon,
            SortExpression: 'TransactionDate desc',
            AccountId: this._HelperService.AccountOwner.AccountId,
            AccountKey: this._HelperService.AccountOwner.AccountKey,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Transactions, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideSpinner();
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this.Redeem_TranList_Data_Overview = _Response.Result;
                }
                else {
                    this._HelperService.HideSpinner();
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    Redeem_TranList_RowSelected(ReferenceData) {
        if (ReferenceData.PaymentStatusCode == 'paymentstatus.pending') {
            var _OrderDetails =
            {
                OrderId: ReferenceData.OrderId,
                OrderKey: ReferenceData.OrderReference,
            };
            this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveOrder, _OrderDetails);
            this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.orderpaymentprocess);
        }
        else {
            var _OrderDetails =
            {
                OrderId: ReferenceData.OrderId,
                OrderKey: ReferenceData.OrderReference,
            };
            this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveOrder, _OrderDetails);
            this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.orderdetails);
        }


    }
    Redeem_TranList_NextLoad(event) {
        this.Redeem_TranList_LoaderEvent = event;
        this.Redeem_TranList_Setup();

    }
    Redeem_TranList_Search(text) {
        clearTimeout(this.Redeem_TranList_delayTimer);
        this.Redeem_TranList_delayTimer = setTimeout(x => {
            this.Redeem_TranList_Data =
            {
                SearchContent: text,
                TotalRecords: 0,
                Offset: -1,
                Limit: 10,
                Data: []
            };
            this.Redeem_TranList_Setup();
        }, 1000);
    }
    async Redeem_TranList_OpenAddModal(Item) {
        Item.MType = "redeem";
        const modal = await this._ModalController.create({
            component: ModalRewardReceipt,
            componentProps: Item
        });
        return await modal.present();
    }
    //#endregion

    //#region Pending Rewards

    PendingReward_Load() {
        this.PendingReward_TranList_Data =
        {
            SearchContent: "",
            TotalRecords: 0,
            Offset: -1,
            Limit: 10,
            Data: []
        };
        this.PendingReward_TranList_Setup();
    }
    public PendingReward_TranList_Data =
        {
            SearchContent: "",
            TotalRecords: 0,
            Offset: -1,
            Limit: 10,
            Data: []
        };
    public PendingReward_TranList_Data_Overview =
        {
            Transactions: 0,
            InvoiceAmount: 0,
            RewardAmount: 0,
            UserAmount: 0,
        };
    private PendingReward_TranList_delayTimer;
    public PendingReward_TranList_LoaderEvent: any = undefined;
    PendingReward_TranList_Setup() {
        this.PendingReward_TranList_Setup_Overview();
        if (this.PendingReward_TranList_Data.Offset == -1) {
            this._HelperService.ShowSpinner();
            this.PendingReward_TranList_Data.Offset = 0;
        }
        var SCon = "";
        if (this.PendingReward_TranList_Data.SearchContent != undefined && this.PendingReward_TranList_Data.SearchContent != null && this.PendingReward_TranList_Data.SearchContent != '') {
            SCon = this._HelperService.GetSearchCondition(SCon, 'UserDisplayName', this._HelperService.AppConfig.DataType.Text, this.PendingReward_TranList_Data.SearchContent);
            SCon = this._HelperService.GetSearchCondition(SCon, 'UserMobileNumber', this._HelperService.AppConfig.DataType.Text, this.PendingReward_TranList_Data.SearchContent);
            SCon = this._HelperService.GetSearchCondition(SCon, 'CashierDisplayName', this._HelperService.AppConfig.DataType.Text, this.PendingReward_TranList_Data.SearchContent);
            SCon = this._HelperService.GetSearchCondition(SCon, 'CashierCode', this._HelperService.AppConfig.DataType.Text, this.PendingReward_TranList_Data.SearchContent);
            SCon = this._HelperService.GetSearchCondition(SCon, 'ReferenceNumber', this._HelperService.AppConfig.DataType.Text, this.PendingReward_TranList_Data.SearchContent);
        }
        SCon = this._HelperService.GetDateCondition(SCon, 'TransactionDate', this._DateConfig.StartTime, this._DateConfig.EndTime);
        var pData = {
            Task: 'getpendingrewardtransactions',
            TotalRecords: this.PendingReward_TranList_Data.TotalRecords,
            Offset: this.PendingReward_TranList_Data.Offset,
            Limit: this.PendingReward_TranList_Data.Limit,
            RefreshCount: true,
            SearchCondition: SCon,
            SortExpression: 'TransactionDate desc',
            AccountId: this._HelperService.AccountOwner.AccountId,
            AccountKey: this._HelperService.AccountOwner.AccountKey,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Transactions, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideSpinner();
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this.PendingReward_TranList_Data.Offset = this.PendingReward_TranList_Data.Offset + this.PendingReward_TranList_Data.Limit;
                    this.PendingReward_TranList_Data.TotalRecords = _Response.Result.TotalRecords;
                    var PendingReward_TranList_Data = _Response.Result.Data;
                    PendingReward_TranList_Data.forEach(element => {
                        if (element.CardBrandName != undefined) {
                            element.CardBrandName = element.CardBrandName.toLowerCase().trim();
                        }
                        element.TypeName = element.TypeName.toLowerCase().trim();
                        element.TransactionDate = this._HelperService.GetDateTimeS(element.TransactionDate);
                        this.PendingReward_TranList_Data.Data.push(element);
                    });
                    if (this.PendingReward_TranList_LoaderEvent != undefined) {
                        this.PendingReward_TranList_LoaderEvent.target.complete();
                        if (this.PendingReward_TranList_Data.TotalRecords == this.PendingReward_TranList_Data.Data.length) {
                            this.PendingReward_TranList_LoaderEvent.target.disabled = true;
                        }
                    }
                }
                else {
                    this._HelperService.HideSpinner();
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    PendingReward_TranList_Setup_Overview() {
        var SCon = "";
        if (this.PendingReward_TranList_Data.SearchContent != undefined && this.PendingReward_TranList_Data.SearchContent != null && this.PendingReward_TranList_Data.SearchContent != '') {
            SCon = this._HelperService.GetSearchCondition(SCon, 'UserDisplayName', this._HelperService.AppConfig.DataType.Text, this.PendingReward_TranList_Data.SearchContent);
            SCon = this._HelperService.GetSearchCondition(SCon, 'UserMobileNumber', this._HelperService.AppConfig.DataType.Text, this.PendingReward_TranList_Data.SearchContent);
            SCon = this._HelperService.GetSearchCondition(SCon, 'CashierDisplayName', this._HelperService.AppConfig.DataType.Text, this.PendingReward_TranList_Data.SearchContent);
            SCon = this._HelperService.GetSearchCondition(SCon, 'CashierCode', this._HelperService.AppConfig.DataType.Text, this.PendingReward_TranList_Data.SearchContent);
            SCon = this._HelperService.GetSearchCondition(SCon, 'ReferenceNumber', this._HelperService.AppConfig.DataType.Text, this.PendingReward_TranList_Data.SearchContent);
        }
        SCon = this._HelperService.GetDateCondition(SCon, 'TransactionDate', this._DateConfig.StartTime, this._DateConfig.EndTime);
        var pData = {
            Task: 'getpendingrewardtransactionsoverview',
            TotalRecords: this.PendingReward_TranList_Data.TotalRecords,
            Offset: this.PendingReward_TranList_Data.Offset,
            Limit: this.PendingReward_TranList_Data.Limit,
            RefreshCount: true,
            SearchCondition: SCon,
            SortExpression: 'TransactionDate desc',
            AccountId: this._HelperService.AccountOwner.AccountId,
            AccountKey: this._HelperService.AccountOwner.AccountKey,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Transactions, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideSpinner();
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this.PendingReward_TranList_Data_Overview = _Response.Result;
                    // this.PendingReward_TranList_Data.Offset = this.PendingReward_TranList_Data.Offset + this.PendingReward_TranList_Data.Limit;
                    // this.PendingReward_TranList_Data.TotalRecords = _Response.Result.TotalRecords;
                    // var PendingReward_TranList_Data = _Response.Result.Data;
                    // PendingReward_TranList_Data.forEach(element => {
                    //     if (element.CardBrandName != undefined) {
                    //         element.CardBrandName = element.CardBrandName.toLowerCase().trim();
                    //     }
                    //     element.TypeName = element.TypeName.toLowerCase().trim();
                    //     element.TransactionDate = this._HelperService.GetDateTimeS(element.TransactionDate);
                    //     this.PendingReward_TranList_Data.Data.push(element);
                    // });
                    // if (this.PendingReward_TranList_LoaderEvent != undefined) {
                    //     this.PendingReward_TranList_LoaderEvent.target.complete();
                    //     if (this.PendingReward_TranList_Data.TotalRecords == this.PendingReward_TranList_Data.Data.length) {
                    //         this.PendingReward_TranList_LoaderEvent.target.disabled = true;
                    //     }
                    // }
                }
                else {
                    this._HelperService.HideSpinner();
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    PendingReward_TranList_RowSelected(ReferenceData) {
        if (ReferenceData.PaymentStatusCode == 'paymentstatus.pending') {
            var _OrderDetails =
            {
                OrderId: ReferenceData.OrderId,
                OrderKey: ReferenceData.OrderReference,
            };
            this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveOrder, _OrderDetails);
            this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.orderpaymentprocess);
        }
        else {
            var _OrderDetails =
            {
                OrderId: ReferenceData.OrderId,
                OrderKey: ReferenceData.OrderReference,
            };
            this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveOrder, _OrderDetails);
            this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.orderdetails);
        }


    }
    PendingReward_TranList_NextLoad(event) {
        this.PendingReward_TranList_LoaderEvent = event;
        this.PendingReward_TranList_Setup();

    }
    PendingReward_TranList_Search(text) {
        clearTimeout(this.PendingReward_TranList_delayTimer);
        this.PendingReward_TranList_delayTimer = setTimeout(x => {
            this.PendingReward_TranList_Data =
            {
                SearchContent: text,
                TotalRecords: 0,
                Offset: -1,
                Limit: 10,
                Data: []
            };
            this.PendingReward_TranList_Setup();
        }, 1000);
    }
    async PendingReward_TranList_OpenAddModal(Item) {
        Item.MType = "reward"; const modal = await this._ModalController.create({
            component: ModalRewardReceipt,
            componentProps: Item
        });
        return await modal.present();
    }

    //#endregion

    //#region 
    LoadOverview() {
        this.GetLoyaltyOverview();
        this.GetSalesHistory();
        this.GetLoyaltyVisitHistory();
    }
    _LoyaltyOverview =
        {
            NewCustomerInvoiceAmount: 0,
            NewCustomers: 0,
            RedeemAmount: 0,
            RedeemInvoiceAmount: 0,
            RedeemTransaction: 0,
            RepeatingCustomerInvoiceAmount: 0,
            RepeatingCustomers: 0,
            RewardAmount: 0,
            RewardCommissionAmount: 0,
            RewardInvoiceAmount: 0,
            RewardTransaction: 0,
            Transaction: 0,
            TransactionInvoiceAmount: 0,
            TucPlusRewardClaimTransaction: 0,
            TucPlusRewardTransaction: 0,
            TucRewardAmount: 0,
            TucRewardCommissionAmount: 0,
            TucRewardInvoiceAmount: 0,
            TucRewardTransaction: 0,
            VisitsByRepeatingCustomers: 0,
        };

    public GetLoyaltyOverview() {
        var pData = {
            Task: 'getloyaltyoverview',
            AccountKey: this._HelperService.AccountOwner.AccountKey,
            AccountId: this._HelperService.AccountOwner.AccountId,
            StartDate: this._DateConfig.StartTime,
            EndDate: this._DateConfig.EndTime,
            Source: "transaction.source.merchant"
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Analytics, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    console.log(_Response);
                    this._LoyaltyOverview = _Response.Result;
                    console.log("_LoyaltyOverview", this._LoyaltyOverview);
                    // this._AccountBalance = _Response.Result;
                    // this._AccountBalance.Balance = this._HelperService.GetFixedDecimalNumber(_Response.Result.Balance / 100);
                    // this._AccountBalance.Credit = this._HelperService.GetFixedDecimalNumber(_Response.Result.Credit / 100);
                    // this._AccountBalance.Debit = this._HelperService.GetFixedDecimalNumber(_Response.Result.Debit / 100);
                    // this._AccountBalance.LastTransactionAmount = this._HelperService.GetFixedDecimalNumber(_Response.Result.LastTransactionAmount / 100);
                    // this._AccountBalance.LastTopupBalance = this._HelperService.GetFixedDecimalNumber(_Response.Result.LastTopupBalance / 100);
                    // this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.Balance, this._AccountBalance);
                } else {
                    this._HelperService.IsFormProcessing = false;
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }
        );
    }

    _SalesHistory =
        {
            TotalCustomer: 0,
            TotalInvoiceAmount: 0,
            TotalTransaction: 0,
            Data: [],
        }
    public _OSalesHistory =
        {
            Labels: [],
            SaleColors: [],
            SaleDataSet: [],
            SaleCustomersColors: [],
            SaleCustomersDataSet: [],

            TransactionsColors: [],
            TransactionsDataSet: [],
        }
    public GetSalesHistory() {
        var pData = {
            Task: 'getsaleshistory',
            AccountKey: this._HelperService.AccountOwner.AccountKey,
            AccountId: this._HelperService.AccountOwner.AccountId,
            StartDate: this._DateConfig.StartTime,
            EndDate: this._DateConfig.EndTime,
            Source: "transaction.source.merchant"
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Analytics, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    console.log('getsaleshistory', _Response);
                    var _Data = _Response.Result;
                    this._SalesHistory.Data = _Data;
                    _Data.forEach(data => {
                        this._SalesHistory.TotalCustomer += data.TotalCustomer;
                        this._SalesHistory.TotalInvoiceAmount += data.TotalInvoiceAmount;
                        this._SalesHistory.TotalTransaction += data.TotalTransaction;
                    });
                    this._OSalesHistory.Labels = [];
                    var ResponseData = _Response.Result;
                    var TSaleColor = [{
                        backgroundColor: [],
                    }];
                    var TTransactionsColor = [{
                        backgroundColor: [],
                    }];
                    var TSaleDataSet = [{
                        label: 'Sale',
                        fill: false,
                        borderColor: "#303f9fc4",
                        borderDash: [6, 6],
                        backgroundColor: "#303f9fc4",
                        pointBackgroundColor: "#303f9fc4",
                        pointBorderColor: "#303f9fc4",
                        pointHoverBackgroundColor: "#303f9fc4",
                        pointHoverBorderColor: "#303f9fc4",
                        data: []
                    }];
                    var TTransactionsDataSet = [{
                        label: 'Visits',
                        fill: false,
                        borderColor: "#303f9fc4",
                        borderDash: [4, 4],
                        backgroundColor: "#303f9fc4",
                        pointBackgroundColor: "#303f9fc4",
                        pointBorderColor: "#303f9fc4",
                        pointHoverBackgroundColor: "#303f9fc4",
                        pointHoverBorderColor: "#303f9fc4",
                        data: []
                    }];
                    ResponseData.forEach(element => {
                        var Data = element;
                        this._OSalesHistory.Labels.push(element.Date);
                        TSaleDataSet[0].data.push(Math.round(Data.TotalInvoiceAmount));
                        TSaleColor[0].backgroundColor.push("rgba(97, 192, 60, 0.7)");

                        TTransactionsDataSet[0].data.push(Math.round(Data.TotalTransaction));
                        TTransactionsColor[0].backgroundColor.push("rgba(97, 192, 60, 0.7)");
                    });
                    this._OSalesHistory.SaleDataSet = TSaleDataSet;
                    this._OSalesHistory.SaleColors = TSaleColor;

                    this._OSalesHistory.TransactionsDataSet = TTransactionsDataSet;
                    this._OSalesHistory.TransactionsColors = TTransactionsColor;





                } else {
                    this._HelperService.IsFormProcessing = false;
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }
        );
    }
    _LoyaltyHistory =
        {
            NewCustomer: 0,
            NewCustomerInvoiceAmount: 0,
            RepeatingCustomer: 0,
            RepeatingCustomerInvoiceAmount: 0,
            TotalCustomer: 0,
            TotalInvoiceAmount: 0,
            TotalTransaction: 0,
            VisitsByRepeatingCustomers: 0,
            Data: [],
        }
    public GetLoyaltyVisitHistory() {
        var pData = {
            Task: 'getloyaltyvisithistory',
            AccountKey: this._HelperService.AccountOwner.AccountKey,
            AccountId: this._HelperService.AccountOwner.AccountId,
            StartDate: this._DateConfig.StartTime,
            EndDate: this._DateConfig.EndTime,
            Source: "transaction.source.merchant"
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Analytics, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    console.log('getloyaltyvisithistory ', _Response);
                    var _Data = _Response.Result;
                    this._LoyaltyHistory =
                    {
                        NewCustomer: 0,
                        NewCustomerInvoiceAmount: 0,
                        RepeatingCustomer: 0,
                        RepeatingCustomerInvoiceAmount: 0,
                        TotalCustomer: 0,
                        TotalInvoiceAmount: 0,
                        TotalTransaction: 0,
                        VisitsByRepeatingCustomers: 0,
                        Data: [],
                    }
                    this._LoyaltyHistory.Data = _Data;
                    _Data.forEach(data => {
                        this._LoyaltyHistory.NewCustomer += data.NewCustomer;
                        this._LoyaltyHistory.NewCustomerInvoiceAmount += data.NewCustomerInvoiceAmount;
                        this._LoyaltyHistory.RepeatingCustomer += data.RepeatingCustomer;
                        this._LoyaltyHistory.RepeatingCustomerInvoiceAmount += data.RepeatingCustomerInvoiceAmount;
                        this._LoyaltyHistory.TotalCustomer += data.TotalCustomer;
                        this._LoyaltyHistory.TotalInvoiceAmount += data.TotalInvoiceAmount;
                        this._LoyaltyHistory.TotalTransaction += data.TotalTransaction;
                        this._LoyaltyHistory.VisitsByRepeatingCustomers += data.VisitsByRepeatingCustomers;
                    });

                } else {
                    this._HelperService.IsFormProcessing = false;
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }
        );
    }
    //#endregion


}