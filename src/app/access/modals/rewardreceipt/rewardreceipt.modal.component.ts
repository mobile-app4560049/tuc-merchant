import { Component } from '@angular/core';
import { ModalController, AlertController, NavParams } from '@ionic/angular';
import { HelperService } from '../../../service/helper.service';
import { Observable } from 'rxjs';
import { OResponse } from '../../../service/object.service';
@Component({
    templateUrl: 'rewardreceipt.modal.component.html',
    selector: 'modal-rewardreceipt'
})
export class ModalRewardReceipt {

    public _TransactionDetails: any =
        {


        };
    constructor(
        public navParams: NavParams,
        public _ModalController: ModalController,
        public _AlertController: AlertController,
        public _HelperService: HelperService
    ) {
    }

    ngOnInit() {
        this._TransactionDetails =
        {

            AcquirerDisplayName: null,
            AcquirerIconUrl: null,
            AcquirerId: null,
            AcquirerKey: null,
            Balance: null,
            CashierCode: null,
            CashierDisplayName: null,
            CashierId: null,
            CashierKey: null,
            CommissionAmount: null,
            CreatedByAccountTypeCode: null,
            CreatedByDisplayName: null,
            CreatedById: null,
            CreatedByKey: null,
            InvoiceAmount: null,
            ParentDisplayName: null,
            ParentIconUrl: null,
            ParentId: null,
            ParentKey: null,
            ProviderDisplayName: null,
            ProviderIconUrl: null,
            ProviderId: null,
            ProviderKey: null,
            ReferenceId: null,
            ReferenceKey: null,
            ReferenceNumber: null,
            RewardAmount: null,
            SourceCode: null,
            SourceId: null,
            SourceName: null,
            StatusCode: null,
            StatusId: null,
            StatusName: null,
            SubParentDisplayName: null,
            SubParentId: null,
            SubParentKey: null,
            TerminalId: null,
            TerminalReferenceId: null,
            TerminalReferenceKey: null,
            TransactionDate: null,
            TypeId: null,
            TypeName: null,
            UserAccountId: null,
            UserAccountKey: null,
            UserAmount: null,
            UserDisplayName: null,
            UserMobileNumber: null,
        };
        this._TransactionDetails = this.navParams.data as any;
        console.log(this._TransactionDetails);
    }
    async ModalDismiss(Type) {
        await this._ModalController.dismiss(Type);
    }
}