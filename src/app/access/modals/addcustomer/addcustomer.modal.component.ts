import { Component } from '@angular/core';
import { ModalController, AlertController, NavParams } from '@ionic/angular';
import { HelperService } from '../../../service/helper.service';
import { Observable } from 'rxjs';
import { OResponse } from '../../../service/object.service';
@Component({
    templateUrl: 'addcustomer.modal.component.html',
    selector: 'modal-addcustomer'
})
export class AddCustomerModal {
    constructor(
        public navParams: NavParams,
        public _ModalController: ModalController,
        public _AlertController: AlertController,
        public _HelperService: HelperService
    ) {
    }

    ngOnInit() {
        this._HelperService.IsFormProcessing = false;
    }

    public _CustomerDetails =
        {
            FirstName: null,
            LastName: null,
            EmailAddress: null,
            MobileNumber: null,
            DateOfBirth: null,
            GenderCode: null,
        };
    ToggleGender(Gender) {
        this._CustomerDetails.GenderCode = Gender;
    }

    async ModalDismiss(Type) {
        await this._ModalController.dismiss(Type);
    }

    async ProcessAccount() {
        if (this._CustomerDetails.FirstName == undefined || this._CustomerDetails.FirstName == null || this._CustomerDetails.FirstName == '') {
            this._HelperService.NotifyToast('First name required')
        }
        else if (this._CustomerDetails.LastName == undefined || this._CustomerDetails.LastName == null || this._CustomerDetails.LastName == '') {
            this._HelperService.NotifyToast('Last name required')
        }
        else if (this._CustomerDetails.MobileNumber == undefined || this._CustomerDetails.MobileNumber == null || isNaN(this._CustomerDetails.MobileNumber) == true) {
            this._HelperService.NotifyToast('Mobile number required')
        }
        else {
            const alert = await this._AlertController.create({
                header: 'Register Customer?',
                message: "Tap on continue to create account",
                buttons: [
                    {
                        text: 'Cancel',
                        role: 'cancel',
                        handler: () => {
                        }
                    },
                    {
                        text: 'Continue',
                        cssClass: 'c-en-b-primary',
                        handler: () => {
                            this.ProcessAccount_Confirm();
                        }
                    }
                ]
            });
            return await alert.present();
        }
    }


    ProcessAccount_Confirm() {
        this._HelperService.ShowSpinner('Processing...');
        this._HelperService.IsFormProcessing = true;
        var pData = {
            Task: "savecustomer",
            AccountId: this._HelperService.AccountOwner.AccountId,
            AccountKey: this._HelperService.AccountOwner.AccountKey,
            FirstName: this._CustomerDetails.FirstName,
            LastName: this._CustomerDetails.LastName,
            EmailAddress: this._CustomerDetails.EmailAddress,
            MobileNumber: this._CustomerDetails.MobileNumber,
            DateOfBirth: this._CustomerDetails.DateOfBirth,
            GenderCode: this._CustomerDetails.GenderCode,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Accounts, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideSpinner();
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this._CustomerDetails =
                    {
                        FirstName: null,
                        LastName: null,
                        EmailAddress: null,
                        MobileNumber: null,
                        DateOfBirth: null,
                        GenderCode: null,
                    };
                    this._HelperService.NotifyToast(_Response.Message);
                }
                else {

                    this._HelperService.Notify(_Response.Status, _Response.Message);
                }
            },
            _Error => {
                this._HelperService.HideSpinner();
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });



    }



}