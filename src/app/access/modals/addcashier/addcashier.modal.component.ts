import { Component } from '@angular/core';
import { ModalController, AlertController, NavParams } from '@ionic/angular';
import { HelperService } from '../../../service/helper.service';
import { Observable } from 'rxjs';
import { OResponse } from '../../../service/object.service';
@Component({
    templateUrl: 'addcashier.modal.component.html',
    selector: 'modal-addcashier'
})
export class AddCashierModal {
    constructor(
        public navParams: NavParams,
        public _ModalController: ModalController,
        public _AlertController: AlertController,
        public _HelperService: HelperService
    ) {
    }

    ngOnInit() {
        this.Form_AddUser_Load();
    }



    async ModalDismiss(Type) {
        await this._ModalController.dismiss(Type);
    }
    public Form_AddUser = {
        OperationType: "new",
        Task: this._HelperService.AppConfig.NetworkApi.Feature.SaveUserAccount,
        AccountTypeCode: 'merchantcashier',
        AccountOperationTypeCode: 'accountoperationtype.offline',
        RegistrationSourceCode: 'regsource.system',
        OwnerKey: this._HelperService.AccountOwner.AccountKey,
        UserName: this._HelperService.GeneratePassoword(),
        Password: this._HelperService.GeneratePassoword(),
        AccessPin: this._HelperService.GetRandomNumber(),
        SecondaryPassword: this._HelperService.GetRandomNumber(),
        Name: null,
        FirstName: null,
        LastName: null,
        DisplayName: null,
        ContactNumber: null,
        ReferralCode: null,
        ReferralUrl: null,
        CountValue: 0,
        AverageValue: 0,
        ApplicationStatusCode: null,
        EmailVerificationStatus: 0,
        EmailVerificationStatusDate: null,
        NumberVerificationStatus: 0,
        NumberVerificationStatusDate: null,
        CountryKey: null,
        RegionKey: null,
        RegionAreaKey: null,
        CityKey: null,
        CityAreaKey: null,
        StatusCode: 'default.active',
        Owners: [],
        Configuration: []
    };
    GetNewCashierId() {
        var pData =
        {
            Task: 'generatecashierid',
            ReferenceKey: this._HelperService.AccountOwner.AccountKey
        }
        this._HelperService.ShowSpinner('Generating cashier id...');
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(
            this._HelperService.AppConfig.Network.V2.ThankU,
            pData
        );
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideSpinner();
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    var CashierCode = _Response.Result;
                    this.Form_AddUser.UserName = this._HelperService.AccountOwner.AccountId + '#' + CashierCode + '#' + this._HelperService.GetRandomNumber();
                    this.Form_AddUser.DisplayName = CashierCode;
                } else {
                    this._HelperService.NotifySimple("Unable to generate cashier id" + _Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }
        );
    }

    Form_AddUser_Load() {
        this.Form_AddUser = {
            OperationType: "new",
            Task: this._HelperService.AppConfig.NetworkApi.Feature.SaveUserAccount,
            AccountTypeCode: 'merchantcashier',
            AccountOperationTypeCode: 'accountoperationtype.offline',
            RegistrationSourceCode: 'regsource.system',
            OwnerKey: this._HelperService.AccountOwner.AccountKey,
            UserName: this._HelperService.GeneratePassoword(),
            Password: this._HelperService.GeneratePassoword(),
            AccessPin: this._HelperService.GetRandomNumber(),
            SecondaryPassword: this._HelperService.GetRandomNumber(),
            Name: null,
            FirstName: null,
            LastName: null,
            DisplayName: null,
            ContactNumber: null,
            ReferralCode: null,
            ReferralUrl: null,
            CountValue: 0,
            AverageValue: 0,
            ApplicationStatusCode: null,
            EmailVerificationStatus: 0,
            EmailVerificationStatusDate: null,
            NumberVerificationStatus: 0,
            NumberVerificationStatusDate: null,
            CountryKey: null,
            RegionKey: null,
            RegionAreaKey: null,
            CityKey: null,
            CityAreaKey: null,
            StatusCode: 'default.active',
            Owners: [],
            Configuration: []
        };
        this.GetNewCashierId();
    }

    Form_AddUser_Process() {

        if (this.Form_AddUser.FirstName == undefined || this.Form_AddUser.FirstName == null || this.Form_AddUser.FirstName == '') {
            this._HelperService.NotifySimple('Enter first name');
        }
        else if (this.Form_AddUser.LastName == undefined || this.Form_AddUser.LastName == null || this.Form_AddUser.LastName == '') {
            this._HelperService.NotifySimple('Enter last name');
        }
        else if (this.Form_AddUser.ContactNumber == undefined || this.Form_AddUser.ContactNumber == null || this.Form_AddUser.ContactNumber == '') {
            this._HelperService.NotifySimple('Enter mobile number');
        }
        else {
            this._HelperService.ShowSpinner('Creating cashier...');
            this.Form_AddUser.OwnerKey = this._HelperService.AccountOwner.AccountKey;
            this.Form_AddUser.Name = this.Form_AddUser.FirstName + " " + this.Form_AddUser.LastName;
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(
                this._HelperService.AppConfig.Network.V2.System, this.Form_AddUser);
            _OResponse.subscribe(
                _Response => {
                    this._HelperService.HideSpinner();
                    if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                        this._HelperService.Notify('Cashier Added', "Cashier account created successfully");
                        this.ModalDismiss('cashieradded');
                    } else {
                        this._HelperService.NotifySimple(_Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.HandleException(_Error);
                }
            );
        }

    }
}