import { Component } from '@angular/core';
import { ModalController, AlertController, NavParams } from '@ionic/angular';
import { HelperService } from '../../../service/helper.service';
import { Observable } from 'rxjs';
import { OResponse } from '../../../service/object.service';
declare var moment: any;

@Component({
    templateUrl: 'dateselector.modal.component.html',
    selector: 'modal-dateselector'
})
export class DateSelectorModal {

    _DealConfig =
        {
            DefaultStartDate: null,
            DefaultEndDate: null,
            StartDate: null,
            EndDate: null,
            StartDateConfig: {
            },
            EndDateConfig: {
            },
        }


    public _TransactionDetails: any =
        {


        };
    constructor(
        public navParams: NavParams,
        public _ModalController: ModalController,
        public _AlertController: AlertController,
        public _HelperService: HelperService
    ) {
    }

    ngOnInit() {
        this._DealConfig.StartDateConfig = {
            autoUpdateInput: false,
            singleDatePicker: false,
            timePicker: false,
            locale: { format: "DD-MM-YYYY" },
            alwaysShowCalendars: false,
            showDropdowns: false,
            startDate: new Date(),
            endDate: new Date(),
            minDate: new Date(),
        };
        this._TransactionDetails = this.navParams.data as any;
        console.log(this._TransactionDetails);
        // this.GetTransactionDetails();
    }

    ScheduleStartDateRangeChange(value) {

    }



    async ModalDismiss(Type) {
        await this._ModalController.dismiss(Type);
    }

}