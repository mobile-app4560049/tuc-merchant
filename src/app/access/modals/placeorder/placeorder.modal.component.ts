import { Component } from '@angular/core';
import { ModalController, AlertController, NavParams } from '@ionic/angular';
import { HelperService } from '../../../service/helper.service';
@Component({
    templateUrl: 'placeorder.modal.component.html',
    selector: 'app-placeorder'
})
export class PlaceOrderModal {
    public Users = [];
    constructor(
        public navParams: NavParams,
        public _ModalController: ModalController,
        public _AlertController: AlertController,
        public _HelperService: HelperService
    ) {
    }
    public OrderType = 'gas';
    public OrderTypeVarient = '6 kg';
    public OrderPrice = 2000;
    Gas(Varient) {
        this.OrderTypeVarient = Varient;
        if (this.OrderTypeVarient == '6 kg') {
            this.OrderPrice = 2000;
        }
        else if (this.OrderTypeVarient == '12.5 kg') {
            this.OrderPrice = 4000;
        }
        else if (this.OrderTypeVarient == '50 kg') {
            this.OrderPrice = 5000;
        }
        else if (this.OrderTypeVarient == '5 Lt.') {
            this.OrderPrice = 1075;
        }
        else if (this.OrderTypeVarient == '10 Lt.') {
            this.OrderPrice = 2150;
        }
        else if (this.OrderTypeVarient == '15 Lt.') {
            this.OrderPrice = 3195;
        }
        else if (this.OrderTypeVarient == '20 Lt.') {
            this.OrderPrice = 4260;
        }
    }
    ngOnInit() {
        this.OrderType = this.navParams.get('OrderType');
        if (this.OrderType == 'gas') {
            this.OrderType = 'gas';
            this.OrderTypeVarient = '6 kg';
            this.OrderPrice = 2000;
        }
        if (this.OrderType == 'disel') {
            this.OrderType = 'disel';
            this.OrderTypeVarient = '5 Lt.';
            this.OrderPrice = 1075;
        }
    }
    async myDismiss() {
        await this._ModalController.dismiss(null);
    }
    async SetItem(TItem) {
        var UserIndex = this.Users.findIndex(x => x.Id == TItem.Id);
        if (UserIndex != undefined) {
            if (!this.Users[UserIndex].IsItemChecked) {
                this.Users[UserIndex].IsItemChecked = 1;
            }
            else {
                this.Users[UserIndex].IsItemChecked = 0;
            }
        }
        // await this._ModalController.dismiss(TItem);
    }

    async CompleteUserSelection() {
        // var UserIndex = this.Users.filter(x => x.IsItemChecked == 1);
        await this._ModalController.dismiss('done');
        // await this._ModalController.dismiss(TItem);
    }

    ProceedOrder() {
        this.CompleteUserSelection();
        this._HelperService.Notify('Order confirmed.', 'We are processing your order. You will get notification once confirmed');
    }
}