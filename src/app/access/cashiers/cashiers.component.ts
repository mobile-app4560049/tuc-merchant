import { Component } from '@angular/core';
import { NavController, MenuController, AlertController, ModalController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from '../../service/helper.service';
import { OResponse, ODeviceInformation } from '../../service/object.service';
import { AddCashierModal } from '../modals/addcashier/addcashier.modal.component';

@Component({
    selector: 'app-cashiers',
    templateUrl: 'cashiers.component.html'
})
export class CashiersPage {
    public DeviceInformation: ODeviceInformation;
    constructor(
        public _ModalController: ModalController,
        public _HelperService: HelperService,
        public _AlertController: AlertController,
    ) {
        var DeviceInformationStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
        if (DeviceInformationStorage != null) {
            this.DeviceInformation = DeviceInformationStorage;
        }
    }
    ngOnInit() {
        this.CashiersList_Data =
            {
                SearchContent: "",
                TotalRecords: 0,
                Offset: -1,
                Limit: 10,
                Data: []
            };
        this.CashiersList_Setup();
    }

    public CashiersList_Data =
        {
            SearchContent: "",
            TotalRecords: 0,
            Offset: -1,
            Limit: 10,
            Data: []
        };
    CashiersList_Setup() {
        if (this.CashiersList_Data.Offset == -1) {
            this._HelperService.ShowSpinner();
            this.CashiersList_Data.Offset = 0;
        }
        // SCon = this._HelperService.GetSearchConditionStrict(SCon, 'StatusCode', this._HelperService.AppConfig.DataType.Text, "default.active", '==');
        var SCon = "";
        if (this.CashiersList_Data.SearchContent != undefined && this.CashiersList_Data.SearchContent != null && this.CashiersList_Data.SearchContent != '') {
            SCon = this._HelperService.GetSearchCondition(SCon, 'Name', this._HelperService.AppConfig.DataType.Text, this.CashiersList_Data.SearchContent);
            SCon = this._HelperService.GetSearchCondition(SCon, 'EmployeeId', this._HelperService.AppConfig.DataType.Text, this.CashiersList_Data.SearchContent);
            SCon = this._HelperService.GetSearchCondition(SCon, 'DisplayName', this._HelperService.AppConfig.DataType.Text, this.CashiersList_Data.SearchContent);
            SCon = this._HelperService.GetSearchCondition(SCon, 'EmailAddress', this._HelperService.AppConfig.DataType.Text, this.CashiersList_Data.SearchContent);
            SCon = this._HelperService.GetSearchCondition(SCon, 'MobileNumber', this._HelperService.AppConfig.DataType.Text, this.CashiersList_Data.SearchContent);
        }
        SCon = this._HelperService.GetSearchConditionStrict(SCon, 'OwnerId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AccountOwner.AccountId, '=');
        var pData = {
            Task: 'getcashiers',
            TotalRecords: this.CashiersList_Data.TotalRecords,
            Offset: this.CashiersList_Data.Offset,
            Limit: this.CashiersList_Data.Limit,
            RefreshCount: true,
            SearchCondition: SCon,
            SortExpression: 'Displayname asc',
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.TUCAccCore, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.HideSpinner();
                if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                    this.CashiersList_Data.Offset = this.CashiersList_Data.Offset + this.CashiersList_Data.Limit;
                    this.CashiersList_Data.TotalRecords = _Response.Result.TotalRecords;
                    var CashiersList_Data = _Response.Result.Data;
                    CashiersList_Data.forEach(element => {
                        element.CreateDate = this._HelperService.GetDateTimeS(element.CreateDate);
                        element.LastTransactionDate = this._HelperService.GetDateTimeS(element.LastTransactionDate);
                        this.CashiersList_Data.Data.push(element);
                    });
                    if (this.LoaderEvent != undefined) {
                        this.LoaderEvent.target.complete();
                        if (this.CashiersList_Data.TotalRecords == this.CashiersList_Data.Data.length) {
                            this.LoaderEvent.target.disabled = true;
                        }
                    }
                }
                else {
                    this._HelperService.HideSpinner();
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    CashiersList_RowSelected(ReferenceData) {
        if (ReferenceData.PaymentStatusCode == 'paymentstatus.pending') {
            var _OrderDetails =
            {
                OrderId: ReferenceData.OrderId,
                OrderKey: ReferenceData.OrderReference,
            };
            this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveOrder, _OrderDetails);
            this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.orderpaymentprocess);
        }
        else {
            var _OrderDetails =
            {
                OrderId: ReferenceData.OrderId,
                OrderKey: ReferenceData.OrderReference,
            };
            this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveOrder, _OrderDetails);
            this._HelperService.NavigatePush(this._HelperService.AppConfig.Pages.Access.orderdetails);
        }


    }
    public LoaderEvent: any = undefined;
    NextLoad(event) {
        this.LoaderEvent = event;
        this.CashiersList_Setup();

    }

    private delayTimer;
    doSearch(text) {
        clearTimeout(this.delayTimer);
        this.delayTimer = setTimeout(x => {
            this.CashiersList_Data =
                {
                    SearchContent: text,
                    TotalRecords: 0,
                    Offset: -1,
                    Limit: 10,
                    Data: []
                };
            this.CashiersList_Setup();
        }, 1000);
    }


    async OpenAddModal() {
        const modal = await this._ModalController.create({
            component: AddCashierModal
        });
        return await modal.present();
    }
}