import { Component } from '@angular/core';
import { NavController, MenuController, AlertController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from '../../service/helper.service';
import { OResponse, ODeviceInformation } from '../../service/object.service';
import { FCM } from '@ionic-native/fcm/ngx';

@Component({
    selector: 'app-login',
    templateUrl: 'login.component.html'
})
export class LoginPage {
    public UserName = "";
    public Password = "";
    public DeviceInformation: ODeviceInformation;
    constructor(
        public _Firebase: FCM,
        private _MenuController: MenuController,
        public _HelperService: HelperService,
        private _AlertController: AlertController,
    ) {
        _MenuController.enable(false);
        var DeviceInformationStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
        if (DeviceInformationStorage != null) {
            this.DeviceInformation = DeviceInformationStorage;
        }
    }

    async StartVerification() {
        if (this.UserName == undefined || this.UserName == null || this.UserName == "") {
            this._HelperService.NotifySimple('Enter username');
        }
        else if (this.Password == undefined || this.Password == null || this.Password == "") {
            this._HelperService.NotifySimple('Enter password');
        }
        else {
            this.AppLogin();
        }
    }

    AppLogin() {
        var Not = this._HelperService.GetStorageValue('dnot');
        var _RequestData = {
            Task: 'login',
            UserName: this.UserName,
            Password: this.Password,
            CountryIso: "ng",
            OsName: this.DeviceInformation.OsName,
            OsVersion: this.DeviceInformation.OsVersion,
            SerialNumber: this.DeviceInformation.SerialNumber,
            Brand: this.DeviceInformation.Brand,
            Model: this.DeviceInformation.Model,
            Width: this.DeviceInformation.Width,
            Height: this.DeviceInformation.Height,
            CarrierName: this.DeviceInformation.CarrierName,
            Mcc: this.DeviceInformation.Mcc,
            Mnc: this.DeviceInformation.Mnc,
            NotificationUrl: Not,
            Channel: "mobile"
        };
        this._HelperService.ShowSpinner('please wait ...');
        try {
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V1.System, _RequestData);
            _OResponse.subscribe(
                (_Response) => {
                    this._HelperService.HideSpinner();
                    if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                        if (_Response.Result.UserAccount.AccountTypeCode == 'merchantsubaccount' || _Response.Result.UserAccount.AccountTypeCode == 'thankumerchant') {
                            var OOwnerId = _Response.Result.UserAccount.AccountId;
                            if (_Response.Result.UserOwner != undefined && _Response.Result.UserOwner != null && _Response.Result.UserOwner.AccountId != 0) {
                                OOwnerId = _Response.Result.UserOwner.AccountId;
                            }
                            var TokenItem = this._HelperService.GetStorageValue('dnot');
                            if (TokenItem != undefined && TokenItem != null) {
                                this.UpdateToken(TokenItem);
                            }
                            this._Firebase.subscribeToTopic(this._HelperService.AppConfig.PushTopics.tucmerchant + this._HelperService.AccountOwner.AccountId).then(x => {
                            }).catch(x => {
                            });

                            // this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.Account, _Response.Result);
                            // this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Dashboard);
                            this._Firebase.subscribeToTopic(this._HelperService.AppConfig.PushTopics.tucmerchant + OOwnerId).then(x => {
                                this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.Account, _Response.Result);
                                this._HelperService.RefreshProfile();
                                this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Dashboard);
                            }).catch(x => {
                                this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.Account, _Response.Result);
                                this._HelperService.RefreshProfile();
                                this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Dashboard);
                            });
                        }
                        else if (_Response.Result.UserAccount.AccountTypeCode == 'merchantcashier') {
                            this._MenuController.enable(false);
                            this._Firebase.subscribeToTopic(this._HelperService.AppConfig.PushTopics.tucmerchant + OOwnerId).then(x => {
                                this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.Account, _Response.Result);
                                this._HelperService.RefreshProfile();
                                setTimeout(() => {
                                    this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.CashierAccess.cashierdashboard);
                                }, 300);
                            }).catch(x => {
                                this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.Account, _Response.Result);
                                this._HelperService.RefreshProfile();
                                setTimeout(() => {
                                    this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.CashierAccess.cashierdashboard);
                                }, 300);
                            });

                        }
                        else {
                            this._HelperService.Notify('Invalid Account', "Invalid account. Please login to webpanel or contact support");
                        }
                    } else {
                        this._HelperService.Notify('Invalid Credentials', "Enter valid username and password");
                    }
                },
                (_Error) => {
                    this._HelperService.HideSpinner();
                    this._HelperService.HandleException(_Error);
                }
            );
        } catch (_Error) {
            debugger;
            this._HelperService.HideSpinner();
            if (_Error.status == 0) {
                this._HelperService.Notify('Operation failed', "Please check your internet connection");
            }
            else if (_Error.status == 401) {
                var EMessage = JSON.parse(_Error._body).error;
                this._HelperService.Notify('Operation failed', EMessage + ' Unable to start verification. Please contact support')
            }
            else {
                this._HelperService.Notify('Operation failed', ' Unable to start verification. Please contact support')
            }
        }
    }

    NavReg() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Reg.Registration);
        // this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Reg.addresslocator);
    }

    UpdateToken(Token) {
        this._HelperService.SaveStorageValue('dnot', Token);
        if (this._HelperService.AccountInfo.UserAccount.AccountId != undefined && this._HelperService.AccountInfo.UserAccount.AccountId != null && this._HelperService.AccountInfo.UserAccount.AccountId != 0) {
            var DeviceInfo =
            {
                Task: "updatedevicenotificationurl",
                UserAccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
                UserAccountId: this._HelperService.AccountInfo.UserAccount.AccountId,
                NotificationUrl: Token,
            };
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V2.System, DeviceInfo);
            _OResponse.subscribe(
                _Response => {
                    this._HelperService.HideSpinner();
                    if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                        this._HelperService.HideSpinner();
                    }
                    else {
                        this._HelperService.Notify(_Response.Status, _Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.HandleException(_Error);
                });
        }
    }


}