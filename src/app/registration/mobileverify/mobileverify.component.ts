import { Component } from '@angular/core';
import { NavController, MenuController, AlertController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from '../../service/helper.service';
import { OResponse, ODeviceInformation } from '../../service/object.service';
import { FCM } from '@ionic-native/fcm/ngx';

@Component({
    selector: 'app-mobileverify',
    templateUrl: 'mobileverify.component.html'
})
export class MobileVerifyPage {
    public DeviceInformation: ODeviceInformation;
    constructor(
        public _Firebase: FCM,
        public _HelperService: HelperService,
        public _AlertController: AlertController,
    ) {
        var DeviceInformationStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
        if (DeviceInformationStorage != null) {
            this.DeviceInformation = DeviceInformationStorage;
        }
    }
    public ionViewDidEnter() {
    }
    ngOnInit() {
        this._Details = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference);
        this._Details.Task = "onboardmerchantverifynumber";
    }



    public _Details =
        {
            ReferenceId: 0,
            ReferenceKey: null,
            Token: null,
            Task: "onboardmerchant",
            DisplayName: null,
            Name: null,
            MobileNumber: null,
            EmailAddress: null,
            Password: null,
            AccountOperationTypeCode: null,
            WebsiteUrl: null,
            About: null,
            Description: null,
            Categories: [],
            AccessCode: null,
            Address:
            {
                Address: null,
                Latitude: 0,
                Longitude: 0,
                CityAreaName: null,
                CityAreaId: 0,
                CityName: null,
                CityId: 0,
                StateName: null,
                StateId: 0,
                CountryName: null,
                CountryId: 0,
                MapAddress: null,
            },
        }

    UpdateProfile() {
        if (this._Details.AccessCode == undefined || this._Details.AccessCode == null || this._Details.AccessCode == "") {
            this._HelperService.NotifyToast('Enter your mobile verification code received on ' + this._Details.MobileNumber);
        }
        else {
            this.UpdatePin();
        }
    }

    async UpdatePin() {
        this._HelperService.IsFormProcessing = true;
        this._HelperService.ShowSpinner('Please wait');
        try {
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Accounts, this._Details);
            _OResponse.subscribe(
                (_Response) => {
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.HideSpinner();
                    if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                        this._HelperService.NotifyToast(_Response.Message);
                        this.AppLogin();
                        // this._Details.ReferenceId = _Response.Result.ReferenceId;
                        // this._Details.ReferenceKey = _Response.Result.ReferenceKey;
                        // this._Details.Token = _Response.Result.Token;
                        // this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference, this._Details);

                    } else {
                        this._HelperService.Notify('Operation  failed failed', _Response.Message);
                    }
                },
                (_Error) => {
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.HideSpinner();
                    this._HelperService.HandleException(_Error);
                }
            );
        } catch (_Error) {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HideSpinner();
            if (_Error.status == 0) {
                this._HelperService.Notify('Operation failed', "Please check your internet connection");
            }
            else if (_Error.status == 401) {
                var EMessage = JSON.parse(_Error._body).error;
                this._HelperService.Notify('Operation failed', EMessage + ' Unable to start verification. Please contact support')
            }
            else {
                this._HelperService.Notify('Operation failed', ' Unable to start verification. Please contact support')
            }
        }
        // const alert = await this._AlertController.create({
        //     header: 'Update Pin?',
        //     message: 'Do you want to continue ?',
        //     buttons: [
        //         {
        //             text: 'No',
        //             role: 'cancel',
        //             handler: () => {
        //             }
        //         },
        //         {
        //             text: 'Yes',
        //             cssClass: 'c-en-b-primary',
        //             handler: () => {

        //             }
        //         }
        //     ]
        // });
        // return await alert.present();
    }

    NavLogin() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Auth.Login);
    }



    AppLogin() {
        this._HelperService.IsFormProcessing = true;
        var Not = this._HelperService.GetStorageValue('dnot');
        var _RequestData = {
            Task: 'login',
            UserName: this._Details.EmailAddress,
            Password: this._Details.Password,
            CountryIso: "ng",
            OsName: this.DeviceInformation.OsName,
            OsVersion: this.DeviceInformation.OsVersion,
            SerialNumber: this.DeviceInformation.SerialNumber,
            Brand: this.DeviceInformation.Brand,
            Model: this.DeviceInformation.Model,
            Width: this.DeviceInformation.Width,
            Height: this.DeviceInformation.Height,
            CarrierName: this.DeviceInformation.CarrierName,
            Mcc: this.DeviceInformation.Mcc,
            Mnc: this.DeviceInformation.Mnc,
            NotificationUrl: Not,
            Channel: "mobile"
        };
        this._HelperService.ShowSpinner('please wait ...');
        try {
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V1.System, _RequestData);
            _OResponse.subscribe(
                (_Response) => {
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.HideSpinner();
                    if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HideSpinner();
                        setTimeout(() => {
                            if (_Response.Result.UserAccount.AccountTypeCode == 'merchantsubaccount' || _Response.Result.UserAccount.AccountTypeCode == 'thankumerchant') {
                                var OOwnerId = _Response.Result.UserAccount.AccountId;
                                if (_Response.Result.UserOwner != undefined && _Response.Result.UserOwner != null && _Response.Result.UserOwner.AccountId != 0) {
                                    OOwnerId = _Response.Result.UserOwner.AccountId;
                                }
                                // this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.Account, _Response.Result);
                                // this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Dashboard);
                                this._Firebase.subscribeToTopic(this._HelperService.AppConfig.PushTopics.tucmerchant + OOwnerId).then(x => {
                                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.Account, _Response.Result);
                                    this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Dashboard);
                                }).catch(x => {
                                    this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.Account, _Response.Result);
                                    this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Dashboard);
                                });
                            }
                            else {
                                this._HelperService.IsFormProcessing = false;
                                this._HelperService.HideSpinner();
                                this._HelperService.Notify('Invalid Account', "Invalid account. Please login to webpanel or contact support");
                                this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Auth.Login);
                            }
                        }, 300);
                    } else {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HideSpinner();
                        this._HelperService.Notify('Invalid Credentials', "Enter valid username and password");
                        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Auth.Login);
                    }
                },
                (_Error) => {
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.HideSpinner();
                    this._HelperService.HandleException(_Error);
                    this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Auth.Login);
                }
            );
        } catch (_Error) {
            debugger;
            this._HelperService.HideSpinner();
            if (_Error.status == 0) {
                this._HelperService.Notify('Operation failed', "Please check your internet connection");
                this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Auth.Login);
            }
            else if (_Error.status == 401) {
                var EMessage = JSON.parse(_Error._body).error;
                this._HelperService.Notify('Operation failed', EMessage + ' Unable to start verification. Please contact support')
                this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Auth.Login);
            }
            else {
                this._HelperService.Notify('Operation failed', ' Unable to start verification. Please contact support')
                this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Auth.Login);
            }
        }
    }
}