import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AddressLocatorPage } from './addresslocator.component';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { AgmCoreModule } from '@agm/core';
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        GooglePlaceModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyARhkleqWEzCqbtlwaTwvcRdYe5X5rwQbY'
        }),
        RouterModule.forChild([
            {
                path: '',
                component: AddressLocatorPage
            }
        ])
    ],
    declarations: [AddressLocatorPage]
})
export class AddressLocatorPageModule { }
