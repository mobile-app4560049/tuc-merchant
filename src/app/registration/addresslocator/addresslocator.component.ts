import { Component } from '@angular/core';
import { NavController, MenuController, AlertController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from '../../service/helper.service';
import { OResponse, ODeviceInformation } from '../../service/object.service';
import { Address } from 'ngx-google-places-autocomplete/objects/address';

@Component({
    selector: 'app-addresslocator',
    templateUrl: 'addresslocator.component.html'
})
export class AddressLocatorPage {
    public DeviceInformation: ODeviceInformation;
    constructor(
        public _HelperService: HelperService,
        public _AlertController: AlertController,
    ) {
        var DeviceInformationStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
        if (DeviceInformationStorage != null) {
            this.DeviceInformation = DeviceInformationStorage;
        }
    }
    public ionViewDidEnter() {
        // this._HelperService.SetPageName("AddressSetup");
    }
    public _Details =
        {
            ReferenceId: 0,
            ReferenceKey: null,
            Token: null,
            Task: "onboardmerchant",
            DisplayName: null,
            Name: null,
            MobileNumber: null,
            EmailAddress: null,
            Password: null,
            AccountOperationTypeCode: null,
            WebsiteUrl: null,
            About: null,
            Description: null,
            Categories: [],
            Address:
            {
                Address: null,
                Latitude: -1,
                Longitude: -1,
                CityAreaName: null,
                CityName: null,
                StateName: null,
                CountryName: null,
                MapAddress: null,
                ZipCode: null,
            },
        }
    // public UserCustomAddress =
    //     {
    //         Name: null,
    //         MobileNumber: null,
    //         EmailAddress: null,
    //         AddressLine1: null,
    //         AddressLine2: null,
    //         Landmark: null,
    //         AlternateMobileNumber: null,

    //         CityName: null,
    //         StateName: null,
    //         CountryName: null,
    //         ZipCode: null,
    //         Instructions: null,

    //         MapAddress: null,
    //         Latitude: -1,
    //         Longitude: -1,
    //     }
    ngOnInit() {
        this._Details = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference); // var TAddress = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.CustomLocation);
        // if (TAddress != null) {
        //     this.UserCustomAddress = TAddress;
        // }
        // else {
        //     this._Details.Address.Latitude = this._HelperService.AppConfig.ActiveLocation.Lat;
        //     this._Details.Address.Longitude = this._HelperService.AppConfig.ActiveLocation.Lon;
        // }
    }

    ConfirmAddress() {
        if (this._Details.Address.Latitude == -1 && this._Details.Address.Longitude == -1) {
            this._HelperService.NotifyToast('Locate your address');
        }
        else if (this._Details.Address.Address == null || this._Details.Address.Address == '') {
            this._HelperService.NotifyToast('Enter address');
        }
        else if (this._Details.About == undefined || this._Details.About == null || this._Details.About == "") {
            this._HelperService.NotifyToast('Enter information about your business');
        }
        else {
            // this._HelperService.DeleteStorage('incartorder');
            // this._HelperService.AppConfig.CustomLocation.Lat = this.UserCustomAddress.Latitude;
            // this._HelperService.AppConfig.CustomLocation.Lon = this.UserCustomAddress.Longitude;
            // this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.CustomLocation, this._Details.Address);
            this._HelperService.IsFormProcessing = true;
            this._HelperService.ShowSpinner('Registring account');
            try {
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.Accounts, this._Details);
                _OResponse.subscribe(
                    (_Response) => {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HideSpinner();
                        if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                            this._HelperService.NotifyToast(_Response.Message);
                            this._Details.ReferenceId = _Response.Result.ReferenceId;
                            this._Details.ReferenceKey = _Response.Result.ReferenceKey;
                            this._Details.Token = _Response.Result.Token;
                            this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference, this._Details);
                            this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Reg.MobileVerifyPage);

                        } else {
                            this._HelperService.Notify('Operation  failed failed', _Response.Message);
                        }
                    },
                    (_Error) => {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HideSpinner();
                        this._HelperService.HandleException(_Error);
                    }
                );
            } catch (_Error) {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HideSpinner();
                if (_Error.status == 0) {
                    this._HelperService.Notify('Operation failed', "Please check your internet connection");
                }
                else if (_Error.status == 401) {
                    var EMessage = JSON.parse(_Error._body).error;
                    this._HelperService.Notify('Operation failed', EMessage + ' Unable to start verification. Please contact support')
                }
                else {
                    this._HelperService.Notify('Operation failed', ' Unable to start verification. Please contact support')
                }
            }
        }
    }
    NavigateDashboard() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Access.Dashboard);

    }
    placeMarker(Item) {
        this._Details.Address.Latitude = Item.coords.lat;
        this._Details.Address.Longitude = Item.coords.lng;
    }

    public Form_UpdateUser_AddressChange(address: Address) {
        this._Details.Address.Latitude = address.geometry.location.lat();
        this._Details.Address.Longitude = address.geometry.location.lng();
        this._Details.Address.MapAddress = address.formatted_address;
        this._Details.Address.Address = address.formatted_address;
        address.address_components.forEach(address_component => {
            if (address_component.types[0] == "locality") {
                this._Details.Address.CityName = address_component.long_name;
            }
            if (address_component.types[0] == "country") {
                this._Details.Address.CountryName = address_component.long_name;
            }
            if (address_component.types[0] == "postal_code") {
                this._Details.Address.ZipCode = address_component.long_name;
            }
            if (address_component.types[0] == "administrative_area_level_1") {
                this._Details.Address.StateName = address_component.long_name;
            }
        });
    }
}