import { Component } from '@angular/core';
import { NavController, MenuController, AlertController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HelperService } from '../../service/helper.service';
import { OResponse, ODeviceInformation } from '../../service/object.service';

@Component({
    selector: 'app-accountinfo',
    templateUrl: 'info.component.html'
})
export class InfoPage {
    public DeviceInformation: ODeviceInformation;
    constructor(
        public _HelperService: HelperService,
        public _AlertController: AlertController,
    ) {
        var DeviceInformationStorage = this._HelperService.GetStorage(this._HelperService.AppConfig.StorageHelper.Device);
        if (DeviceInformationStorage != null) {
            this.DeviceInformation = DeviceInformationStorage;
        }
    }
    public ionViewDidEnter() {
    }
    ngOnInit() {
        this.GetMerchantCategories();
    }

    public CategoriesList = [];

    onCatChange(Items) {
        if (Items != undefined && Items.length > 0) {
            Items.forEach(Item => {
                var Cat = this.CategoriesList.find(x => x.ReferenceId == Item);
                if (Cat != undefined && Cat != null) {
                    this._Details.Categories.push(
                        {
                            ReferenceId: Cat.ReferenceId,
                            ReferenceKey: Cat.ReferenceKey,
                        }
                    )
                }
            });
        }
    }
    async GetMerchantCategories() {
        this._HelperService.IsFormProcessing = true;
        var _RequestData = {
            Task: "getcategories",
            AccountId: this._HelperService.AccountInfo.UserAccount.AccountId,
            AccountKey: this._HelperService.AccountInfo.UserAccount.AccountKey,
        };
        this._HelperService.ShowSpinner('Please wait');
        try {
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.Network.V3.OuterOperations, _RequestData);
            _OResponse.subscribe(
                (_Response) => {
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.HideSpinner();
                    if (_Response.Status == this._HelperService.AppConfig.StatusSuccess) {
                        this.CategoriesList = _Response.Result.Data;
                    } else {
                        this._HelperService.Notify('Update failed', _Response.Message);
                    }
                },
                (_Error) => {
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.HideSpinner();
                    this._HelperService.HandleException(_Error);
                }
            );
        } catch (_Error) {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HideSpinner();
            if (_Error.status == 0) {
                this._HelperService.Notify('Operation failed', "Please check your internet connection");
            }
            else if (_Error.status == 401) {
                var EMessage = JSON.parse(_Error._body).error;
                this._HelperService.Notify('Operation failed', EMessage + ' Unable to start verification. Please contact support')
            }
            else {
                this._HelperService.Notify('Operation failed', ' Unable to start verification. Please contact support')
            }
        }
    }

    public _Details =
        {
            ReferenceId: 0,
            ReferenceKey: null,
            Token: null,
            Task: "onboardmerchant",
            DisplayName: null,
            Name: null,
            MobileNumber: null,
            EmailAddress: null,
            Password: null,
            AccountOperationTypeCode: null,
            WebsiteUrl: null,
            About: null,
            Description: null,
            Categories: [],

            Address:
            {
                Address: null,
                Latitude: 0,
                Longitude: 0,
                CityAreaName: null,
                CityAreaId: 0,
                CityName: null,
                CityId: 0,
                StateName: null,
                StateId: 0,
                CountryName: null,
                CountryId: 0,
                MapAddress: null,
            },
        }

    UpdateProfile() {
        if (this._Details.DisplayName == undefined || this._Details.DisplayName == null || this._Details.DisplayName == "") {
            this._HelperService.NotifyToast('Enter your business name');
        }
        else if (this._Details.EmailAddress == undefined || this._Details.EmailAddress == null || this._Details.EmailAddress == "") {
            this._HelperService.NotifyToast('Enter email address');
        }

        else if (this._Details.MobileNumber == undefined || this._Details.MobileNumber == null || this._Details.MobileNumber == "") {
            this._HelperService.NotifyToast('Enter mobile number');
        }
        else if (this._Details.Password == undefined || this._Details.Password == null || this._Details.Password == "") {
            this._HelperService.NotifyToast('Enter password for your account');
        }

        else {
            // this.UpdatePin();
            this._HelperService.SaveStorage(this._HelperService.AppConfig.StorageHelper.ActiveReference, this._Details);
            this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Reg.addresslocator);
        }
    }

    async UpdatePin() {

        // const alert = await this._AlertController.create({
        //     header: 'Update Pin?',
        //     message: 'Do you want to continue ?',
        //     buttons: [
        //         {
        //             text: 'No',
        //             role: 'cancel',
        //             handler: () => {
        //             }
        //         },
        //         {
        //             text: 'Yes',
        //             cssClass: 'c-en-b-primary',
        //             handler: () => {

        //             }
        //         }
        //     ]
        // });
        // return await alert.present();
    }

    NavLogin() {
        this._HelperService.NavigateRoot(this._HelperService.AppConfig.Pages.Auth.Login);
    }


}